# US3001
=======================================


# 1. Requisitos

**US2005** Como Gestor de produção, eu pretendo introduzir uma nova ordem de produção.


# 2. Análise

* O perfil de utilizador é o Gestor de Produção que já deve estar registado;
* A ordem de produção é caracterizado por encomenda, produto, quantidade, previsão de data de execução e data de emissão.
* encomendas e produtos já devem existir no sistema.

# 3. Design

![US2010_IntroduceNewProductionOrder_SD](US2010_IntroduceNewProductionOrder_SD.svg)

## 3.2. Diagrama de Classes

![US2010_IntroduceNewProductionOrder_CD](US2010_IntroduceNewProductionOrder_CD.svg)

* **IntroduceNewProductionOrderUI**: classe de user interface;
* **IntroduceNewProductionOrderController** : controller com as principais funcionalidades do user story;
* **ListProductService**: service class com funcionalidades de consulta de Products da presistência;
* **Product**: classe Product com construtor e outros métodos;
* **ProductRepository**: classe que agrega todas os Products.
* **ProductDTO**: classe DTO com apenas um construtor;
* **OrderService**: service class com funcionalidades de consulta de Orders da presistência;
* **Order**: classe Order com construtor e outros métodos;
* **OrderRepository**: classe que agrega todas os Orders.
* **OrderDTO**: classe DTO com apenas um construtor;
* **ProductionOrder**: classe ProductionOrder com construtor e outros métodos;
* **ProductionOrderRepository**: classe que agrega todas os ProductionOrders.
* ***Interface* DTO**: interface DTO com método *toDTO()*.

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acopolamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Presistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
