# US3001
=======================================


# 1. Requisitos

**US3003** Como Gestor de Chão de Fábrica, eu especificar um novo deposito.

* Este user story foi interpretado como uma definição de uma novo deposito.

# 2. Análise

* O perfil de utilizador é o Gestor de Chão de Fábrica já deve estar registado;
* O deposito é caracterizado por um código e uma descrição
* O deposito é identificado pelo código que, por essa razão, deve ser único


![US3003_SpecifyNewDeposit_SSD](US3003_SpecifyNewDeposit_SSD.png)

# 3. Design

![US3003_SpecifyNewDeposit_SD](US3003_SpecifyNewDeposit_SD.png)

## 3.2. Diagrama de Classes

![US3003_SpecifyNewDeposit_CD](US3003_SpecifyNewDeposit_CD.png)

* **SpecifyNewDepositUI**: classe de user interface;
* **SpecifyNewDepositController** : controller com as principais funcionalidades do user story;
* **ListDepositService**: service class com funcionalidades de consulta de Depositos da presistência;
* **Deposit**: classe Deposit com construtor e outros métodos;
* **DepositRepository**: classe que agrega todas os Depositos.
* **DepositDTO**: classe DTO com apenas um construtor;
* ***Interface* DTO**: interface DTO com método *toDTO()*.

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acopolamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Presistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
