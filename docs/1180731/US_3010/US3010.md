# US3010
=======================================


# 1. Requisitos

**US3010** Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.


# 2. Análise

* O perfil de utilizador é o Gestor de Produção que já deve estar registado;
* Devem existir maquinas registadas.
* Uma maquina deve ter configurações.
* Se uma maquina já tiver uma configuração solicitada é possível substituir a mesma por uma nova configuração.


# 3. Design

![US_3010_SD](US_3010_SD.svg)

## 3.2. Diagrama de Classes

![US_3010_CD](US_3010_CD.svg)

* **RequestConfigurationToMachineUI**: classe de user interface;
* **RequestConfigurationToMachineController** : controller com as principais funcionalidades do user story;
* **MachineManagementService**: service class com funcionalidades de consulta de Machines da presistência;
* **Machine**: classe Machine com construtor e outros métodos;
* **Configuration**: classe Configuration com construtor e outros métodos;
* **MachineRepository**: classe que agrega todas as Machines.
* **MachineDTO**: classe DTO com apenas um construtor;
* ***Interface* DTO**: interface DTO com método *toDTO()*.

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acopolamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Presistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
