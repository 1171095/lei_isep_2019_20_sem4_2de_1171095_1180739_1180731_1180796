# US1006
=======================================


# 1. Requisitos

**US1006** Como Gestor de Projeto, eu pretendo que a equipa proceda à inicialização (bootstrap) de alguns produtos.

* Este user story foi interpretado como inicializção de alguns objetos 'Produto' (Product) no contexto de persistência, ou seja, adicionar produtos à presistência para que possam ser usados nos restantes US´s que necessitem.

# 2. Análise

* Das primeiras funcionalidades a ser efetuadas do programa.
* O ator é o sistema.
* São inicalizados vários produtos com diferentes atributos e/ou sem certos atributos (ficha de produção p.e.);
* Os produtos inicalizados são colocados na presistência para futura utilização.

# 3. Design
![SD](SD.jpg)

* **ProductBootstrap**: classe que vai criar novos produtos (Product) e depois adiciona diretamente na presistencia (ProductRepository)
* **Product**: classe referente ao Produto, contém construtor e outros métodos.
* **ProductRepository**: classe que comunica com a presistência.

## 3.1. Realização da Funcionalidade

* **Fluxo principal**
  * o sistema inicia o bootstrap;
  * instacia vários produtos;
  * carrega os produtos instanciados para a presistência.

## 3.2. Diagrama de Classes

*sem diagrama de classes visto se tratar de bootstrap*

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.
  
* Facilita a implementação de testes unitários.
* Diminui o acoplamento entre classes.


## 3.4. Testes 
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



