# US1011
=======================================


# 1. Requisitos

**US1011** Como Gestor de Projeto, eu pretendo que a equipa desenvolva uma aplicação que simule o funcionamento de uma máquina, nomeadamente no envio de mensagens geradas por estas.

* Este user story foi interpretado utilização de threads para simular o envio de mensagens gerado por uma máquina.

# 2. Análise

* Pode ou não haver máquinas já registadas;
* Comunicação entre clientes e servidores através de protocolos de comunicações.


# 3. Design


## 3.1. Realização da Funcionalidade

* **Fluxo principal**

    * O gestor de projeto inicia um simulador de máquina;
    * O simulador de máquina cria duas threads, uma para funcionar como cliente (na comunicação com o sistema central através de tcp) e outra como servidor;
    * **TCP**
      * O cliente envia um *Hello Request* para o servidor do sistema central.
      * O servidor envia um *ACK response* para o cliente.
      * O cliente lê o ficheiro de mensagens e envia cada mensagem para o servidor do sistema central.
      * O servidor envia um *ACK response* para o cliente.
      * O cliente recebe *Config request* do servidor do sistema central.
      * O cliente envia um *ACK response* para o servidor.





# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
