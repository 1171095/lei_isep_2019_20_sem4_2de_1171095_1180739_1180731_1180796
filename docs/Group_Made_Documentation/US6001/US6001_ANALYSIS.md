# US6001
==========================================================================


# 1. Requisitos

**US6001** Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.

* Este user story foi interpretado como o sistema de Monitorização enviar *HELLO request* e *RESET request* para máquinas.

# 2. Análise

* Pode ou não haver máquinas já registadas;
* Comunicação entre clientes e servidores através de protocolos de comunicação.


# 3. Design


## 3.1. Realização da Funcionalidade

* **Fluxo principal**

>(Nesta US é utilizado UDP como protocolo de comunicação)

  * O sistema de Monitorização inicia a comunicação com máquinas de uma determinada linha de produção;
  * O sistema de Monitorização envia *HELLO request* para as máquinas (isto acontece a cada 30s);
  * O sistema de Monitorização recebe um *ACK response* das máquinas;
  * O sistema de Monitorização envia um *RESET request* para as máquinas;
  * O sistema de Monitorização recebe um *ACK response* das máquina;



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
