# US4001
==========================================================================


# 1. Requisitos

**US4001** Como SCM, pretendo importar mensagens de ficheiros de um diretório e posteriormente mover esses mesmos ficheiros para outro diretório.

# 2. Análise

* existe um diretório com ficheiro de mensagens por ler;
* existe um diretório com ficheiro de mensagens já lidas;
* os ficheiros de mensagens podem conter qualquer tipo de mensagens;
* podem ser lidos mais que um ficheiro de cada vez, lendo um ficheiro por thread.

# 3. Design


## 3.1. Realização da Funcionalidade

* **Fluxo principal**

  * O sistema central inicia a importação das mensagens;
  * O sistema central cria uma thread para cada ficheiro encontrado no diretório de mensagens por ler;
  * O sistema central lê as mensagens de cada ficheiro;
  * O sistema central move os ficheiros lidos para o diretório de ficheiros lidos;



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
