# US4002
==========================================================================


# 1. Requisitos

**US4002** Como SCM, pretendo proceder à recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção.

* Este user story foi interpretado como o sistema central responder a *HELLO request* e *MSG request* da máquina e enviar *CONFIG request* à máquina.

# 2. Análise

* Pode ou não haver máquinas já registadas;
* Comunicação entre clientes e servidores através de protocolos de comunicação.


# 3. Design


## 3.1. Realização da Funcionalidade

* **Fluxo principal**

>(Nesta US é utilizado TCP como protocolo de comunicação)

  * O sistema central inicia a comunicação com máquinas de uma determinada linha de produção;
  * O sistema central recebe *HELLO request* das máquinas e verifica o seu id e armazena o seu endereço;
  * O sistema central envia um *ACK response* para a máquina;
  * O sistema central recebe um *MSG request* das máquinas.
  * O sistema central envia um *ACK response* para a máquina;



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
