# US2007
=======================================


# 1. Requisitos

**US2007** Como Gestor de Produção, eu pretendo exportar, para um ficheiro XML, toda a informação subjacente ao chão de fábrica (e.g. produtos, matérias-primas, máquinas, linhas de produção, categorias, ordens de produção, fichas de produção, lotes, consumos reais e efetivos, estornos, desvios, tempos de produção, entre outros).

* Este user story foi interpretado como exportação para um ficheiro, de todos os dados do chão de fábrica tendo em conta o filtro escolhido pelo Gestor de Produção e dos dados que este escolhe para estar presente nesse ficheiro.

# 2. Análise

* O perfil de utilizador é o Gestor de Produção que já deve estar registado;
* Pode ou não haver dados do chão de fábrica na base de dados;
* É criado um ficheiro do tipo XML com todos os dados escolhidos pelo gestor de produção e tendo em conta o filtro.


# 3. Design
![SD](US2007_SD.svg)



## 3.1. Realização da Funcionalidade

* **Fluxo principal**

    * O gestor de produção seleciona opção *"Export to XML file"* ;
    * o sistema vai pedir a escolha de um filtro e a seleção das informações que o gestor queira presente nesse ficheiro;
    * o gestor de produção introduz os dados pedidos;
    * o sistema vai buscar todos os dados à persistência;
    * o sistema cria o ficheiro, preenche-o com os dados acima escolhidos e informa o utilizador do sucesso da operação;
      * se o ficheiro já existir, osistema apaga e cria um novo com os bovos dados;

## 3.2. Diagrama de Classes

![CD](US2007_CD.svg)

* **ExportToXMLUI**: classe de user interface;
* **ExportToXMLController** : controller com as principais funcionalidades do user story;
* **XManagementService**: service class com funcionaldiaddes de consulta de produtos da persistência de cada um dos dados escolhidos pelo gestor de produção;
* **XRepository**: classe que agrega todos os obetos do tipo X, sendo que X é cada um dos dados selecionados;

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acoplamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Persistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

* O grupo decidiu usar a framework JAXB visto que permite mapear as classes existentes para XML.
* A framwork contém duas principais funcionalidades (*marshall e unmarshal*) sendo a única utilizada para já a funcionalidade marshal que permite carregar a estrutura das classes e escrever os dados das mesmas para um ficheiro.
* Para validar o ficheiro XML foi implementado um ficheiro XSD que teve de ser reescrito de maneira a estar em sincornização com a estrutra das classes.

# 5. Integração/Demonstração

* Visto que se utilizou a framework JAXB foi necessário adicionar anotações da biblioteca *javax.xml.bind.annotation*.
    
![anotations_usage_dataAdapter](anotations_usage_dataAdapter.svg)

* Para exportar os value objects que eram constituídos por *java.sql.Date* foi necessário implementar uma classe *DateAdapter*

![dateAdapter](dateAdapter.svg)

* Para validar o ficheiro XML criou-se um ficheiro xsd com o seguinte design

![xsdDiagram](xsdDiagram.svg)

  

# 6. Observações

* Inicialmente foi pensado exportar os objetos DTO mas, pensando na possibilidade de importar dados do ficheiro xml usando *unmarshal*, implementou-se de acordo com a estrutra das classes dos objetos.
  
* Este US juntamente com o 1010 foi efetuado feito em vídeoconferência com contributo igual de todos os membros da equipa.