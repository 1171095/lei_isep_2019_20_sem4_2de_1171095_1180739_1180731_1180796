# US5001
==========================================================================


# 1. Requisitos

**US5001** Como Serviço de Processamento de Mensagens (SPM), pretendo efetuar o processamento das mensagens disponíveis no sistema.

* Este user story foi interpretado como o processamento de mensagens armazenas na base de dados que ainda não estivessem tratadas, para um determinado intervalo de tempo.

# 2. Análise

* Pode ou não haver mensagens já criadas;
* O processamento de mensagens é feito por *Production Line*.
* O processamento de mensagens é feito pela linha de produção de forma independente e em paralelo.


# 3. Design


## 3.1. Realização da Funcionalidade

* **Fluxo principal**

>(Nesta US é utilizado UDP como protocolo de comunicação)

  * O SPM inicia a opção de processamento de mensagens por linha de produção;
  * O sistema vai buscar todas as mensagens não processadas numa linha de produção, à base de dados, num determinado intervalo de tempo;
  * O sistema processa as mensagens;



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
