# US3005
=======================================


# 1. Requisitos

**US3005** Como Gestor de Chão de Fábrica, eu pretendo consultar as notificações de erros de processamento por tratar.

* Este user story foi interpretado como uma visualização de notificações de erros de processamento não tratadas utilizando ou não um filtro na consulta.

# 2. Análise

* O perfil de utilizador é o Gestor de Chão de Fábrica que já deve estar registado;
* Pode ou não haver notificações de erros de processamento não tratadas;
* Existem diversos filtros para o utilizador escolher.
* São apresentados ao Gestor de Chão de Fábrica as (se existentes) notificações de erros de processamento por tratar.

# 3. Design

![US3005_ConsultErrorNotificationsUntreated_SD](US3005_SD.svg)

## 3.1. Realização da Funcionalidade

* **Fluxo principal**

	* O Gestor de Chão de Fábrica seleciona a opção *"Consult Error Notifications Untreated"*;
	* O Sistema pede ao Gestor um filtro (sem filtro, linha de produção, máquina, tipo de erro, estado);
	* O Gestor escolhe um filtro;
	* O sistema mostra uma lista com a consulta requerida.


## 3.2. Diagrama de Classes

![US3005_ConsultErrorNotificationsUntreated_CD](US3005_CD.svg)

* **ConsultErrorNotificationsUntreatedUI**: classe de user interface;
* **ConsultErrorNotificationsUntreatedController** : controller com as principais funcionalidades do user story;
* **ListErrorNotificationsService**: service class com funcionalidades de consulta de notificações de erro da presistência;
* **ErrorNotification**: classe notificações de erro com construtor e outros métodos;
* **ErrorNotificationRepository**: classe que agrega todas as notificações de erro.
* **ErrorNotificationDTO**: classe DTO(data transfer object) usada com o intuito de diminuir o acoplamento entre as classes de forma a que seja devolvido para a classe service um objeto DTO sem qualquer tipo de lógica usado para transferir dados entre diferentes camadas do sistema(e.g entre aplicação e persistência).

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo;
* Código centralizado em um único ponto, evitando duplicidade;
* Facilita a implementação de testes unitários;
* Diminui o acoplamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;
* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Persistência);
* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.

**Service Pattern**

* O Padrão Serviço pode ser aplicado no código como uma classe intermediária que disponibiliza métodos funcionais e necessários a vários Casos de Usos. Assim, não há a necessidade de instanciar  Controllers desconhecidos a um Caso de Uso específico, substituindo-se este pela nossa entidade Serviço. O uso deste padrão de design tem como vantagem também a  redução de possível código duplicado e promove a reutilização de métodos préviamente criados.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

* Implementação das classes necessárias (*ver secção 3.2*);

# 5. Integração/Demonstração

* É necessário implementar a funcionalidade de consultar as notificações de erro de processamento não tratadas com diferentes tipos de filtros;
* A utilização de outras classes é necessária. Isto porque os filtros estão relativos a classes como *ProductionLine* e *Machine*.

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
