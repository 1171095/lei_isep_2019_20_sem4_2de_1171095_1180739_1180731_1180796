# US1004
=======================================

# 1. Requisitos

**US1004** - Como Gestor de Projeto, eu pretendo que a equipa proceda à inicialização (bootstrap) de algumas matérias-primas.

* Este user story foi interpretado como inicializção de alguns objetos 'Matéria-primas' (Raw Material) no contexto de persistência, ou seja, adicionar matérias-primas à presistência para que possam ser usados nos restantes US´s que necessitem.

# 2. Análise

* Das primeiras funcionalidades a ser efetuadas do programa.
* O ator é o sistema.
* São inicalizadas várias matérias-primas com diferentes atributos;
* As matérias-primas inicalizados são colocadas na presistência para futura utilização.

# 3. Design

![US1004_AddNewRawMaterial_SD](US1004_AddNewRawMaterial_SD.png)

* **RawMaterialBootstrap**: classe que vai criar novas matérias-primas (Raw Material) e depois adiciona diretamente na presistencia (RawMaterialRepository)
* **RawMaterial**: classe referente à Matéria-prima, contém construtor e outros métodos.
* **RawMaterialRepository**: classe que comunica com a presistência.

## 3.1. Realização da Funcionalidade

* **Fluxo principal**
  * o sistema inicia o bootstrap;
  * instacia várias matérias-primas;
  * carrega as matérias-primas instanciadas para a presistência.

## 3.2. Diagrama de Classes

*sem diagrama de classes visto se tratar de bootstrap*

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acopolamento entre classes.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
