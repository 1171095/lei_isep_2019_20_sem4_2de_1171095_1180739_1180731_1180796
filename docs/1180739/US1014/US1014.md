# US1014
=======================================


# 1. Requisitos

**US1014** Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte a recepção de ficheiros de configuração.

# 2. Análise

* Para a realização deste user story o simulador de máquina foi preparado para receber e guardar ficheiros de configurações enviados pelo SCM

# 3. Design

![US1014](US1014.svg)

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
