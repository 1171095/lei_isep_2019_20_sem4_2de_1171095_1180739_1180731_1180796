# US3002
=======================================


# 1. Requisitos

**US3002** Como Gestor de Chão de Fábrica, eu pretendo definir uma nova linha de produção.

* Este user story foi interpretado como uma adição de uma nova linha de produção.

# 2. Análise

* O perfil de utilizador é o Gestor de Chão de Fábrica que já deve estar registado;
* Pode ou não haver linhas de produção já registadas;
* Adiciona-se à presistência uma nova linha de produção.


# 3. Design
![SD](US3002_SpecifyNewProductionLine_SD.jpg)



## 3.1. Realização da Funcionalidade

* **Fluxo principal**

    * O gestor de produção seleciona opção *"Specify new Production Line"* ;
    * o sistema vai pesquisar na presistência todas as linhas de produção;
    * o sistema apresenta uma lista das mesmas;
    * o gestor introduz os dados necesários para instanciar uma linha de produção;
    * a linha de produção é adcionada à presistência;

## 3.2. Diagrama de Classes

![CD](US3002_SpecifyNewProductionLine_CD.jpg)

* **SpecifyNewProductionLineUI**: classe de user interface;
* **SpecifyNewProductionLineController** : controller com as principais funcionalidades do user story;
* **ListProductionLineService**: service class com funcionalidades de consulta de linhas de produção da presistência;
* **ProductionLine**: classe linha de produção com construtor e outros métodos;
* **ProductionLineRepository**: classe que agrega todas as linhas de produção.
* **ProductionLineDTO**: classe DTO com apenas um construtor;
* ***interface* DTO**: interface DTO com método *toDTO()*.

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acoplamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Presistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
