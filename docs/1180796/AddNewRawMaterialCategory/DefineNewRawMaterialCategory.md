# US2002
=======================================


# 1. Requisitos

**US2002** Como Gestor de Produção, eu pretendo definir uma nova categoria de matérias-primas.

* Este user story foi interpretado como uma adição de uma nova categoria de matéria prima.

# 2. Análise

* O perfil de utilizador é o Gestor de Produção que já deve estar registado;
* Pode ou não haver categorias de matérias primas já registadas;
* Adiciona-se à presistência uma nova categoria de matéria prima.


# 3. Design
![SD](US2002_DefineNewRawMaterialCategory_SD.jpg)



## 3.1. Realização da Funcionalidade

* **Fluxo principal**

    * O gestor de produção seleciona opção *"Define new Raw Material Category"* ;
    * o sistema vai pesquisar na presistência todas as categorias de matéria prima;
    * o sistema apresenta uma lista das mesmas;
    * o gestor introduz os dados necesários para instanciar uma nova categoria de matéria prima;
    * a categoria de matéria prima é adcionada à presistência;

## 3.2. Diagrama de Classes

![CD](US2002_DefineNewRawMaterialCategory_CD.jpg)

* **DefineNewRawMaterialCategoryUI**: classe de user interface;
* **DefineNewRawMaterialCategoryController** : controller com as principais funcionalidades do user story;
* **ListRawMaterialCategoryService**: service class com funcionalidades de consulta de categorias de matéria prima da presistência;
* **RawMaterialCategory**: classe categoria de matéria prima com construtor e outros métodos;
* **RawMaterialCategoryRepository**: classe que agrega todas as categorias de matéria prima.
* **RawMaterialCategoryDTO**: classe DTO com apenas um construtor;
* ***interface* DTO**: interface DTO com método *toDTO()*.

## 3.3. Padrões Aplicados

**Repository Pattern**

* Permitir a troca do banco de dados utilizado sem afetar o sistema como um todo.
* Código centralizado em um único ponto, evitando duplicidade.

* Facilita a implementação de testes unitários.
* Diminui o acoplamento entre classes.

**DTO Pattern**

* DTO (Data Transfer Object), é um padrão de projeto de software usado para transferir dados entre subsistemas de um software;

* DTOs são frequentemente usados em conjunção com objetos de acesso a dados para obter dados de um banco de dados (Presistência);

* Camadas não-adjacentes à camada de dados da aplicação não podem acessar estes objetos e modificá-los.


## 3.4. Testes
*Nesta secção deve sistematizar como os testes foram concebidos para permitir uma correta aferição da satisfação dos requisitos.*

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*
