package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ConsumeMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface ConsumeMessageRepository extends DomainRepository<Long, ConsumeMessage> {
    default Optional<ConsumeMessage> findByConsumeMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ConsumeMessage> getAllConsumeMessages();

    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachine(Machine machine);

    public ConsumeMessage updateMessage(ConsumeMessage cm);
}
