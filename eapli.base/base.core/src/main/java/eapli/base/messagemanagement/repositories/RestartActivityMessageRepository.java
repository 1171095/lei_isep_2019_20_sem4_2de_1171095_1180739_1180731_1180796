package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.RestartActivityMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface RestartActivityMessageRepository extends DomainRepository<Long, RestartActivityMessage> {
    default Optional<RestartActivityMessage> findByRestartActivityMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<RestartActivityMessage> getAllRestartActivityMessages();

    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachine(Machine machine);

    public RestartActivityMessage updateMessage(RestartActivityMessage ram);
}
