package eapli.base.messagemanagement.application;

import eapli.base.errornotificationmanagement.application.ErrorNotificationManagementService;
import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.messagemanagement.domain.*;
import eapli.base.messagemanagement.repositories.*;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public class MessageManagementService {

    //private final AuthorizationService authz = AuthzRegistry.authorizationService();

    private final TransactionalContext txCtx = PersistenceContext.repositories().newTransactionalContext();
    private final ChargebackMessageRepository cbmRepoTxCtx = PersistenceContext.repositories().chargebackMessage(txCtx);
    private ConsumeMessageRepository cmRepoTxCtx = PersistenceContext.repositories().ConsumeMessage(txCtx);
    private EndActivityMessageRepository eamRepoTxCtx = PersistenceContext.repositories().endActivity(txCtx);
    private ErrorMessageRepository emRepoTxCtx = PersistenceContext.repositories().errorMessage(txCtx);
    private ProductionDeliveryMessageRepository pdmRepoTxCtx = PersistenceContext.repositories().productionDeliveryMessage(txCtx);
    private ProductionMessageRepository pmRepoTxCtx = PersistenceContext.repositories().productionMessage(txCtx);
    private RestartActivityMessageRepository ramRepoTxCtx = PersistenceContext.repositories().restartActivityMessage(txCtx);
    private StartMessageRepository samRepoTxCtx = PersistenceContext.repositories().startActivityMessage(txCtx);

    private final ChargebackMessageRepository chargebackMessageRepo = PersistenceContext.repositories().chargebackMessage();
    private final ConsumeMessageRepository consumeMessageRepo = PersistenceContext.repositories().ConsumeMessage();
    private final EndActivityMessageRepository endActivityMessageRepo = PersistenceContext.repositories().endActivity();
    private final ErrorMessageRepository errorMessageRepo = PersistenceContext.repositories().errorMessage();
    private final ProductionDeliveryMessageRepository productionDeliveryMessageRepo = PersistenceContext.repositories().productionDeliveryMessage();
    private final ProductionMessageRepository productionMessageRepo = PersistenceContext.repositories().productionMessage();
    private final RestartActivityMessageRepository restartActivityMessageRepo = PersistenceContext.repositories().restartActivityMessage();
    private final StartMessageRepository startActivityMessageRepo = PersistenceContext.repositories().startActivityMessage();


    private static final String SPLIT = ";";
    private static final String CHARGEBACK_MESSAGE_CODE = "CBM";
    private static final String CONSUME_MESSAGE_CODE = "CM";
    private static final String END_ACTIVITY_MESSAGE_CODE = "EAM";
    private static final String ERROR_MESSAGE_CODE = "EM";
    private static final String PRODUCTION_DELIVERY_MESSAGE_CODE = "PDM";
    private static final String PRODUCTION_MESSAGE_CODE = "PM";
    private static final String RESTART_MESSAGE_CODE = "RAM";
    private static final String START_MESSAGE_CODE = "SAM";

    public boolean createMessage(String messageString){
        String [] messageSplit = messageString.split(SPLIT);
        switch (messageSplit[0]){
            case CHARGEBACK_MESSAGE_CODE:
                if(messageSplit.length==6){
                    ChargebackMessage message = new ChargebackMessage(messageSplit);
                    chargebackMessageRepo.save(message);
                    return true;

                }
                break;

            case CONSUME_MESSAGE_CODE:
                if(messageSplit.length==6){
                    ConsumeMessage message = new ConsumeMessage(messageSplit);
                    consumeMessageRepo.save(message);
                    return true;
                }
                break;

            case END_ACTIVITY_MESSAGE_CODE:
                if(messageSplit.length==3 || messageSplit.length==4){
                    EndActivityMessage message = new EndActivityMessage(messageSplit);
                    endActivityMessageRepo.save(message);
                    return true;
                }
                break;

            case ERROR_MESSAGE_CODE:
                if(messageSplit.length==4){
                    ErrorMessage message = new ErrorMessage(messageSplit);
                    errorMessageRepo.save(message);
                    return true;
                }
                break;

            case PRODUCTION_DELIVERY_MESSAGE_CODE:
                if(messageSplit.length==6 || messageSplit.length==7){
                    ProductionDeliveryMessage message = new ProductionDeliveryMessage(messageSplit);
                    productionDeliveryMessageRepo.save(message);
                    return true;
                }
                break;

            case PRODUCTION_MESSAGE_CODE:
                if(messageSplit.length==6 || messageSplit.length==5){
                    ProductionMessage message = new ProductionMessage(messageSplit);
                    productionMessageRepo.save(message);
                    return true;
                }
                break;

            case RESTART_MESSAGE_CODE:
                if(messageSplit.length==3){
                    RestartActivityMessage message = new RestartActivityMessage(messageSplit);
                    restartActivityMessageRepo.save(message);
                    return true;
                }
                break;

            case START_MESSAGE_CODE:
                if(messageSplit.length==4){
                    StartActivityMessage message = new StartActivityMessage(messageSplit);
                    startActivityMessageRepo.save(message);
                    return true;
                }
                break;

            default:
                break;
        }
        return false;
    }

    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return chargebackMessageRepo.getChargebackUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return consumeMessageRepo.getConsumeUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return endActivityMessageRepo.getEndActivityUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<ErrorMessage> getErrorUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return errorMessageRepo.getErrorUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return productionDeliveryMessageRepo.getProductionDeliveryUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return productionMessageRepo.getProductionUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return restartActivityMessageRepo.getRestartActivityUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate){
        return startActivityMessageRepo.getStartActivityUnprocessedMessagesFromMachineBetweenDates(machine, initialDate, finalDate);
    }

    //
    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachine(Machine machine){
        return chargebackMessageRepo.getChargebackUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachine(Machine machine){
        return consumeMessageRepo.getConsumeUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachine(Machine machine){
        return endActivityMessageRepo.getEndActivityUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<ErrorMessage> getErrorUnprocessedMessagesFromMachine(Machine machine){
        return errorMessageRepo.getErrorUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachine(Machine machine){
        return productionDeliveryMessageRepo.getProductionDeliveryUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachine(Machine machine){
        return productionMessageRepo.getProductionUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachine(Machine machine){
        return restartActivityMessageRepo.getRestartActivityUnprocessedMessagesFromMachine(machine);
    }

    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachine(Machine machine){
        return startActivityMessageRepo.getStartActivityUnprocessedMessagesFromMachine(machine);
    }

    public void processChargebackMessageIterable(Iterable<ChargebackMessage> chargebackMessageIterable) {
        for (ChargebackMessage cm : chargebackMessageIterable) {
            txCtx.beginTransaction();
            cm.setProcessed();
            cbmRepoTxCtx.updateMessage(cm);
            txCtx.commit();
        }
    }
    public void processConsumeMessageIterable(Iterable<ConsumeMessage> consumeMessageIterable) {
        for (ConsumeMessage cm : consumeMessageIterable) {
            txCtx.beginTransaction();
            cm.setProcessed();
            cmRepoTxCtx.updateMessage(cm);
            txCtx.commit();
        }
    }
    public void processEndActivityMessageIterable(Iterable<EndActivityMessage> endActivityMessageIterable) {
        for (EndActivityMessage eam : endActivityMessageIterable) {
            txCtx.beginTransaction();
            eam.setProcessed();
            eamRepoTxCtx.updateMessage(eam);
            txCtx.commit();
        }
    }
    public void processErrorMessageIterable(Iterable<ErrorMessage> errorMessageIterable, ProductionLine productionLine) {
        for (ErrorMessage em : errorMessageIterable) {
            txCtx.beginTransaction();
            em.setProcessed();
            ErrorNotificationManagementService enms = new ErrorNotificationManagementService();
            MachineManagementService mms = new MachineManagementService();
            Optional<Machine> machine = mms.getMachineByID(em.getMachineID());
            ErrorNotificationUntreated enu = new ErrorNotificationUntreated(ErrorNotificationUntreated.getCounter(),
                    ErrorType.valueOf(em.getErrorType()),new ErrorDate(em.getMessageDate()) , productionLine, machine.get());

            enms.addErrorNotificationUntreated(enu);

            emRepoTxCtx.updateMessage(em);
            txCtx.commit();
        }
    }
    public void processProductionDeliveryMessageIterable(Iterable<ProductionDeliveryMessage> deliveryMessageIterable) {
        for (ProductionDeliveryMessage pdm : deliveryMessageIterable) {
            txCtx.beginTransaction();
            pdm.setProcessed();
            pdmRepoTxCtx.updateMessage(pdm);
            txCtx.commit();
        }
    }
    public void processProductionMessageIterable(Iterable<ProductionMessage> productionMessageIterable) {
        for (ProductionMessage pm : productionMessageIterable) {
            txCtx.beginTransaction();
            pm.setProcessed();
            pmRepoTxCtx.updateMessage(pm);
            txCtx.commit();
        }
    }
    public void processRestartActivityMessageIterable(Iterable<RestartActivityMessage> restartActivityMessageIterable) {
        for (RestartActivityMessage ram : restartActivityMessageIterable) {
            txCtx.beginTransaction();
            ram.setProcessed();
            ramRepoTxCtx.updateMessage(ram);
            txCtx.commit();
        }
    }
    public void processStartActivityMessageIterable(Iterable<StartActivityMessage> startActivityMessageIterable) {
        for (StartActivityMessage sam : startActivityMessageIterable) {
            txCtx.beginTransaction();
            sam.setProcessed();
            samRepoTxCtx.updateMessage(sam);
            txCtx.commit();
        }
    }
}
