package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ErrorMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface ErrorMessageRepository extends DomainRepository<Long, ErrorMessage> {
    default Optional<ErrorMessage> findByErrorMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ErrorMessage> getAllErrorMessages();

    public Iterable<ErrorMessage> getErrorUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<ErrorMessage> getErrorUnprocessedMessagesFromMachine(Machine machine);

    public ErrorMessage updateMessage(ErrorMessage em);
}
