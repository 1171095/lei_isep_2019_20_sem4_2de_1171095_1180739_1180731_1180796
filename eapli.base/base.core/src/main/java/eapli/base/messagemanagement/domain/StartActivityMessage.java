package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class StartActivityMessage extends Message {

    private Long productionOrderID; //context


    public StartActivityMessage(){
        super();
    }

    /**
     * Constructor for StartMessage with context
     * @param messageDate
     * @param productionOrderID
     */
    public StartActivityMessage(Calendar messageDate, Long machineID, Long productionOrderID) {
        super(messageDate, machineID);
        this.productionOrderID = productionOrderID;
    }

    /**
     * Constructor for StartMessage without context
     * @param messageDate
     */
    public StartActivityMessage(Calendar messageDate, Long machineID) {
        super(messageDate, machineID);
    }

    public StartActivityMessage(String[] messageSplit) {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        this.productionOrderID=Long.parseLong(messageSplit[3]);
    }
}
