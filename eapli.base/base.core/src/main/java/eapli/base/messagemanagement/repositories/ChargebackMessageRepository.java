package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface ChargebackMessageRepository extends DomainRepository<Long, ChargebackMessage> {
    default Optional<ChargebackMessage> findByChargebackMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ChargebackMessage> getAllChargebackMessages();

    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachine(Machine machine);

    public ChargebackMessage updateMessage(ChargebackMessage cm);
}
