package eapli.base.messagemanagement.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

@MappedSuperclass
public abstract class Message implements AggregateRoot<Long> {

    @Id
    @GeneratedValue
    private Long messageID;

    private Calendar messageDate;

    private Long machineID;

    private int processed;

    public Message() {
    }

    public Message(Calendar messageDate, Long machineID){
        this.messageDate = messageDate;
        this.machineID = machineID;
        processed=0;
    }
    //String messageDate format : ddMMyyyyHHmm
    public Message(String messageDate, Long machineID) {

        int ano = Integer.parseInt(messageDate.substring(4, 8));
        int mes = Integer.parseInt(messageDate.substring(2, 4));
        int dia = Integer.parseInt(messageDate.substring(0, 2));
        int hora = Integer.parseInt(messageDate.substring(8, 10));
        int minuto = Integer.parseInt(messageDate.substring(10, 12));

        this.messageDate = new GregorianCalendar(ano, mes, dia, hora, minuto);
        this.machineID = machineID;
        processed = 0;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return this.messageID;
    }

    public void setProcessed() {
        this.processed = 1;
    }

    public Calendar getMessageDate() {
        return messageDate;
    }

    public Long getMachineID() {
        return machineID;
    }
}
