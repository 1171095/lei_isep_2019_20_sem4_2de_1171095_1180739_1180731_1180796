package eapli.base.messagemanagement.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity
public class ChargebackMessage extends Message implements AggregateRoot<Long> {

    public ChargebackMessage() {
        super();
    }

    private int quantity;

    private Long depositID;

    private String rawMaterialID;

    public ChargebackMessage(Calendar messageDate, Long machineID, int quantity, Long depositID, String rawMaterialID) {

        super(messageDate, machineID);
        this.quantity=quantity;
        this.depositID=depositID;
        this.rawMaterialID=rawMaterialID;
    }

    public ChargebackMessage(String[] messageSplit)  {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        this.quantity = Integer.parseInt(messageSplit[3]);
        this.depositID = Long.parseLong(messageSplit[4]);
        this.rawMaterialID = messageSplit[5];

    }
}
