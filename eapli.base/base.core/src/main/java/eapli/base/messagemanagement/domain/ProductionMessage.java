package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class ProductionMessage extends Message{

    private String productID;

    private int quantity;

    private String batch;

    public ProductionMessage(){
        super();
    }

    /**
     * Constructor for ProductionMessage without batch
     * @param messageDate
     * @param productID
     * @param quantity
     */
    public ProductionMessage(Calendar messageDate, Long machineID, String productID, int quantity) {
        super(messageDate, machineID);
        this.productID = productID;
        this.quantity = quantity;
        this.batch = null;
    }

    /**
     * Constructor for ProductionMessage with batch
     * @param messageDate
     * @param productID
     * @param quantity
     * @param batch
     */
    public ProductionMessage(Calendar messageDate, Long machineID, String productID, int quantity, String batch) {
        super(messageDate, machineID);
        this.productID = productID;
        this.quantity = quantity;
        this.batch = batch;
    }

    public ProductionMessage(String[] messageSplit) {

        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        this.productID = messageSplit[3];
        this.quantity = Integer.parseInt(messageSplit[4]);
        if(messageSplit.length>5){
            batch=messageSplit[5];
        }
    }
}
