package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.StartActivityMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface StartMessageRepository extends DomainRepository<Long, StartActivityMessage> {
    default Optional<StartActivityMessage> findByStartActivityMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<StartActivityMessage> getAllStartActivityMessages();

    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachine(Machine machine);

    public StartActivityMessage updateMessage(StartActivityMessage sam);
}
