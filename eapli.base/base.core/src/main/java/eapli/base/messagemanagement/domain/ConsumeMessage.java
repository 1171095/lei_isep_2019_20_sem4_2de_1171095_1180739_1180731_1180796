package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;

@Entity
public class ConsumeMessage extends Message{

    private int quantity;

    private String rawMaterialID;

    private Long depositID;

    public ConsumeMessage (){
        super();
    }

    /**
     * Constructor Origin is deposit
     * @param messageDate
     * @param rawMaterialID
     * @param depositID
     */
    public ConsumeMessage(Calendar messageDate, Long machineID, String rawMaterialID, Long depositID) {
        super(messageDate, machineID);
        this.rawMaterialID = rawMaterialID;
        this.depositID = depositID;
    }

    /**
     * Constructor Origin is machine (doesnt save machine's id)
     * @param messageDate
     * @param rawMaterialID
     */
    public ConsumeMessage(Calendar messageDate, Long machineID,String rawMaterialID) {
        super(messageDate, machineID);
        this.rawMaterialID = rawMaterialID;
        this.depositID = null;
    }

    public ConsumeMessage(String[] messageSplit)  {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        this.quantity = Integer.parseInt(messageSplit[3]);
        this.depositID = Long.parseLong(messageSplit[4]);
        this.rawMaterialID = messageSplit[5];
    }
}
