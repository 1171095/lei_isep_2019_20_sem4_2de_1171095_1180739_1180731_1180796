package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class ProductionDeliveryMessage extends Message{

    private String productID;

    private int quantity;

    private Long depositID;

    private String batch;

    public ProductionDeliveryMessage(){
        super();
    }


    /**
     * Constructor for ProductionDeliveryMessage without batch
     * @param messageDate
     * @param productID
     * @param quantity
     */
    public ProductionDeliveryMessage(Calendar messageDate, Long machineID, String productID, int quantity, Long depositID) {
        super(messageDate, machineID);
        this.productID = productID;
        this.quantity = quantity;
        this.depositID = depositID;
        this.batch = null;
    }

    /**
     * Constructor for ProductionDeliveryMessage with batch
     * @param messageDate
     * @param productID
     * @param quantity
     * @param batch
     */
    public ProductionDeliveryMessage(Calendar messageDate, Long machineID, String productID, int quantity, Long depositID, String batch) {
        super(messageDate, machineID);
        this.productID = productID;
        this.quantity = quantity;
        this.depositID = depositID;
        this.batch = batch;
    }

    public ProductionDeliveryMessage(String[] messageSplit) {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        this.productID = messageSplit[3];
        this.quantity = Integer.parseInt(messageSplit[4]);
        this.depositID = Long.parseLong(messageSplit[5]);
        if(messageSplit.length>6){
            batch=messageSplit[6];
        }
    }
}
