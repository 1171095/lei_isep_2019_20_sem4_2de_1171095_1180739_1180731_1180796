package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ProductionMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface ProductionMessageRepository extends DomainRepository<Long, ProductionMessage> {
    default Optional<ProductionMessage> findByProductionMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ProductionMessage> getAllProductionMessages();

    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachine(Machine machine);

    public ProductionMessage updateMessage(ProductionMessage pm);
}
