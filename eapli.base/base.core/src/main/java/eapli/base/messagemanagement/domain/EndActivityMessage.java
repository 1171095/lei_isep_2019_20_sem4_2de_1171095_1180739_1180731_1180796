package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;

@Entity
public class EndActivityMessage extends Message{

    private String context;

    public EndActivityMessage(){
        super();
    }
    /**
     * Constructor for endActivityMessage without Context
     * @param messageDate
     */
    public EndActivityMessage(Calendar messageDate, Long machineID) {
        super(messageDate, machineID);
        this.context = null;
    }

    /**
     * Constructor for endActivityMessage with Context
     * @param messageDate
     * @param context
     */
    public EndActivityMessage(Calendar messageDate, Long machineID, String context) {
        super(messageDate, machineID);
        this.context = context;
    }

    public EndActivityMessage(String[] messageSplit) {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
        if(messageSplit.length>3){
            this.context = messageSplit[3];
        }

    }

}
