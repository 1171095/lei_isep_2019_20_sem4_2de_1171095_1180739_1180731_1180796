package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class RestartActivityMessage extends Message{

    public RestartActivityMessage(){
        super();
    }

    public RestartActivityMessage(Calendar messageDate, Long machineID) {
        super(messageDate, machineID);
    }

    public RestartActivityMessage(String[] messageSplit) {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );
    }
}
