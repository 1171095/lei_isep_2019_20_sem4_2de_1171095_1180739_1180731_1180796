package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ProductionDeliveryMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface ProductionDeliveryMessageRepository extends DomainRepository<Long, ProductionDeliveryMessage> {
    default Optional<ProductionDeliveryMessage> findByProductionDeliveryMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ProductionDeliveryMessage> getAllProductionDeliveryMessages();

    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachine(Machine machine);

    public ProductionDeliveryMessage updateMessage(ProductionDeliveryMessage pdm);
}
