package eapli.base.messagemanagement.domain;

import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
public class ErrorMessage extends Message{

    private String errorType;


    public ErrorMessage(){
        super();
    }

    public ErrorMessage(Calendar messageDate, Long machineID, String errorType) {
        super(messageDate, machineID);
        this.errorType=errorType;
    }

    public ErrorMessage(String[] messageSplit) {
        super(messageSplit[1],Long.parseLong(messageSplit[2]) );

        this.errorType = messageSplit[3];

    }

    public String getErrorType() {
        return errorType;
    }
}
