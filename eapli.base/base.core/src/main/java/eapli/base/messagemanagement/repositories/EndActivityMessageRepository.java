package eapli.base.messagemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.EndActivityMessage;
import eapli.framework.domain.repositories.DomainRepository;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public interface EndActivityMessageRepository extends DomainRepository<Long, EndActivityMessage> {
    default Optional<EndActivityMessage> findByEndActivityMessageID(Long id){
        return ofIdentity(id);
    };

    public Iterable<EndActivityMessage> getAllEndActivityMessages();

    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate);

    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachine(Machine machine);

    public EndActivityMessage updateMessage(EndActivityMessage eam);
}
