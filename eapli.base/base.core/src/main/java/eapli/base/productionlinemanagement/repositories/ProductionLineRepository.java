package eapli.base.productionlinemanagement.repositories;

import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ProductionLineRepository extends DomainRepository<Long, ProductionLine> {

    default Optional<ProductionLine> findByProductionLineID(Long id){ return ofIdentity(id); };

    public Iterable<ProductionLine> getListProductionLine();

    public void update(ProductionLine selectedElement);
}
