package eapli.base.productionlinemanagement.domain;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;


import java.util.List;
import java.util.Map;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import java.util.Set;

public class ProductionLineDTO {

    Long prodLineID;

    Map<Integer,Machine> lstMachines;

    public ProductionLineDTO(Long prodLineID, Map<Integer,Machine> lstMachines) {
        this.prodLineID = prodLineID;
        this.lstMachines = lstMachines;
    }

    public Long getProdLineID() { return prodLineID; }


    public Map<Integer,Machine> getLstMachines (){return this.lstMachines;}

    @Override
    public String toString() {
        return "ProductionLineDTO{" +
                "prodLineID=" + prodLineID +
                ", lstMachines=" + lstMachines.toString() +
                '}';
    }
}
