package eapli.base.productionlinemanagement.application;

import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListProductionLineDTOService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public Iterable<ProductionLineDTO> getProductionLineDTOS(Iterable<ProductionLine> lstProducts) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER,BaseRoles.POWER_USER, BaseRoles.SCM);
        ArrayList<ProductionLineDTO> lstProdDTO = new ArrayList<>();

        for(ProductionLine p: lstProducts){
            ProductionLineDTO pDTO = (ProductionLineDTO)p.toDTO();
            lstProdDTO.add(pDTO);
        }
        return lstProdDTO;
    }
}
