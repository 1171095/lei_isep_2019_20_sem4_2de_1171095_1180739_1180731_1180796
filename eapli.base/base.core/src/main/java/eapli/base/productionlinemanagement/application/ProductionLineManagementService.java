package eapli.base.productionlinemanagement.application;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProcessingStatus;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.*;


public class ProductionLineManagementService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    final TransactionalContext autoTx = PersistenceContext.repositories().newTransactionalContext();
    private final ProductionLineRepository repoTx = PersistenceContext.repositories().productionLine(autoTx);
    private final ProductionLineRepository repo = PersistenceContext.repositories().productionLine();

    public void registerProductionLine (Map<Integer,Machine> lstMachine){
        ProductionLine newPL = new ProductionLine(lstMachine);
        repo.save(newPL);
    }

    public Iterable<ProductionLine> listProductionLines(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER, BaseRoles.SCM, BaseRoles.SPM);
        return repo.getListProductionLine();

    }

    public Optional<ProductionLine> findByProductionLineID(Long id){
        return repo.findByProductionLineID(id);
    }

    public void updateProductionLine (ProductionLine pl){
        autoTx.beginTransaction();
        repoTx.update(pl);
        autoTx.commit();
    }



}
