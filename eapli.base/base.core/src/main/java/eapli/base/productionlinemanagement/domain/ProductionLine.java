package eapli.base.productionlinemanagement.domain;


import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.domain.Machine;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.*;

@Entity
@XmlType(propOrder = {"plID","machineMap"})
public class ProductionLine implements AggregateRoot<Long>, DTOable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long plID;

    @OneToMany(cascade = CascadeType.ALL)
    private Map<Integer,Machine> machineMap = new HashMap<>();


    private ProcessingStatus processingStatus;
    private ProcessingRequest processingRequest;


    public Map<Integer,Machine> getLstMachines() {
        return machineMap;
    }


    public ProductionLine(){};

    public ProductionLine(Map<Integer,Machine> map){
        this.machineMap = map;
        this.processingStatus = ProcessingStatus.ACTIVE;
        this.processingRequest = new ProcessingRequest();
    }

    @XmlElement(required = true, name = "productionLineID")
    public Long getPlID() {
        return plID;
    }

    @XmlTransient
    public ProcessingStatus getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(ProcessingStatus processingStatus) {
        this.processingStatus = processingStatus;
    }

    @XmlTransient
    public ProcessingRequest getProcessingRequest() {
        return processingRequest;
    }

    public void setPlID(Long plID) {
        this.plID = plID;
    }

    @XmlElement(required = true)
    public Map<Integer, Machine> getMachineMap() {
        return machineMap;
    }

    public void setMachineMap(Map<Integer, Machine> machineMap) {
        this.machineMap = machineMap;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductionLine that = (ProductionLine) o;
        return plID.equals(that.plID) &&
                machineMap.equals(that.machineMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plID, machineMap);
    }

    public void addMachine(Machine machine, int position) {
        this.machineMap.put(position,machine);
    }

    @Override
    public Object toDTO() {
        return new ProductionLineDTO(plID, machineMap);
    }

    @Override
    public String toString() {
        return "ProductionLine{" +
                "plID=" + plID +
                ", machineMap=" + machineMap +
                '}';
    }

    public Machine getMachineByID(Long machineID){
        for (Machine machine: this.getLstMachines().values()) {
            if(machine.identity().equals(machineID)){
                return machine;
            }
        }
        return null;
    }
}
