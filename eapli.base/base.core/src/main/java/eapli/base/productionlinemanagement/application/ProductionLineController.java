package eapli.base.productionlinemanagement.application;

import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.*;

public class ProductionLineController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineManagementService plms = new ProductionLineManagementService();
    private final MachineManagementService machineManagementService = new MachineManagementService();
    private ProductionLine productionLine;
    private Machine machine;
    public void registerProductionLine (Map<Integer,Machine> lstMachine){
        plms.registerProductionLine(lstMachine);
    }


    public Iterable<MachineDTO> listMachines(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        Iterable<Machine> listMachines = machineManagementService.listMachines();
        productionLine=new ProductionLine();
        return new MachineDTOListService().dtoMachines(listMachines);

    }
    public void addMachine(MachineDTO mDTO,int position){
        Optional<Machine> optionalMachine = machineManagementService.getMachineByID(mDTO.getId());
        if(!optionalMachine.isPresent())
            throw new IllegalArgumentException("invalid machine!");

        machine = optionalMachine.get();
        productionLine.addMachine(machine,position);
    }
    public void addMachineByID(Long id, int position){
        Optional<Machine> optionalMachine = machineManagementService.getMachineByID(id);
        if(!optionalMachine.isPresent())
            throw new IllegalArgumentException("invalid machine!");

        machine = optionalMachine.get();
        productionLine.addMachine(machine,position);
    }

    public void save(){
        new ProductionLineManagementService().registerProductionLine(productionLine.getLstMachines());

    }
    public ProductionLine productionLine(){return productionLine;}

    public Optional<ProductionLine> findByProductionLineID(Long id){
        return plms.findByProductionLineID(id);
    }

}
