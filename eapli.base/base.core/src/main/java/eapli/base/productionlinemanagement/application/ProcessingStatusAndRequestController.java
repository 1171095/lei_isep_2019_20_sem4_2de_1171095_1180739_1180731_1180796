package eapli.base.productionlinemanagement.application;

import eapli.base.productionlinemanagement.domain.ProcessingStatus;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.productmanagement.domain.Product;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

public class ProcessingStatusAndRequestController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductionLineManagementService plms = new ProductionLineManagementService();


    public Iterable<ProductionLine> getAllProductionLines(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return plms.listProductionLines();
    }

    public void save(ProductionLine pl){
        plms.updateProductionLine(pl);
    }


    public ProcessingStatus getProcessingStatusOfProductionLine (ProductionLine pl){
        return pl.getProcessingStatus();
    }

    public boolean checkIfHasRequest (ProductionLine pl){
        return pl.getProcessingRequest().HasRequest();
    }

}
