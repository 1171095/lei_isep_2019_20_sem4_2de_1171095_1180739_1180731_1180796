package eapli.base.productionlinemanagement.application;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.application.RawMaterialManagementService;

import java.util.*;

public class SpecifyNewProductionLineController {
    ProductionLineManagementService plms = new ProductionLineManagementService();

    public void registerProductionLine (Map<Integer,Machine> lstMachine){
        plms.registerProductionLine(lstMachine);
    }
}
