package eapli.base.productionlinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Embeddable
public class ProcessingRequest implements ValueObject {

    private boolean hasRequest;
    private Calendar initial_date;
    private Calendar final_date;

    public ProcessingRequest(){
        this.hasRequest = false;
        this.initial_date = new GregorianCalendar();
        this.final_date = new GregorianCalendar();
    }

    public boolean HasRequest() {
        return hasRequest;
    }

    public void setHasRequest(boolean hasRequest) {
        this.hasRequest = hasRequest;
    }

    public Calendar getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(Calendar initial_date) {
        this.initial_date = initial_date;
    }

    public Calendar getFinal_date() {
        return final_date;
    }

    public void setFinal_date(Calendar final_date) {
        this.final_date = final_date;
    }
}
