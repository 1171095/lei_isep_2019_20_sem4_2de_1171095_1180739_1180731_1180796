package eapli.base.consumemanagement.domain;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.framework.domain.model.AggregateRoot;
import javax.persistence.*;

import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class Consume implements AggregateRoot<Long> {

    @Id
    @GeneratedValue
    private Long consumeId;
    @ManyToOne
    private Deposit deposit;
    @ManyToOne
    private RawMaterial rawMaterial;
    private Quantity quantity;

    public Consume(){
    }

    public Consume(Deposit deposit, RawMaterial rawMaterial, Quantity quantity) {
        this.deposit = deposit;
        this.rawMaterial = rawMaterial;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consume consume = (Consume) o;
        return consumeId.equals(consume.consumeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consumeId);
    }

    @Override
    public boolean sameAs(Object other) {
        return this.equals(other);
    }

    @Override
    public Long identity() {
        return this.consumeId;
    }
}
