package eapli.base.consumemanagement.repositories;

import eapli.base.consumemanagement.domain.Consume;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ConsumeRepository extends DomainRepository<Long, Consume> {

    default Optional<Consume> findByConsumeID(Long id){
        return ofIdentity(id);
    };

    public Iterable<Consume> getAllConsumes();
}

