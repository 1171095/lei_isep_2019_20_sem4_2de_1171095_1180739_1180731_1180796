package eapli.base.consumemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class Quantity implements ValueObject {

    private double quantity;

    protected Quantity(){}

    public Quantity(double quantity){
        this.quantity = quantity;
    }

    public double value(){ return quantity;}
}
