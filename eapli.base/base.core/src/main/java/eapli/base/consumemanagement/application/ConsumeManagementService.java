package eapli.base.consumemanagement.application;

import eapli.base.consumemanagement.domain.Consume;
import eapli.base.consumemanagement.repositories.ConsumeRepository;
import eapli.base.consumemanagement.domain.Quantity;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Optional;

public class ConsumeManagementService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ConsumeRepository repo = PersistenceContext.repositories().consume();


    public void specifyConsume(Deposit deposit, RawMaterial rawMaterial, Quantity quantity){
        Consume newConsume = new Consume(deposit,  rawMaterial, quantity);
        repo.save(newConsume);


    }

    public Iterable<Consume> listConsumes(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return repo.getAllConsumes();

    }

    public Optional<Consume> getConsumeByID(Long id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if(repo.containsOfIdentity(id)){
            return repo.findByConsumeID(id);
        }
        throw new IllegalArgumentException("Invalid Consume");
    }
}
