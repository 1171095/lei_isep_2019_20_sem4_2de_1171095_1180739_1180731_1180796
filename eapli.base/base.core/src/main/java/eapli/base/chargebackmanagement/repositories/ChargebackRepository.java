package eapli.base.chargebackmanagement.repositories;

import eapli.base.chargebackmanagement.domain.Chargeback;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ChargebackRepository  extends DomainRepository<Long, Chargeback> {

    default Optional<Chargeback> findByChargebackID(Long id){
        return ofIdentity(id);
    };

    public Iterable<Chargeback> getAllChargebacks();
}
