package eapli.base.chargebackmanagement.domain;

import eapli.base.consumemanagement.domain.Quantity;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class Chargeback implements AggregateRoot<Long> {
    @Id
    @GeneratedValue
    private Long chargebackId;
    @ManyToOne
    private Deposit deposit;
    @ManyToOne
    private RawMaterial rawMaterial;
    private Quantity quantity;

    public Chargeback(){
    }

    public Chargeback(Deposit deposit, RawMaterial rawMaterial, Quantity quantity) {
        this.deposit = deposit;
        this.rawMaterial = rawMaterial;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chargeback chargeback = (Chargeback) o;
        return chargebackId.equals(chargeback.chargebackId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chargebackId);
    }

    @Override
    public boolean sameAs(Object other) {
        return this.equals(other);
    }

    @Override
    public Long identity() {
        return this.chargebackId;
    }
}
