package eapli.base.chargebackmanagement.application;

import eapli.base.chargebackmanagement.domain.Chargeback;
import eapli.base.chargebackmanagement.repositories.ChargebackRepository;
import eapli.base.consumemanagement.domain.Quantity;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Optional;

public class ChargebackManagementService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ChargebackRepository repo = PersistenceContext.repositories().chargeback();


    public void specifyChargeback(Deposit deposit, RawMaterial rawMaterial, Quantity quantity){
        Chargeback newChargeback = new Chargeback(deposit,  rawMaterial, quantity);
        repo.save(newChargeback);


    }

    public Iterable<Chargeback> listChargebacks(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return repo.getAllChargebacks();

    }

    public Optional<Chargeback> getChargebackByID(Long id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if(repo.containsOfIdentity(id)){
            return repo.findByChargebackID(id);
        }
        throw new IllegalArgumentException("Invalid Chargeback");
    }
}
