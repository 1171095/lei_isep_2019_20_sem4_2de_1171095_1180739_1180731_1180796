package eapli.base.productionformmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable
public class ProductionFormID implements ValueObject, Comparable<ProductionFormID> {
    private Long id;

    protected ProductionFormID(){}

    public ProductionFormID(Long id){
        this.id=id;


    }

    @Override
    public int compareTo(ProductionFormID productionFormID) {
        return this.id.compareTo(productionFormID.id);
    }
}
