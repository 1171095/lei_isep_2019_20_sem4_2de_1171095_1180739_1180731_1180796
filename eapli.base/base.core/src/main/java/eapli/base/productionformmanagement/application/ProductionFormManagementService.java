package eapli.base.productionformmanagement.application;


import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

public class ProductionFormManagementService {

    private final ProductionFormRepository repo = PersistenceContext.repositories().productionFile();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    
    public Iterable<ProductionForm> allProductionForms(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return repo.lstProdFile();
    }
    
    public ProductionForm registerProductionForm(ProductionForm productionForm){
       if(productionForm != null){
           repo.save(productionForm);
           return productionForm;
       }
       return null;
    }
}
