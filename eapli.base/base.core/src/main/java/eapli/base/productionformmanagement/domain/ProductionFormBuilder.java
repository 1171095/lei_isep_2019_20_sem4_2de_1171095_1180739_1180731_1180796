package eapli.base.productionformmanagement.domain;

import eapli.base.rawmaterialmanagement.domain.RawMaterial;

import java.util.ArrayList;
import java.util.List;
public class ProductionFormBuilder {

    private List<ItemUsed> lstItems;
    private double quantity;

    public ProductionFormBuilder(){
        lstItems = new ArrayList<>();
    }
    public ProductionForm builder(){
        ProductionForm pf = new ProductionForm();
        pf.setLstItemsUsed(lstItems);
        return pf;
    }

    public void addUsedRawMaterial(RawMaterial raw, double quant){
        this.lstItems.add(new ItemUsed(raw,quant));
    }

    public void setQuantity(double quantity){
        this.quantity = quantity;
    }


}
