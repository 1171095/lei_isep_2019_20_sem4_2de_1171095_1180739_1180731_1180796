package eapli.base.productionformmanagement.domain;


import eapli.base.productmanagement.domain.Product;
import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@Entity
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ProductionForm implements AggregateRoot<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProdForm;

    @ElementCollection
    private List<ItemUsed> lstItemsUsed = new ArrayList<>();


    public ProductionForm(){}

    public ProductionForm(List<ItemUsed> listItems){
        this.lstItemsUsed = listItems;
    }

    public void setLstItemsUsed(List<ItemUsed> lstItemsUsed) {
        this.lstItemsUsed = lstItemsUsed;
    }

    @XmlElement(required = true)
    public Long getIdProdForm() {
        return idProdForm;
    }

    @XmlElement(required = false)
    public List<ItemUsed> getLstItemsUsed() {
        return lstItemsUsed;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return null;
    }
}
