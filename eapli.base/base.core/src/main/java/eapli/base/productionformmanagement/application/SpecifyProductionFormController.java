package eapli.base.productionformmanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productionformmanagement.domain.ProductionFormBuilder;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.rawmaterialmanagement.application.ListRawMaterialDTOService;
import eapli.base.rawmaterialmanagement.application.RawMaterialManagementService;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SpecifyProductionFormController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    private final TransactionalContext txCtx = PersistenceContext.repositories().newTransactionalContext();
    private final ProductionFormRepository pfRep = PersistenceContext.repositories().productionFile(txCtx);
    private final ProductRepository productRepository = PersistenceContext.repositories().product(txCtx);

    private Product prod;
    private List<RawMaterialDTO> lstRawMaterials;
    private ProductionFormBuilder pfBuilder;
    private ProductionForm prodForm = new ProductionForm();

   public Iterable<ProductDTO> lstProducts(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);
        ArrayList<Product> lstProducts = (ArrayList) new ProductManagementService().listProductsWOutForm();

        if(lstProducts.isEmpty()){
            throw new IllegalArgumentException("All products have production form");

        }
        Iterable<ProductDTO> lstProductsDto = new ListProductDTOService().getProductDTOS(lstProducts);
        return lstProductsDto;
    }

    public void product(ProductDTO pDTO){
        Optional<Product> optProduct = new ProductManagementService().getProductByID(pDTO.getId());

        if(!optProduct.isPresent()){
            throw new IllegalArgumentException("Product id invalid");
        }

        prod = optProduct.get();
        this.pfBuilder = new ProductionFormBuilder();

    }

    public Iterable<RawMaterialDTO> lstRawMaterial(){
        Iterable<RawMaterial> lst = new RawMaterialManagementService().listRawMaterial();
        return lstRawMaterials = (ArrayList) new ListRawMaterialDTOService().rawMaterialDTO(lst);

    }


    public boolean saveProdFile(){
        prodForm = pfBuilder.builder();
        txCtx.beginTransaction();
        prodForm = pfRep.save(prodForm);
        prod.setForm(prodForm);
        productRepository.updateProduct(prod);
        txCtx.commit();
        return true;

    }
    public Iterable<RawMaterialDTO> setRawMaterialQuantity(RawMaterialDTO rmDTO, double quantity){
        Optional<RawMaterial> optRaw = new RawMaterialManagementService().getRawMaterialByID(rmDTO.getRawMaterialID());

        if(!optRaw.isPresent())
            throw new IllegalArgumentException("invalid raw material id");

        RawMaterial newRaw = optRaw.get();
        pfBuilder.addUsedRawMaterial(newRaw, quantity);

        this.lstRawMaterials.remove(rmDTO);
        return lstRawMaterials;


    }

    public String showProdForm(){
        return prodForm.toString() ;
    }
}
