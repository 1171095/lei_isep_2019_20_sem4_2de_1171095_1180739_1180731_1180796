package eapli.base.productionformmanagement.domain;

import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ItemUsed implements ValueObject {


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RAW_MATERIAL")
    private RawMaterial rawMaterial;
    private double quantity;

    protected ItemUsed(){}

    public ItemUsed(RawMaterial rawMaterial, double quantity){
        this.rawMaterial = rawMaterial;
        this.quantity = quantity;
    }

    @XmlElement(required = true)
    public RawMaterial getRawMaterial() {
        return rawMaterial;
    }

    @XmlElement(required = true)
    public double getQuantity() {
        return quantity;
    }
}
