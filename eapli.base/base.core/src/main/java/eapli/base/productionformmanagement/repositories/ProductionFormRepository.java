package eapli.base.productionformmanagement.repositories;

import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.framework.domain.repositories.DomainRepository;

public interface ProductionFormRepository extends DomainRepository<Long, ProductionForm> {
    public Iterable<ProductionForm> lstProdFile();
}
