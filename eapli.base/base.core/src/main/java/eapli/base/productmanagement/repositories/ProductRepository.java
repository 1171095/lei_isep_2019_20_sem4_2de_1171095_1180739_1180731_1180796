package eapli.base.productmanagement.repositories;

import eapli.base.productmanagement.domain.Product;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ProductRepository
        extends DomainRepository<String, Product> {

    default Optional<Product> findByProductionID(String id){
        return ofIdentity(id);
    };

    public Iterable<Product> findAllProducts();

    public Product updateProduct(Product prod);
}
