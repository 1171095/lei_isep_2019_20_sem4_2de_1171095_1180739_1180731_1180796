package eapli.base.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class CommercialCode implements ValueObject {

    private String code;

    protected CommercialCode(){}

    public CommercialCode(String code){
        this.code=code;

    }

    public void setCode(String code) {
        this.code = code;
    }

    @XmlElement(required = true)
    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommercialCode that = (CommercialCode) o;
        return Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public String value(){
        return code;
    }


}
