package eapli.base.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ProductCategory implements ValueObject {

    private String productCategory;

    protected ProductCategory(){}

    public ProductCategory(String productCategory){
        this.productCategory = productCategory;
    }

    @XmlElement(required = true)
    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String category) {
        this.productCategory = category;
    }

    public String value(){
        return productCategory;
    }


}
