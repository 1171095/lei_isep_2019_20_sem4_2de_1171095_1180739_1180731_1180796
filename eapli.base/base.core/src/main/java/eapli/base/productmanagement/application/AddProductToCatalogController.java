package eapli.base.productmanagement.application;

import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productmanagement.domain.*;

public class AddProductToCatalogController {
    private ProductManagementService pms = new ProductManagementService();

    public void registerProduct(String id, CommercialCode code, BriefDescription bd, Description desc, Unit unit, ProductCategory cat){
       pms.registerProduct(id,code, bd, desc,unit,cat);
    }


}
