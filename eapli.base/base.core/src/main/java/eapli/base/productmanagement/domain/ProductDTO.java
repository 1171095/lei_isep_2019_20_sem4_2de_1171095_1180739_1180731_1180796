package eapli.base.productmanagement.domain;

public class ProductDTO {
    private String id;
    private String brief;
    private String desc;
    private String commercialCode;
    private String unit;
    private String cat;
    private String productionForm;

    public ProductDTO(String idProduct, String commercialCode, String desc, String brief, String unit, String cat, String prodForm){
        this.id = idProduct;
        this.commercialCode = commercialCode;
        this.desc = desc;
        this.brief = brief;
        this.unit=unit;
        this.cat=cat;
        this.productionForm = prodForm;
    }

    public ProductDTO(String idProduct, String commercialCode, String desc, String unit, String cat, String brief){
        this.id = idProduct;
        this.commercialCode = commercialCode;
        this.desc = desc;
        this.brief = brief;
        this.unit=unit;
        this.cat=cat;
        this.productionForm = "";
    }

    public String getId() {
        return id;
    }

    public String getBrief() {
        return brief;
    }

    public String getDesc() {
        return desc;
    }

    public String getUnit() {
        return unit;
    }

    public String getCat() {
        return cat;
    }

    public String getCommercialCode() {
        return commercialCode;
    }

    public String getProductionForm() {
        return productionForm;
    }

    @Override
    public String toString(){
        return String.format(this.id + ";" + this.commercialCode + ";" + this.desc + ";" + this.brief + ";" + this.unit + ";" + this.cat + ";" + this.productionForm);
    }
}
