package eapli.base.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class BriefDescription implements ValueObject {

    @XmlElement(required = true)
    private String briefDescription;

    protected BriefDescription(){}

    public BriefDescription(String briefDescription){
        this.briefDescription = briefDescription;
    }

    public String value(){
        return briefDescription;
    }


//   // public String getBriefDescription() {
//        return briefDescription;
//    }

    public void setBriefDescription(String briefDescription) {
        this.briefDescription = briefDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BriefDescription that = (BriefDescription) o;
        return Objects.equals(briefDescription, that.briefDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(briefDescription);
    }
}
