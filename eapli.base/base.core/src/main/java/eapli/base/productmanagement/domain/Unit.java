package eapli.base.productmanagement.domain;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable

public class Unit {

    @XmlElement(required = true)
    private String unit;

    public Unit(){}

    public Unit(String unit){
        this.unit = unit;
    }


    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String value() {
        return unit;
    }
}
