package eapli.base.productmanagement.application;

import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListProductDTOService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();


    public Iterable<ProductDTO> getProductDTOS(Iterable<Product> lstProducts) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);
        ArrayList<ProductDTO> lstProdDTO = new ArrayList<>();

        for(Product p: lstProducts){
            ProductDTO pDTO = (ProductDTO)p.toDTO();
            lstProdDTO.add(pDTO);
        }
        return lstProdDTO;
    }
}
