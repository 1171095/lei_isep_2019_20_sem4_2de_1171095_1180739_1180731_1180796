package eapli.base.productmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Description implements ValueObject {

    private String description;

    protected Description(){}

    public Description(String description){

        this.description = description;
    }

    public String value(){
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Description that = (Description) o;
        return Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public String toString() {
        return description;
    }

    public boolean taVazio(){
        return description.isEmpty();
    }

    @XmlElement(required = true)
    private String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = description;
    }


}
