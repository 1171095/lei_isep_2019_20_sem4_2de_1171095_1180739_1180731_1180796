package eapli.base.productmanagement.application;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.productmanagement.repositories.ProductRepository;

import java.util.List;

public class ListProductsWOutFormController {

    private ProductManagementService pms = new ProductManagementService();
    private ListProductDTOService dtoService = new ListProductDTOService();

    public Iterable<ProductDTO> getLstProductsWOutForm() {
        Iterable<Product> lstProducts = this.pms.listProductsWOutForm();
        return this.dtoService.getProductDTOS(lstProducts);
    }
}
