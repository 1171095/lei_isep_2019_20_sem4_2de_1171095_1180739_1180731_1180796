package eapli.base.productmanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productmanagement.domain.*;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;

public class ProductManagementService{

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProductRepository repo = PersistenceContext.repositories().product();
    final TransactionalContext autoTx = PersistenceContext.repositories().newTransactionalContext();
    private final ProductRepository repoAtx = PersistenceContext.repositories().product(autoTx);

    public Iterable<Product> listProductsWOutForm(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,BaseRoles.PRODUCTION_MANAGER);
        Iterator<Product> it = this.repo.findAllProducts().iterator();
        ArrayList<Product> listProd = new ArrayList<>();
        while(it.hasNext()){
            Product p = it.next();
            if ((!p.hasForm())){
                listProd.add(p);
            }
        }
        return listProd;
    }
    public Iterable<Product> listAll(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);
        return repo.findAllProducts();
    }

    public void registerProduct(String id, CommercialCode cc, BriefDescription bd, Description desc,Unit unit, ProductCategory cat){
        Product newProduct = new Product(id,cc,bd,desc,unit,cat);
        if(newProduct != null && !repo.containsOfIdentity(id)){
            try{
                repo.save(newProduct);
            } catch(Exception  ex){
                System.out.println("Error saving product");

            }

        }

    }


    public Optional<Product> getProductByID(String id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if(repo.containsOfIdentity(id)){
            return repo.findByProductionID(id);
        }
        throw new IllegalArgumentException("Product production id not valid");
    }

    public Product updateProduct(Product prod){
        if(prod != null){
            return repoAtx.updateProduct(prod);

        }
        return null;
    }
}
