package eapli.base.productmanagement.application;

import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productmanagement.domain.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImportCatalogFromCSVController {

    private static final String SPLIT = ";";
    private static final String OUTPUT_FILE ="\\" + "ImportProductsError_" + (new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss")).format(new Date()) + ".csv";

    private final AddProductToCatalogController productController = new AddProductToCatalogController();
    private File path;

    private int countSuccess;
    private int countFail;
    private String errorFile;

    public boolean selectInputFile(String fileName){
        this.path = new File(fileName + ".csv");

        try(BufferedReader reader = new BufferedReader(new FileReader(this.path))){

        }catch(FileNotFoundException fileEx){
            return false;
        }catch(IOException ioEx){
            return false;
        }
        return true;
    }

    public String importCatalog(){
        try(BufferedReader reader = new BufferedReader(new FileReader(this.path))){
            String line;
            String[] aux;

            reader.readLine();

            while((line = reader.readLine())!= null){
                aux = line.split(SPLIT);
                if(aux.length == 6){
                    String prodCode = aux[0];
                    String commercialCode = aux[1];
                    String briefDesc = aux[2];
                    String desc = aux[3];
                    String unit = aux[4];
                    String cat = aux[5];

                    this.productController.registerProduct(prodCode,new CommercialCode(commercialCode), new BriefDescription(briefDesc),new Description(desc)
                            ,new Unit(unit), new ProductCategory(cat));
                    this.countSuccess++;


                }else{
                    this.countFail ++;
                    this.errorFile += line + "\n";
                }
            }

        }catch(FileNotFoundException fileEx){
            fileEx.printStackTrace();
        }catch(IOException ioEx){
           ioEx.printStackTrace();
        }
        if(countFail!=0)
            errorFile();
        return String.format("%d products catalogued, %d products failed", this.countSuccess, this.countFail);

    }

    private void errorFile(){
        String out = "ProdCode;CommCode;Brief;Desc;Unit;Cat\n";
        out += this.errorFile;
        String outputFile = this.path.getParent() + OUTPUT_FILE;
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))){
            writer.write(out);
        }catch(IOException ioEx){
            ioEx.printStackTrace();
        }
    }
}
