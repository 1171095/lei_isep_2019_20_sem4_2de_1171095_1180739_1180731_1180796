package eapli.base.productmanagement.domain;

import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

/*
 entity class 'Product'
 */
@Entity
@XmlType(propOrder = {"productionID" , "commercialID" , "briefDescription", "productDescription", "unit" , "productCategory","productionForm"})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Product implements AggregateRoot<String>, DTOable
{

    @Id
    @Column(length = 15)
    private String productionID;
    private CommercialCode commercialID;
    private BriefDescription briefDescription;
    private Description productDescription;
    private Unit unit;
    private ProductCategory productCategory;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = true, name = "PROD_FORM")
    private ProductionForm productionForm;


    /**
     * empty constructor for ORM
     */
    public Product(){}

    public Product(String id, CommercialCode commercialID, BriefDescription briefDescription, Description productDescription, Unit unit, ProductCategory productCategory) {
        this.productionID = id;
        this.commercialID = commercialID;
        this.briefDescription = briefDescription;
        this.productDescription = productDescription;
        this.unit = unit;
        this.productCategory = productCategory;
        this.productionForm = null;
    }

    /**
     * overwritten equals method
     * @param o object to be compared
     * @return true if same object false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(productionID, product.productionID);
    }

    @XmlElement(required = true)
    public String getProductionID() {
        return productionID;
    }

    @XmlElement(required = true)
    public CommercialCode getCommercialID() {
        return commercialID;
    }

    @XmlElement(required = true)
    public BriefDescription getBriefDescription() {
        return briefDescription;
    }

    @XmlElement(required = true)
    public Description getProductDescription() {
        return productDescription;
    }

    @XmlElement(required = true)
    public Unit getUnit() {
        return unit;
    }

    @XmlElement(required = true)
    public ProductCategory getProductCategory() {
        return productCategory;
    }

    @XmlElement(required = true)
    public ProductionForm getProductionForm() {
        return productionForm;
    }


    /**
     * overwritten hashCode method
     * @return object hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(productionID);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String identity() {
        return this.productionID;
    }

    @Override
    public Object toDTO() {
        if (hasForm()) {
            return new ProductDTO(productionID, commercialID.value(), productDescription.value(),
                    briefDescription.value(),unit.value(), productCategory.value(), productionForm.toString());
        } else {
            return new ProductDTO(productionID, commercialID.value(), productDescription.value(),
                    briefDescription.value(),unit.value(), productCategory.value());
        } }

    public boolean hasForm() {
        return (this.productionForm != null);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productionID='" + productionID + '\'' +
                ", commercialID=" + commercialID +
                ", briefDesc=" + briefDescription +
                ", desc=" + productDescription +
                ", unit=" + unit +
                ", category=" + productCategory +
                ", form=" + productionForm +
                '}';
    }

    public boolean setForm(ProductionForm form) {
        this.productionForm = form;
        return true;
    }

}
