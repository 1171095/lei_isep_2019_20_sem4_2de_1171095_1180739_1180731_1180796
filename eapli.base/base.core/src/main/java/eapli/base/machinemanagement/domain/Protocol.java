package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;

@Embeddable

public class Protocol implements ValueObject{
    private String protocol;

    protected Protocol(){}

    public Protocol(String protocol){

        this.protocol = protocol;
    }

    public String value(){
        return protocol;
    }

    public boolean taVazio(){
        return protocol.isEmpty();
    }
}
