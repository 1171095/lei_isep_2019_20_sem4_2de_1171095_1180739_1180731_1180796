package eapli.base.machinemanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.domain.*;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.repositories.TransactionalContext;

public class DefiningNewMachineController {
    private ProductionLineManagementService productionLineManagementService = new ProductionLineManagementService();
    private final TransactionalContext txCtx = PersistenceContext.repositories().newTransactionalContext();
    private final ProductionLineRepository productionLineRepository = PersistenceContext.repositories().productionLine(txCtx);
    public Machine definingNewMachine(SerialNumber serialNumID, Description desc, InstallationDate instDate,
                                   Brand brand, Model model, InternalCode ic){
        if(serialNumID == null || serialNumID.taVazio() || desc == null || desc.taVazio() ||
                instDate == null || brand == null || brand.taVazio()
                || model == null || model.taVazio()|| ic == null || ic.taVazio()){
            return null;
        }
        return new MachineManagementService().definingMachine(serialNumID, desc, instDate, brand, model, ic);
    }

    public boolean addMachineToProdLine(ProductionLine selectedElement, Machine machine, int pos) {
        txCtx.beginTransaction();
        selectedElement.addMachine(machine,pos);
        productionLineRepository.update(selectedElement);
        txCtx.commit();
        return true;

    }
}
