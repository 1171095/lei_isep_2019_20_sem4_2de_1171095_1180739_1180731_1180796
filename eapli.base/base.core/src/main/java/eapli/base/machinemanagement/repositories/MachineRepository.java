package eapli.base.machinemanagement.repositories;

import eapli.base.machinemanagement.domain.Machine;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface MachineRepository extends DomainRepository<Long, Machine> {

    default Optional<Machine> findByMachineID(Long id){
        return ofIdentity(id);
    };

    public Iterable<Machine> getAllMachines();

}
