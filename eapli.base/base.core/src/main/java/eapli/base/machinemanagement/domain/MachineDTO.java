package eapli.base.machinemanagement.domain;

public class MachineDTO {

    private Long id;
    private String internalCode;
    private String serialNumber;
    private String description;
    private String installationDate;
    private String brand;
    private String model;



    public MachineDTO(Long id, String internalCode, String serialNumber, String description, String installationDate, String brand, String model) {
        this.id = id;
        this.internalCode = internalCode;
        this.serialNumber = serialNumber;
        this.description = description;
        this.installationDate = installationDate;
        this.brand = brand;
        this.model = model;

    }

    public Long getId() {
        return id;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getInstallationDate() {
        return installationDate;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }


}
