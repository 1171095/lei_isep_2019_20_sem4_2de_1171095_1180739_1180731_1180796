package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Model implements ValueObject {

    private String model;

    protected Model(){}

    public Model(String model){

        this.model = model;
    }

    public String value(){
        return model;
    }

    public boolean taVazio(){
        return model.isEmpty();
    }

    @Override
    public String toString() {
        return model;
    }

    @XmlElement(required = true)
    public String getModel() {
        return model;
    }
}
