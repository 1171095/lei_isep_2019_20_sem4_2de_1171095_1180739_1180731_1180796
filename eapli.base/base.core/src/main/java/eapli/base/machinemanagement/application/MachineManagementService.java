package eapli.base.machinemanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.domain.*;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productmanagement.domain.Description;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import javax.crypto.Mac;
import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.Set;

public class MachineManagementService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MachineRepository repo = PersistenceContext.repositories().machine();

    public boolean updateMachine(Machine machine){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        if (machine!=null){
            repo.save(machine);
            return true;
        }
        return false;
    }
    public Machine definingMachine(SerialNumber serialNumID, Description desc, InstallationDate instDate,
                                Brand brand, Model model, InternalCode ic){
        Machine newMachine = new Machine(serialNumID,desc,instDate,brand,model, ic);
        if (newMachine!=null){
            repo.save(newMachine);
            return newMachine;
        }
        return null;
    }
    public Iterable<Machine> listMachines(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repo.findAll();
    }


    public Optional<Machine> getMachineByID(Long id) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER, BaseRoles.SPM);
        if(repo.containsOfIdentity(id)){
            return repo.findByMachineID(id);
        }
        throw new IllegalArgumentException("invalid machine id");
    }

    public boolean removeConfigRequestFromMachine(Machine machine){
        machine.setRequestedConfiguration(null);
        return updateMachine(machine);
    }

}
