package eapli.base.machinemanagement.domain;

import eapli.base.xmlmanagement.DateAdapter;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class InstallationDate implements ValueObject {
    private Date instDate;

    protected InstallationDate(){}

    public InstallationDate(Date instDate){
        this.instDate=instDate;
    }

    public String value(){ return instDate.toString();}

    @Override
    public String toString() {
        return instDate.toString();
    }

    @XmlElement(required = true, name = "installationDate")
    @XmlJavaTypeAdapter(DateAdapter.class)
    public Date getInstDate() {
        return instDate;
    }
}
