package eapli.base.machinemanagement.application;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MachineDTOListService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public Iterable<MachineDTO> dtoMachines(Iterable<Machine> listMachines) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        List<MachineDTO> listMachineDTO = new ArrayList<>();

        for(Machine m : listMachines){
            MachineDTO maquinaDTO = (MachineDTO) m.toDTO();
            listMachineDTO.add(maquinaDTO);
        }

        return listMachineDTO;
    }

    public Set<MachineDTO> setOfMachines (Set<Machine> lstM){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        Set<MachineDTO> lstMachineDTO = new HashSet<>();

        for(Machine m : lstM){
            MachineDTO maquinaDTO = (MachineDTO) m.toDTO();
            lstMachineDTO.add(maquinaDTO);
        }

        return lstMachineDTO;
    }
}
