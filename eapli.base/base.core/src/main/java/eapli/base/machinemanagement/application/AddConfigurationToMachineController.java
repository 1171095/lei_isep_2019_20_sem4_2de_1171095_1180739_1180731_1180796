package eapli.base.machinemanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.domain.Configuration;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

public class AddConfigurationToMachineController {

    private final MachineManagementService mms = new MachineManagementService();

    public void addConfigurationToMachine (Machine machine, Configuration configuration) {

        if (machine == null || configuration == null){
            return;
        }

        machine.addConfiguration(configuration);

        mms.updateMachine(machine);

    }
}
