package eapli.base.machinemanagement.domain;


import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@XmlType(propOrder = {"machineID","internalCode","serialNumID","desc","instDate","brand","model"})
public class Machine implements AggregateRoot<Long>, DTOable {

    @Id
    @GeneratedValue
    private Long machineID;
    private InternalCode internalCode;
    private SerialNumber serialNumID;
    private Description desc;
    private InstallationDate instDate;
    private Brand brand;
    private Model model;
    @ElementCollection
    private Set<Configuration> configurations;
    private File requestedConfiguration;

    /**
     * empty constructor for ORM
     */
    public Machine(){}

    /**
     *  @param serialNumID
     * @param desc
     * @param instDate installation date of the machine
     * @param brand brand of the machine
     * @param model model of the machine
     * @param internalCode
     */
    public Machine(SerialNumber serialNumID, Description desc, InstallationDate instDate,
                   Brand brand, Model model, InternalCode internalCode, Set<Configuration> configurations){
        this.internalCode = internalCode;
        this.serialNumID=serialNumID;
        this.desc=desc;
        this.instDate=instDate;
        this.brand=brand;
        this.model=model;
        this.configurations = configurations;
        this.requestedConfiguration = null;

    }

    /**
     *  @param serialNumID
     * @param desc
     * @param instDate installation date of the machine
     * @param brand brand of the machine
     * @param model model of the machine
     * @param internalCode
     */
    public Machine(SerialNumber serialNumID, Description desc, InstallationDate instDate,
                   Brand brand, Model model, InternalCode internalCode){
        this.internalCode = internalCode;
        this.serialNumID=serialNumID;
        this.desc=desc;
        this.instDate=instDate;
        this.brand=brand;
        this.model=model;
        this.configurations = new HashSet<>();
        this.requestedConfiguration = null;

    }

    /**
     * overwritten equals method
     * @param o object to be compared
     * @return true if same object false if not
     */


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Machine machine = (Machine) o;
        return machineID.equals(machine.machineID);
    }

    /**
     * overwritten hashCode method
     * @return object hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(machineID);
    }


    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return this.machineID;
    }

    @Override
    public Object toDTO() {
        return new MachineDTO(this.machineID,internalCode.value(),serialNumID.value(),desc.value(),instDate.value(),brand.value(),model.value());
    }

    public boolean addConfiguration(Configuration configuration){
        return configurations.add(configuration);
    }

    @Override
    public String toString() {
        return "Machine{" +
                "machineID=" + machineID +
                ", internalCode=" + internalCode +
                ", serialNumID=" + serialNumID +
                ", desc=" + desc +
                ", instDate=" + instDate +
                ", brand=" + brand +
                ", model=" + model +
                ", configurations=" + configurations +
                '}'+"\n";
    }

    @XmlElement(required = true)
    public Long getMachineID() {
        return machineID;
    }
    @XmlElement(required = true)
    public InternalCode getInternalCode() {
        return internalCode;
    }
    @XmlElement(required = true, name = "serialNumber")
    public SerialNumber getSerialNumID() {
        return serialNumID;
    }
    @XmlElement(required = true, name = "description")
    public Description getDesc() {
        return desc;
    }
    @XmlElement(required = true,name = "installationDate")
    public InstallationDate getInstDate() {
        return instDate;
    }
    @XmlElement(required = true)
    public Brand getBrand() {
        return brand;
    }
    @XmlElement(required = true)
    public Model getModel() {
        return model;
    }

    public boolean hasRequestedConfiguration(){
        return this.requestedConfiguration!=null;
    }

    @XmlTransient
    public File getRequestedConfiguration() {
        return requestedConfiguration;
    }
    @XmlTransient
    public Set<Configuration> getConfigurations() {
        return configurations;
    }

    public void setRequestedConfiguration(Configuration requestedConfiguration) {
        this.requestedConfiguration = requestedConfiguration.getFile();
    }
}
