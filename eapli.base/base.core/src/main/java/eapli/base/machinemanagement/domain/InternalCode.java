package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class InternalCode implements ValueObject {
    private String internalCode;

    protected InternalCode(){

    }

    public InternalCode(String internalCode){
        this.internalCode = internalCode;
    }

    public String value() {
        return internalCode;
    }

    public boolean taVazio(){
        return internalCode.isEmpty();
    }

    @Override
    public String toString() {
        return internalCode;
    }

    @XmlElement(required = true)
    public String getInternalCode() {
        return internalCode;
    }
}
