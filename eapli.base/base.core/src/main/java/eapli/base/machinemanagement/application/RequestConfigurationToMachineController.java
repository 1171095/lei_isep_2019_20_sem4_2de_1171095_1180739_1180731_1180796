package eapli.base.machinemanagement.application;

import eapli.base.machinemanagement.domain.Configuration;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.machinemanagement.repositories.MachineRepository;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RequestConfigurationToMachineController {
    private MachineManagementService machineManagementService;
    private MachineDTOListService machineDTOListService;
    private Machine machine;

    public Iterable<MachineDTO> getMachines() {
        machineManagementService = new MachineManagementService();
        Iterable<Machine> listMachines = machineManagementService.listMachines();
        machineDTOListService = new MachineDTOListService();
        return machineDTOListService.dtoMachines(listMachines);
    }

    public Set<Configuration> getMachineConfigurations(Long id) {
        machine = machineManagementService.getMachineByID(id).get();
        return machine.getConfigurations();
    }

    public void setRequestedConfiguration(Configuration configuration) {
        machine.setRequestedConfiguration(configuration);
        machineManagementService.updateMachine(machine);
    }
}
