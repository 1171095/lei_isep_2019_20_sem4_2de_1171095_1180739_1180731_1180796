package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.io.File;

@Embeddable
public class Configuration implements ValueObject{
    private String description;
    private File file;

    protected Configuration(){}

    public Configuration(String description){
        this.description = description;
    }

    public Configuration(String description, File file){
        this.description = description;
        this.file = file;
    }

    public String value(){
        return description;
    }

    public boolean taVazio(){
        return description.isEmpty();
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "description='" + description + '\'' +
                ", fileName='" + file.getName() + '\'' +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public File getFile() {
        return file;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
