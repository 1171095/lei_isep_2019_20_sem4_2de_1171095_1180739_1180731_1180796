package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class SerialNumber implements ValueObject {
    private String serialCod;

    protected SerialNumber(){}

    public SerialNumber(String serialCod){
        this.serialCod=serialCod;
    }

    public String value(){ return serialCod;}

    public boolean taVazio(){
        return serialCod.isEmpty();
    }

    @XmlElement(required = true, name = "serialNumber")
    public String getSerialCod() {
        return serialCod;
    }

    @Override
    public String toString() {
        return serialCod;
    }
}
