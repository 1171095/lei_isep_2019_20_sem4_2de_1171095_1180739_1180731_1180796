package eapli.base.machinemanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Brand implements ValueObject {

    private String brand;

    protected Brand(){}

    public Brand(String brand){

        this.brand = brand;
    }

    public String value(){
        return brand;
    }

    public boolean taVazio(){
        return brand.isEmpty();
    }

    @Override
    public String toString() {
        return brand ;
    }

    @XmlElement(required = true)
    public String getBrand() {
        return brand;
    }
}
