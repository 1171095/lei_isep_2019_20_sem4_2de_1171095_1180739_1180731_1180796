package eapli.base.depositmanagement.application;


import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.depositmanagement.repositories.DepositRepository;
import eapli.base.productmanagement.domain.Description;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Optional;

public class DepositManagementService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final DepositRepository repo = PersistenceContext.repositories().deposit();


    public void specifyDeposit(Description description){
        Deposit newDeposit = new Deposit(description);
        repo.save(newDeposit);


    }

    public Iterable<Deposit> listDeposits(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return repo.getAllDeposits();

    }

    public Optional<Deposit> getDepositByID(Long id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if(repo.containsOfIdentity(id)){
            return repo.findByDepositID(id);
        }
        throw new IllegalArgumentException("Invalid deposit");
    }


}
