package eapli.base.depositmanagement.application;

import eapli.base.productmanagement.domain.Description;

public class SpecifyNewDepositController {
    private DepositManagementService dms = new DepositManagementService();

    public void specifyNewDeposit(Description desc){
        dms.specifyDeposit(desc);
    }
}
