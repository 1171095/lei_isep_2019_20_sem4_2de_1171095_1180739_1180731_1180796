package eapli.base.depositmanagement.domain;

import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Entity
public class    Deposit implements AggregateRoot<Long>, DTOable {

    @Id
    @GeneratedValue
    private Long depositID; // deposit's code (entity ID)
    private Description description; // deposit's description

    /**
     * empty constructor for ORM
     */
    public Deposit(){}

    /**
     *
     * @param description deposit's description
     */
    public Deposit(Description description) {
        this.description = description;
    }

    /**
     * overwritten equals method
     * @param o object to be compared
     * @return true if same object false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return Objects.equals(depositID, deposit.depositID);
    }

    @XmlElement(required = true)
    public Long getDepositID() {
        return depositID;
    }

    @XmlElement(required = true)
    public Description getDescription() {
        return description;
    }

    /**
     * overwritten hashCode method
     * @return object hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(depositID);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return this.depositID;
    }

    @Override
    public Object toDTO() {
        return new DepositDTO(this.depositID, this.description.value());
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "depositID=" + depositID +
                ", desc=" + description +
                '}';
    }
}
