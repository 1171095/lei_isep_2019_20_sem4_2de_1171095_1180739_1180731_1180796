package eapli.base.depositmanagement.domain;

import java.util.Objects;

public class DepositDTO {
    private long id;
    private String Description;

    public DepositDTO(long id, String description) {
        this.id = id;
        Description = description;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return Description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepositDTO that = (DepositDTO) o;
        return id == that.id &&
                Objects.equals(Description, that.Description);
    }



}
