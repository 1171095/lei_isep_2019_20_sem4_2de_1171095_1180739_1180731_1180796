package eapli.base.depositmanagement.repositories;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface DepositRepository extends DomainRepository<Long, Deposit> {

    default Optional<Deposit> findByDepositID(Long id){
        return ofIdentity(id);
    };

    public Iterable<Deposit> getAllDeposits();

}
