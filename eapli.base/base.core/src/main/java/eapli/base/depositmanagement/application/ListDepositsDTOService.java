package eapli.base.depositmanagement.application;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.depositmanagement.domain.DepositDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListDepositsDTOService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();


    public Iterable<DepositDTO> getDepositDTOS(Iterable<Deposit> lstDeposits) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);
        ArrayList<DepositDTO> lstProdDTO = new ArrayList<>();

        for(Deposit p: lstDeposits){
            DepositDTO pDTO = (DepositDTO)p.toDTO();
            lstProdDTO.add(pDTO);
        }
        return lstProdDTO;
    }
}
