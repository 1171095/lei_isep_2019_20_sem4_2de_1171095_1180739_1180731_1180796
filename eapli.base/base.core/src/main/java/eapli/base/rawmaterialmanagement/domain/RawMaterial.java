package eapli.base.rawmaterialmanagement.domain;

import eapli.base.productmanagement.domain.Product;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

/*
 entity class 'RawMaterial'
 */
@Entity
public class RawMaterial implements AggregateRoot<String>, DTOable {

    @Id
    @Column(length = 15)
    @XmlElement(required = true)
    private String rawMaterialID;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CategoryID")
    @XmlElement(required = true)
    private RawMaterialCategory rawMaterialCategory;



    /**
     * empty constructor for ORM
     */
    public RawMaterial(){};

    /**
     *  (anotaçao many raw para one category)
     * @param rawMaterialCategory raw material category id
     */
    public RawMaterial(Product p, RawMaterialCategory rawMaterialCategory){

        this.rawMaterialID = p.identity();
        this.rawMaterialCategory = rawMaterialCategory;
    }

    public RawMaterial(Material m, RawMaterialCategory rawMaterialCategory){
        this.rawMaterialID = m.identity();
        this.rawMaterialCategory = rawMaterialCategory;

    }

    /**
     * overwritten equals method
     * @param o object to be compared
     * @return true if same object false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawMaterial rawMaterial = (RawMaterial) o;
        return Objects.equals(rawMaterialID, rawMaterial.rawMaterialCategory);
    }

    public void setRawMaterialID(String rawMaterialID) {
        this.rawMaterialID = rawMaterialID;
    }

    public void setRawMaterialCategory(String rawMaterialID) {
        this.rawMaterialID = rawMaterialID;
    }





    /**
     * overwritten hashCode method
     * @return object hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(rawMaterialCategory);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public String identity() {
        return this.rawMaterialID;
    }

    public Object toDTO() {
        return new RawMaterialDTO(rawMaterialID, rawMaterialCategory.toString());

    }
}

