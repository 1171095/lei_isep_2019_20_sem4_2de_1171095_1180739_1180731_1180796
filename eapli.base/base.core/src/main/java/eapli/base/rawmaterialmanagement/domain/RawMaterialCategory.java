package eapli.base.rawmaterialmanagement.domain;

import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

@Entity
@SequenceGenerator(name="CategoryID", initialValue=0, allocationSize=100)
@XmlAccessorType(XmlAccessType.PROPERTY)
public class RawMaterialCategory implements AggregateRoot<Long>, DTOable {

    @Id
    @GeneratedValue
    private Long rmcID;
    private Description rawMaterialDescription;


    /*
    empty constructor ofr ORM
     */
    public RawMaterialCategory() {};

    public RawMaterialCategory ( Description rawMaterialDescription) {
        this.rawMaterialDescription = rawMaterialDescription;
    }

    @XmlElement(required = true)
    public Long getRawMaterialCategoryID() {
        return rmcID;
    }
    @XmlElement(required = true)
    public Description getRawMaterialDescription() {
        return rawMaterialDescription;
    }

    @Override
    public boolean sameAs(Object other) {
        return DomainEntities.areEqual(this, other);
    }


    @Override
    public Long identity() {
        return this.rmcID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawMaterialCategory that = (RawMaterialCategory) o;
        return rmcID == that.rmcID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rmcID);
    }

    public Description retDesc (){
        return rawMaterialDescription;
    }

    @Override
    public Object toDTO() {
        return new RawMaterialCategoryDTO(this.rmcID, this.rawMaterialDescription.value());

    }


    @Override
    public String toString() {
        return
                "rmcID=" + rmcID +
                ", desc=" + rawMaterialDescription
                ;
    }
}
