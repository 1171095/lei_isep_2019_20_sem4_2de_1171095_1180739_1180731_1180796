package eapli.base.rawmaterialmanagement.application;

import com.fasterxml.jackson.databind.ser.Serializers;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListCategoriesDTOService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public Iterable<RawMaterialCategoryDTO> listRawMaterialCategoriesDTO(Iterable<RawMaterialCategory> listCategories){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);
        ArrayList<RawMaterialCategoryDTO> listDTO = new ArrayList<>();

        for (RawMaterialCategory cat:listCategories)
        {
            RawMaterialCategoryDTO catDTO = (RawMaterialCategoryDTO) cat.toDTO();
            listDTO.add(catDTO);

        }
        return listDTO;
    }
}
