package eapli.base.rawmaterialmanagement.application;

import com.zaxxer.hikari.util.IsolationLevel;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productmanagement.domain.Description;
import eapli.base.productmanagement.domain.Product;
import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import javax.swing.text.html.Option;
import java.util.Optional;

public class RawMaterialManagementService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final RawMaterialCategoryRepository repo = PersistenceContext.repositories().rawMaterialCategory();
    private final RawMaterialRepository rawRepo = PersistenceContext.repositories().rawMaterial();

    public void registerRawMaterialCategory (Description desc){
        RawMaterialCategory newRMC = new RawMaterialCategory(desc);
        repo.save(newRMC);
    }
    public Optional<RawMaterial> getRawMaterialByID(String id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);
        if(rawRepo.containsOfIdentity(id))
            return rawRepo.findByRawMaterialID(id);

        throw new IllegalArgumentException("Raw material id invalid");
    }

    public boolean definingRawMaterialMaterial(Material m, RawMaterialCategory rmcID){
        RawMaterial newRM = new RawMaterial(m,rmcID);
        if(newRM != null){
            rawRepo.save(newRM);
            return true;
        }
        return false;

    }

    public boolean definingRawMaterialProduct(Product p, RawMaterialCategory rmcID){
        RawMaterial newRM = new RawMaterial(p,rmcID);
        if (newRM!= null){
            rawRepo.save(newRM);
            return true;
        }

        return false;
    }

    public Iterable<RawMaterial> listRawMaterial(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER);
        return rawRepo.getAllRawMaterials();
    }
}
