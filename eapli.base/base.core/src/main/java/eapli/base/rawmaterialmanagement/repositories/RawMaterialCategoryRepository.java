package eapli.base.rawmaterialmanagement.repositories;

import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface    RawMaterialCategoryRepository extends DomainRepository<Long, RawMaterialCategory> {

    public Optional<RawMaterialCategory> findByRawMaterialCategoryID(Long id);

    public Iterable<RawMaterialCategory> getAllRawMaterialCategory ();

}
