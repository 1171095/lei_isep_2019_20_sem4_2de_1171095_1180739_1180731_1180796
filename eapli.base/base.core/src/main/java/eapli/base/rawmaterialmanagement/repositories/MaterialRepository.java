package eapli.base.rawmaterialmanagement.repositories;

import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.framework.domain.repositories.DomainRepository;

public interface MaterialRepository extends DomainRepository<String, Material> {
}
