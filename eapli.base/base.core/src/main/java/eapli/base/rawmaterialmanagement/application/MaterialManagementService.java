package eapli.base.rawmaterialmanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.repositories.MaterialRepository;
import eapli.framework.infrastructure.authz.application.AuthorizationService;

public class MaterialManagementService {
    private final MaterialRepository repo = PersistenceContext.repositories().material();

    public Material createMaterial(String id, String desc){
        Material newMat= new Material(id, new Description(desc));
        if(newMat!=null && !repo.containsOfIdentity(id)){
            return repo.save(newMat);
        }
        return null;
    }

    public Iterable<Material> getAllMaterials(){
        return repo.findAll();
    }

}
