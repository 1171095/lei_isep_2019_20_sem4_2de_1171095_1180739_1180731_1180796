package eapli.base.rawmaterialmanagement.domain;


import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Objects;

@Entity
@XmlType(propOrder = {"materialID", "description"})
public class Material implements AggregateRoot<String> {

    @Id
    @Column(length = 15)
    private String materialID;

    private Description description;


    protected Material(){}

    public Material(String id, Description description){
        this.materialID = id;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return Objects.equals(materialID, material.materialID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(materialID);
    }

    @XmlElement(required = true)
    public String getMaterialID() {
        return materialID;
    }

    public void setMaterialID(String materialID) {
        this.materialID = materialID;
    }

    @XmlElement(required = true)
    public Description getDescription() {
        return description;
    }

    public void setDescription(Description desc) {
        this.description = desc;
    }

    @Override
    public boolean sameAs(Object other) {
        return DomainEntities.areEqual(this, other);
    }

    @Override
    public String identity() {
        return this.materialID;
    }
}
