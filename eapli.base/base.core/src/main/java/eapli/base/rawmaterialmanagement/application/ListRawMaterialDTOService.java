package eapli.base.rawmaterialmanagement.application;

import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;
import java.util.List;

public class ListRawMaterialDTOService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public Iterable<RawMaterialDTO> rawMaterialDTO(Iterable<RawMaterial> lstRawMaterial){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        List<RawMaterialDTO> lstDTO = new ArrayList<>();

        for (RawMaterial rm: lstRawMaterial)
        {
            RawMaterialDTO rawDTO = (RawMaterialDTO) rm.toDTO();
            lstDTO.add(rawDTO);

        }

        return lstDTO;
    }
}
