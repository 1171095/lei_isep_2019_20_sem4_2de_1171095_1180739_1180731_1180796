package eapli.base.rawmaterialmanagement.domain;

import java.util.Objects;

public class RawMaterialDTO {

    private String rawMaterialID;
    private String category;

    public RawMaterialDTO(String rawMaterialID, String category){
        this.rawMaterialID = rawMaterialID;
        this.category = category;
    }

    public String getRawMaterialID() {
        return rawMaterialID;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawMaterialDTO that = (RawMaterialDTO) o;
        return Objects.equals(rawMaterialID, that.rawMaterialID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawMaterialID);
    }
}
