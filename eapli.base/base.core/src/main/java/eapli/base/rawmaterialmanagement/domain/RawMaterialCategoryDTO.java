package eapli.base.rawmaterialmanagement.domain;

public class RawMaterialCategoryDTO {

    String desc;
    Long id;

    public RawMaterialCategoryDTO(Long id, String desc){
        this.id = id;
        this.desc = desc;
    }

    public String getDesc(){
        return this.desc;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "RawMaterialCategoryDTO{" +
                "desc='" + desc + '\'' +
                ", id=" + id +
                '}';
    }
}
