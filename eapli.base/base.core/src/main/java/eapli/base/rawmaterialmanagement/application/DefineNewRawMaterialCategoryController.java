package eapli.base.rawmaterialmanagement.application;

import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Description;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;

import java.util.ArrayList;
import java.util.Optional;

public class DefineNewRawMaterialCategoryController {

    public boolean addRawMaterialCategory(Description desc){
        if(desc == null || desc.taVazio()){
            return false;
        }
        return new RawMaterialCategoryManagementService().registerCategory(desc);
    }
}
