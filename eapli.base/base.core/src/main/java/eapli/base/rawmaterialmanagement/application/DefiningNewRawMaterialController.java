package eapli.base.rawmaterialmanagement.application;

import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Description;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;

import java.util.ArrayList;
import java.util.Optional;

public class DefiningNewRawMaterialController {
    private RawMaterialManagementService rmms= new RawMaterialManagementService();
    private RawMaterialCategory rmc;
    private Product prod;

    public Iterable<RawMaterialCategoryDTO> listRawMaterialCategories(){
        Iterable<RawMaterialCategory> lstCategories = new RawMaterialCategoryManagementService().listRawMaterialCategories();
        return new ListCategoriesDTOService().listRawMaterialCategoriesDTO(lstCategories);
    }

    public void selectRawMaterialCategory(Long idCat){
        Optional<RawMaterialCategory> optCat = new RawMaterialCategoryManagementService().getCategotyByID(idCat);
        if(!optCat.isPresent()){
            rmc = null;
        }
        rmc = optCat.get();
    }

    public Iterable<ProductDTO> listNonRawMaterialProducts(){
        Iterable<Product> listProducts = new ProductManagementService().listAll();
        Iterable<RawMaterial> listRawMaterial = new RawMaterialManagementService().listRawMaterial();
        Iterable<Product> listNonRawMaterialProducts = listNonRawMaterialProducts(listProducts, listRawMaterial);
        return new ListProductDTOService().getProductDTOS(listNonRawMaterialProducts);
    }

    private Iterable<Product> listNonRawMaterialProducts(Iterable<Product> lstProducts, Iterable<RawMaterial> lstRawMAterials){
        ArrayList<Product> listNonRawMaterialProducts = new ArrayList<>();
        for (Product p: lstProducts){
            String id = p.identity();
            boolean flag = false;
            for (RawMaterial rm : lstRawMAterials){
                if (id.equals(rm.identity())){
                    flag = true;
                    break;
                }
            }

            if(!flag){
                listNonRawMaterialProducts.add(p);
            }


        }
        return listNonRawMaterialProducts;
    }

    public boolean addProductRawMaterial(ProductDTO productDTO){
        Optional<Product> optProd = new ProductManagementService().getProductByID(productDTO.getId());
        prod = optProd.get();
        return new RawMaterialManagementService().definingRawMaterialProduct(prod, rmc);
    }

    public boolean addMaterialRawMaterial(String id, String desc){

        Material mat = new MaterialManagementService().createMaterial(id, desc);
        if(mat == null){
            return false;
        }
        return new RawMaterialManagementService().definingRawMaterialMaterial(mat,rmc);
    }
}
