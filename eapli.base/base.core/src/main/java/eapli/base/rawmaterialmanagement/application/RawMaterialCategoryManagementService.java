package eapli.base.rawmaterialmanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Iterator;
import java.util.Optional;

public class RawMaterialCategoryManagementService {
    private final RawMaterialCategoryRepository repo = PersistenceContext.repositories().rawMaterialCategory();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public Iterable<RawMaterialCategory> listRawMaterialCategories(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return repo.getAllRawMaterialCategory();

    }

    public Optional<RawMaterialCategory> getCategotyByID(Long id){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if(repo.containsOfIdentity(id)){
            return repo.findByRawMaterialCategoryID(id);
        }
        throw new IllegalArgumentException("Invalid raw material category");
    }

    public boolean registerCategory(Description desc){
        RawMaterialCategory cat = new RawMaterialCategory((desc));
        if (cat!=null){
            repo.save(cat);
            return true;
        }
        return false;
    }
}
