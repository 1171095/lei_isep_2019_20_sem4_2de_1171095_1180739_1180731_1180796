package eapli.base.rawmaterialmanagement.repositories;

import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface RawMaterialRepository extends DomainRepository<String, RawMaterial> {

    public  Optional<RawMaterial> findByRawMaterialID(String id);

    public Iterable<RawMaterial> getAllRawMaterials();

}
