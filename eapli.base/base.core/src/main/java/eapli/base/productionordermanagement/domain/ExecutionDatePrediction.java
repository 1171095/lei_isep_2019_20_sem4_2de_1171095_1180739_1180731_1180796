package eapli.base.productionordermanagement.domain;

import eapli.base.xmlmanagement.DateAdapter;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ExecutionDatePrediction implements ValueObject {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date executionDatePrediction;

    protected ExecutionDatePrediction(){}

    public ExecutionDatePrediction(Date executionDatePrediction){
        this.executionDatePrediction = executionDatePrediction;
    }
    public String value(){ return executionDatePrediction.toString();}


    public Date getExecutionDatePrediction() {
        return executionDatePrediction;
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd");
        return df.format(executionDatePrediction);
    }
}