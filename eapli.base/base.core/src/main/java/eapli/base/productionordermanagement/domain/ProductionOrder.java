package eapli.base.productionordermanagement.domain;


import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.Unit;
import eapli.framework.domain.model.AggregateRoot;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@Entity
@XmlType(propOrder = {"productionOrderID","emissionDate","executionDatePrediction","product", "unit","productQuantity","productionOrderState"})
public class ProductionOrder implements AggregateRoot<Long> {


    @Id
    @XmlElement(required = true)
    private Long productionOrderID;
    @XmlElement(required = true)
    private EmissionDate emissionDate;
    @XmlElement(required = true)
    private ExecutionDatePrediction executionDatePrediction;
    @OneToOne
    @XmlElement(required = true)
    private Product product;

    private Unit unit;
    @XmlElement(required = true)
    private ProductQuantity productQuantity;

    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    private ProductionOrderState productionOrderState;

    protected ProductionOrder(){}

    public ProductionOrder(Long productionOrderID, EmissionDate emissionDate, ExecutionDatePrediction executionDatePrediction,
                           Product product, Unit unit, ProductQuantity productQuantity){

        this.productionOrderID = productionOrderID;
        this.emissionDate = emissionDate;
        this.executionDatePrediction = executionDatePrediction;
        this.product = product;
        this.unit = unit;
        this.productQuantity = productQuantity;
        this.productionOrderState = ProductionOrderState.PENDING;
       // this.parcels = new HashSet<>();

    }

    @XmlElement(required = true)
    public Unit getUnit() {
        return unit;
    }

    @Override
    public boolean sameAs(Object other) {
        return this.productionOrderID.equals(((ProductionOrder)other).productionOrderID);
    }


    @Override
    public Long identity() {
        return this.productionOrderID;
    }

    public Object toDTO() {
        return new ProductionOrderDTO(productionOrderID, emissionDate.toString(), executionDatePrediction.toString(), product.identity(), unit.value(), productQuantity.value(),productionOrderState.toString());
    }


}
