package eapli.base.productionordermanagement.application;

import eapli.base.productionordermanagement.domain.*;
import eapli.base.productmanagement.domain.Product;

import java.text.ParseException;
import java.util.List;

public class IntroduceNewProductionOrderController {

    private final ProductionOrderManagementService productionOrderManagementService = new ProductionOrderManagementService();

    public void introduceNewProductionOrder (String id, List<Parcel> parcels, Product product,
                                             String quantity, String unit,
                                             String executionDatePrediction,
                                             String emissionDate) {

        if( parcels == null || product == null ||
            quantity == null || executionDatePrediction == null ||
            emissionDate == null ){
            return;
        }

        try{
        productionOrderManagementService.registerProductionOrder(id, emissionDate, executionDatePrediction, product, quantity, unit);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }
}
