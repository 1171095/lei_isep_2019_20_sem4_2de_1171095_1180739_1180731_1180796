package eapli.base.productionordermanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productionordermanagement.repositories.OrderRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Optional;

public class OrderManagementService {
    private  OrderRepository orderRepository;
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private Parcel parcel;

    public OrderManagementService(TransactionalContext txCtx) {
        orderRepository = PersistenceContext.repositories().order(txCtx);

    }
    public OrderManagementService() {
        orderRepository = PersistenceContext.repositories().order();

    }

    public Iterable<Parcel> getAllOrders(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        return orderRepository.listOrders();
    }

    public boolean saveOrder(String orderID, ProductionOrder po){
//        Parcel parcel = new Parcel(orderID);
//        parcel.addProductionOrder(po);
        if(!orderRepository.containsOfIdentity(orderID)){
            parcel = new Parcel(orderID);
            parcel.addProductionOrder(po);
            orderRepository.save(parcel);
            return true;

        }else if(orderRepository.containsOfIdentity(orderID)) {
            parcel = orderRepository.findOrderByID(orderID).get();
            parcel.addProductionOrder(po);
            orderRepository.updateOrder(parcel);
            return true;
        }
        return false;
    }

    public Parcel getOrderByID(String orderID){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER, BaseRoles.POWER_USER);
        if (orderRepository.containsOfIdentity(orderID)){
            Optional<Parcel> optionalOrder = orderRepository.findOrderByID(orderID);
            if (optionalOrder.isPresent()){
                return optionalOrder.get();
            }
        }
        throw new IllegalArgumentException("!!Invalid Order identification!!");
    }
}
