package eapli.base.productionordermanagement.domain;



import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum ProductionOrderState {
    PENDING, RUNNING, COMPLETED;
}
