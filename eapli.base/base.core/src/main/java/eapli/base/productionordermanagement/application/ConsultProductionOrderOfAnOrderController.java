package eapli.base.productionordermanagement.application;

import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productionordermanagement.domain.ProductionOrderDTO;
import eapli.base.productmanagement.application.ListProductDTOService;
import org.aspectj.weaver.ast.Or;

public class ConsultProductionOrderOfAnOrderController {
    ProductionOrderManagementService poms = new ProductionOrderManagementService();
    OrderManagementService oms = new OrderManagementService();

    public Iterable<String> getAllOrders(){
        return poms.getAllOrders();
    }

    public Iterable<ProductionOrderDTO> getAllProductionOrdersOfAnOrder (String parcelID){
        Parcel parcel = oms.getOrderByID(parcelID);
        return new ListProductionOrderDTOService().getProductionOrderDTOS(parcel.getProductionOrder());
    }
}
