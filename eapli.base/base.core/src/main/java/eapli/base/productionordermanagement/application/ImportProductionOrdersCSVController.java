package eapli.base.productionordermanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Product;
import eapli.framework.domain.repositories.TransactionalContext;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ImportProductionOrdersCSVController {

    private static final String SPLIT_COMMA = ";";
    private static final String SPLIT_ORDER = ",";
    private static final String OUTPUT_FILE ="\\" + "ImportProductionOrdersError_" + (new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss")).format(new Date()) + ".csv";

    private final TransactionalContext txCtx= PersistenceContext.repositories().newTransactionalContext();
    private final ProductManagementService productManagementService = new ProductManagementService();
    private final ProductionOrderManagementService productionOrderManagementService = new ProductionOrderManagementService(txCtx);


    private File file;
    private Optional<Product> optProduct;
    private Product product;
    private ProductionOrder po;
    private  List<Parcel> listParcels;

    private int countSuccess;
    private int countFail;

    private String errorFile;

    public boolean selectInputFile(String fileName){
        this.file = new File(fileName + ".csv");

        try(BufferedReader reader = new BufferedReader(new FileReader(this.file))){

        } catch(IOException fileEx){
            return false;
        }
        return true;
    }

    public String importProductionOrders(){
        try(BufferedReader reader = new BufferedReader(new FileReader(this.file))){
            String line;
            String[] auxLine;
            String[] auxOrder;
            boolean success = false;


            reader.readLine();

            while((line = reader.readLine())!=null){
                auxLine = line.split(SPLIT_COMMA);
                  if(auxLine.length == 7){
                    String orderID = auxLine[0];
                    String emissionDate = auxLine[1];
                    String predictedDate = auxLine[2];
                    String productionCode = auxLine[3];
                    optProduct = productManagementService.getProductByID(productionCode);
                    optProduct.ifPresent(value -> product = value);
                    String quantity = auxLine[4];
                    String unit = auxLine[5];
                    txCtx.beginTransaction();
                    po=productionOrderManagementService.registerProductionOrder(orderID,emissionDate,predictedDate, product,quantity,unit);

                    auxOrder = auxLine[6].split(SPLIT_ORDER);
                    for (String order: auxOrder) {
                         success =  new OrderManagementService(txCtx).saveOrder(order.replace("\"",""),po);

                    }
                    txCtx.commit();
                    if (success)
                        countSuccess++;
                }
                else {
                    this.countFail++;
                    this.errorFile += line + "\n";
                }
            }

        }catch(FileNotFoundException fileEx){
            fileEx.printStackTrace();
        }catch(IOException ioEx){
            ioEx.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(countFail!=0)
            errorFile();
        return String.format("%d production orders imported, %d production orders failed", this.countSuccess, this.countFail);
    }

    private void errorFile(){
        String out = "orderID;emissionDate;predictedDate;productionCode;quantity;unit;order\n";
        out += this.errorFile;
        String outputFile = this.file.getParent() + OUTPUT_FILE;
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))){
            writer.write(out);
        }catch(IOException ioEx){
            ioEx.printStackTrace();
        }
    }


}
