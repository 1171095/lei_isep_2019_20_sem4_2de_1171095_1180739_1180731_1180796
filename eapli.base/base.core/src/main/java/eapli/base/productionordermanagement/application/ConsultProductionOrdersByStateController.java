package eapli.base.productionordermanagement.application;

import eapli.base.productionordermanagement.domain.ProductionOrderDTO;

import java.util.LinkedList;
import java.util.List;

public class ConsultProductionOrdersByStateController {

    private final ProductionOrderManagementService productionOrderManagementService = new ProductionOrderManagementService();

    public List<Long> consultProductionOrdersByState(String state){

        Iterable<Long> productionOrders = productionOrderManagementService.getProductionOrdersByState(state);
        List<Long> productionOrderDTOS = new LinkedList<>();
        for (Long po :productionOrders) {
            productionOrderDTOS.add(po);
        }
        return productionOrderDTOS;

    }

    public ProductionOrderDTO detailedProductionOrder(Long id){
        return productionOrderManagementService.getProductionOrderByID(id);
    }
}
