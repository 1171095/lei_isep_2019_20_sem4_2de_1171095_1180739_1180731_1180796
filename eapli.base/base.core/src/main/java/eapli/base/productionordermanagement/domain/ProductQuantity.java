package eapli.base.productionordermanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ProductQuantity implements ValueObject {

    private double productQuantity;

    protected ProductQuantity(){}

    public ProductQuantity(double productQuantity){
        this.productQuantity = productQuantity;
    }

    public double value(){ return productQuantity;}

    @XmlElement(required = true)
    public double getProductQuantity() {
        return productQuantity;
    }
}
