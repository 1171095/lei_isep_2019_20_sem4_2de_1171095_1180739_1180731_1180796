package eapli.base.productionordermanagement.repositories;

import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ProductionOrderRepository extends DomainRepository<Long, ProductionOrder> {

    default Optional<ProductionOrder> findByProductionOrderID(Long id){
        return ofIdentity(id);
    };

    public Iterable<ProductionOrder> findAllProductionOrders();

    public ProductionOrder updateProductionOrder(ProductionOrder prodOrd);

    public Iterable<Long> getProductionOrdersByState(String state);

    public Iterable<String> getAllOrderID ();

    public Iterable<Long> getAllProductionOrdersOfAnOrder (Parcel parcel);
}
