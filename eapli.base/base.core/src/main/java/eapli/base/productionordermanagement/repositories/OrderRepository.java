package eapli.base.productionordermanagement.repositories;

import eapli.base.productionordermanagement.domain.Parcel;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface OrderRepository extends DomainRepository<String, Parcel> {

    public Iterable<Parcel> listOrders();


    public Optional<Parcel> findOrderByID(String id);


    public Parcel updateOrder(Parcel parcel);
}
