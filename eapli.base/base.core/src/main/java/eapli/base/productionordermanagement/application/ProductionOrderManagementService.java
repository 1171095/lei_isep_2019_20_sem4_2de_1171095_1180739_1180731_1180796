package eapli.base.productionordermanagement.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionordermanagement.domain.*;
import eapli.base.productionordermanagement.repositories.ProductionOrderRepository;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.Unit;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProductionOrderManagementService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    private final ProductionOrderRepository repo ;

    public ProductionOrderManagementService(TransactionalContext tx){
        repo = PersistenceContext.repositories().productionOrder(tx);
    }
    public ProductionOrderManagementService(){
        repo = PersistenceContext.repositories().productionOrder();
    }
    public ProductionOrder registerProductionOrder(String orderID, String emissionDate, String predictedDate, Product product, String quantity, String unit) throws ParseException {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);

            Date auxEmissionDate = simpleDateFormat.parse(emissionDate);
            Date auxPredictedDate = simpleDateFormat.parse(predictedDate);
            java.sql.Date sqlEmissionDate= new java.sql.Date(auxEmissionDate.getTime());
            java.sql.Date sqlPredictedDate= new java.sql.Date(auxPredictedDate.getTime());


            ProductionOrder productionOrder = new ProductionOrder(Long.parseLong(orderID),new EmissionDate(sqlEmissionDate),new ExecutionDatePrediction(sqlPredictedDate),product,new Unit(unit),new ProductQuantity(Integer.parseInt(quantity)));
            if(!repo.contains(productionOrder)){
                try{
                    repo.save(productionOrder);
                    return productionOrder;
                } catch(Exception ex){
                    System.out.println("error saving production Order");
                }
            }
            return null;
    }

    public Iterable<ProductionOrder> listAll() {
        return repo.findAllProductionOrders();
    }

    public Iterable<String> getAllOrders(){
        return repo.getAllOrderID();
    }

    public Iterable<Long> getAllProductionOrdersOfAnOrder (Parcel parcel){
        return repo.getAllProductionOrdersOfAnOrder(parcel);
    }

    public Iterable<Long> getProductionOrdersByState(String state){
        return this.repo.getProductionOrdersByState(state);
    }

    public ProductionOrderDTO getProductionOrderByID(Long id)
    {
        return (ProductionOrderDTO) repo.findByProductionOrderID(id).get().toDTO() ;
    }
}
