package eapli.base.productionordermanagement.domain;

import eapli.base.xmlmanagement.DateAdapter;
import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Embeddable
@XmlAccessorType(XmlAccessType.PROPERTY)
public class EmissionDate implements ValueObject {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date emissionDate;

    protected EmissionDate(){

    }


    public EmissionDate(Date emissionDate){
        this.emissionDate = emissionDate;
    }

    public String value(){ return emissionDate.toString();}


    public Date getEmissionDate() {
        return emissionDate;
    }


    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd");
        return df.format(emissionDate);
    }
}
