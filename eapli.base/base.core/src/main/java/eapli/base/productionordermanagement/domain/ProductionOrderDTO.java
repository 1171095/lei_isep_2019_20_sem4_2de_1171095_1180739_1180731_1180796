package eapli.base.productionordermanagement.domain;

import java.util.ArrayList;
import java.util.List;

public class ProductionOrderDTO {


    private Long productionOrderID;
    private String emissionDate;
    private String executionDatePrediction;
    private String product;
    private String unit;
    private double productQuantity;

    private String productionOrderState;

    protected ProductionOrderDTO(){}

    public ProductionOrderDTO(Long productionOrderID, String emissionDate, String executionDatePrediction,
                              String product,String unit, double productQuantity, String productionOrderState){

        this.productionOrderID = productionOrderID;
        this.emissionDate = emissionDate;
        this.executionDatePrediction = executionDatePrediction;
        this.product = product;
        this.unit = unit;
        this.productQuantity = productQuantity;
        this.productionOrderState = productionOrderState;

    }

    public Long getProductionOrderID() {
        return productionOrderID;
    }

    public String getEmissionDate() {
        return emissionDate;
    }

    public String getExecutionDatePrediction() {
        return executionDatePrediction;
    }

    public String getProduct() {
        return product;
    }

    public String getUnit() {
        return unit;
    }

    public double getProductQuantity() {
        return productQuantity;
    }

    public String getProductionOrderState() {
        return productionOrderState;
    }

    @Override
    public String toString() {
        return "\nProductionOrder{\n" +
                "productionOrderID=" + productionOrderID + '\n' +
                "emissionDate=" + emissionDate + '\n' +
                "executionDatePrediction='" + executionDatePrediction + '\n' +
                "product='" + product + '\n' +
                "unit='" + unit + '\n' +
                "productQuantity=" + productQuantity +
                "productionOrderState='" + productionOrderState + '\n' +
                '}' + '\n';
    }
}

