package eapli.base.productionordermanagement.domain;

import eapli.framework.domain.model.AggregateRoot;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@XmlType(propOrder = {"id","productionOrder"})
public class Parcel implements AggregateRoot<String> {

    @Id
    private String id;

    @ManyToMany
    private List<ProductionOrder> productionOrder;

    protected Parcel(){}

    public Parcel(String id){
        this.id = id;
        this.productionOrder = new ArrayList<>();
    }

    public void addProductionOrder(ProductionOrder productionOrder) {
        this.productionOrder.add(productionOrder);
    }

    @XmlElement(required = true, name = "productionOrder")
    public List<ProductionOrder> getProductionOrder() {
        return productionOrder;
    }
    @XmlElement(required = true, name = "parcelID")
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean sameAs(Object other) {
        return this.id.equalsIgnoreCase(((Parcel)other).id);
    }

    @Override
    public String identity() {
        return this.id;
    }
}
