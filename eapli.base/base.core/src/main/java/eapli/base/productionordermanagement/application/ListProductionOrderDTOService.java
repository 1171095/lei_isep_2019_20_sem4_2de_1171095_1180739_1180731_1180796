package eapli.base.productionordermanagement.application;

import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productionordermanagement.domain.ProductionOrderDTO;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListProductionOrderDTOService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();


    public Iterable<ProductionOrderDTO> getProductionOrderDTOS(Iterable<ProductionOrder> lstProductionOrders) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.PRODUCTION_MANAGER,BaseRoles.POWER_USER);
        ArrayList<ProductionOrderDTO> lstProdDTO = new ArrayList<>();

        for(ProductionOrder p: lstProductionOrders){
            ProductionOrderDTO pDTO = (ProductionOrderDTO)p.toDTO();
            lstProdDTO.add(pDTO);
        }
        return lstProdDTO;
    }
}
