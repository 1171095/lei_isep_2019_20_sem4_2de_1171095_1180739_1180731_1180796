package eapli.base.xmlmanagement;

import eapli.base.depositmanagement.application.DepositManagementService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionordermanagement.application.OrderManagementService;
import eapli.base.productionordermanagement.application.ProductionOrderManagementService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.rawmaterialmanagement.application.MaterialManagementService;
import eapli.base.rawmaterialmanagement.application.RawMaterialManagementService;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ExportToXmlController {
    private final String XML_FILE_NAME = "factoryFloor";

    private FactoryFloor factoryFloor = new FactoryFloor();


    public void setAllObjects(){
        setProducts();
        setRawMaterials();
        setMaterials();
        setProductionOrders();
        setMachines();
        setDeposits();
        setProductionLines();
        setParcels();
    }

    public boolean writeToXML() throws SAXException {
        File exportFile = new File("./XSD_XML/xml/"+ XML_FILE_NAME + ".xml");
        File xsd = new File("./XSD_XML/xsd/main.xsd");
        SchemaFactory factory =SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(new StreamSource(xsd)) ;
        try{
            JAXBContext jaxbContext = JAXBContext.newInstance(FactoryFloor.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setSchema(schema);
            marshaller.marshal(factoryFloor, exportFile);

        }catch(JAXBException  e){
            e.printStackTrace();
            return false;
        }


        return true;
    }
    private static <T> List<T> getListFromIterable(Iterable<T> it){
        List<T> list = new ArrayList<>();
        for (T t:it)
        {
            list.add(t);
        }
        return list;
    }

    public void setProducts() {
        factoryFloor.setProducts(getListFromIterable(new ProductManagementService().listAll()));
    }

    public void setRawMaterials() {
        factoryFloor.setRawMaterials(getListFromIterable(new RawMaterialManagementService().listRawMaterial()));
    }

    public void setMaterials() {
        factoryFloor.setMaterials(getListFromIterable(new MaterialManagementService().getAllMaterials()));

    }

    public void setProductionOrders() {
        factoryFloor.setProductionOrders(getListFromIterable(new ProductionOrderManagementService().listAll()));

    }

    public void setMachines() {
        factoryFloor.setMachines(getListFromIterable(new MachineManagementService().listMachines()));

    }

    public void setDeposits() {
        factoryFloor.setDeposits(getListFromIterable(new DepositManagementService().listDeposits()));

    }

    public void setProductionLines() {
        factoryFloor.setProductionLines(getListFromIterable(new ProductionLineManagementService().listProductionLines()));

    }

    public void setParcels() {
        factoryFloor.setParcels(getListFromIterable(new OrderManagementService().getAllOrders()));

    }
}
