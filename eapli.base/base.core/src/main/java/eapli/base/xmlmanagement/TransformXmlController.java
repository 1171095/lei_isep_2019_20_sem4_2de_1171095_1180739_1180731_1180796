package eapli.base.xmlmanagement;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class TransformXmlController {
    private static final String HTML_EXTENSION = ".html";
    private static final String HTML_OPTION = "HMTL";
    private static final String TXT_EXTENSION = ".txt";
    private static final String TXT_OPTION = "TXT";
    private static final String JSON_EXTENSION = ".json";
    private static final String JSON_OPTION = "JSON";


    private static final String OUT_HMTL_PATH ="./XSD_XML/xml2html/factoryFloor";
    private static final String OUT_TXT_PATH ="./XSD_XML/xml2txt/factoryFloor";
    private static final String OUT_JSON_PATH ="./XSD_XML/xml2json/factoryFloor";

    private static final String HTML_XSL1_PATH ="./XSD_XML/xml2html/htmlTotal.xsl";
    private static final String TXT_XSL1_PATH ="./XSD_XML/xml2txt/txtTotal.xsl";
    private static final String JSON_XSL1_PATH ="./XSD_XML/xml2json/jsonTotal.xsl";

    private static final String HTML_XSL2_PATH ="./XSD_XML/xml2html/htmlProductionOrders.xsl";
    private static final String TXT_XSL2_PATH ="./XSD_XML//xml2txt/txtProductionOrders.xsl";
    private static final String JSON_XSL2_PATH ="./XSD_XML/xml2json/jsonProductionOrders.xsl";

    private static final String HTML_XSL3_PATH ="./XSD_XML/xml2html/htmlParcelInformation.xsl";
    private static final String TXT_XSL3_PATH ="./XSD_XML//xml2txt/txtParcelInformation.xsl";
    private static final String JSON_XSL3_PATH ="./XSD_XML/xml2json/jsonParcelInformation.xsl";


    private ExporterService exporterService= new ExporterService();;

    public void transform(String option){


        if(option.startsWith("html"))
            htmlOptions(option);
        if (option.startsWith("txt"))
            txtOptions(option);
        if(option.startsWith("json"))
            jsonOptions(option);
        if(option.equalsIgnoreCase("relat"))
            exporterService.transformRelat("./XSD_XML/relat2html/relatHtml.hmtl","./XSD_XML/relat2html/relat2html.xsl","./XSD_XML/relat/relatorio.xml");


    }

    private void jsonOptions(String option) {
        String[] aux = option.split("_");
        switch (aux[1]){
            case "total":
                exporterService.transformXML(OUT_JSON_PATH + "Total" + JSON_OPTION + JSON_EXTENSION, JSON_XSL1_PATH);
                break;
            case "ParcelInformation":
                exporterService.transformXML(OUT_JSON_PATH + "ParcelInformation" + JSON_OPTION + JSON_EXTENSION, JSON_XSL3_PATH);
                break;
            case "ProductionOrders":
                exporterService.transformXML(OUT_JSON_PATH + "ProductionOrders" + JSON_OPTION + JSON_EXTENSION, JSON_XSL2_PATH);
                break;

        }
    }

    private void txtOptions(String option) {
        String[] aux = option.split("_");
        switch (aux[1]){
            case "total":
                exporterService.transformXML(OUT_TXT_PATH + "Total" + TXT_OPTION + TXT_EXTENSION, TXT_XSL1_PATH);
                break;
            case "ParcelInformation":
                exporterService.transformXML(OUT_TXT_PATH + "ParcelInformation" + TXT_OPTION + TXT_EXTENSION, TXT_XSL3_PATH);
                break;
            case "ProductionOrders":
                exporterService.transformXML(OUT_TXT_PATH + "ProductionOrders" + TXT_OPTION + TXT_EXTENSION, TXT_XSL2_PATH);
                break;

        }
    }

    private void htmlOptions(String option) {
        String[] aux = option.split("_");
        switch (aux[1]){
            case "total":
                exporterService.transformXML(OUT_HMTL_PATH + "Total" + HTML_OPTION + HTML_EXTENSION, HTML_XSL1_PATH);
                break;
            case "ParcelInformation":
                exporterService.transformXML(OUT_HMTL_PATH + "ParcelInformation" + HTML_OPTION + HTML_EXTENSION, HTML_XSL3_PATH);
                break;
            case "ProductionOrders":
                exporterService.transformXML(OUT_HMTL_PATH + "ProductionOrders" + HTML_OPTION + HTML_EXTENSION, HTML_XSL2_PATH);
                break;

        }
    }
}
