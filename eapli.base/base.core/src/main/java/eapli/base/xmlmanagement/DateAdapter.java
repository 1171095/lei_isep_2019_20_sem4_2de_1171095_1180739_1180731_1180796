package eapli.base.xmlmanagement;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class DateAdapter extends XmlAdapter<String, Date> {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public Date unmarshal(String s) throws Exception {
        return null;
    }

    @Override
    public String marshal(Date date) throws Exception {
        synchronized (dateFormat){
            return dateFormat.format(date);
        }
    }
}
