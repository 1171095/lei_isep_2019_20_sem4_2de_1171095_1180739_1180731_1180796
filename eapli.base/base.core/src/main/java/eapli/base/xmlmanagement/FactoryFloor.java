package eapli.base.xmlmanagement;


import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productmanagement.domain.Product;
import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement(name = "FactoryFloor")
public class FactoryFloor {

    @XmlElementWrapper(name = "Products")
    @XmlElement(name = "Product")
    private List<Product> products;

    @XmlElementWrapper(name = "RawMaterials")
    @XmlElement(name = "RawMaterial")
    private List<RawMaterial> rawMaterials;

    @XmlElementWrapper(name = "Materials")
    @XmlElement(name = "Material")
    private List<Material> materials;

    @XmlElementWrapper(name = "ProductionOrders")
    @XmlElement(name = "ProductionOrder")
    private List<ProductionOrder> productionOrders;

    @XmlElementWrapper(name = "Machines")
    @XmlElement(name = "Machine")
    private List<Machine> machines;

    @XmlElementWrapper(name = "Deposits")
    @XmlElement(name = "Deposit")
    private List<Deposit> deposits;

    @XmlElementWrapper(name = "ProductionLines")
    @XmlElement(name = "ProductionLine")
    private List<ProductionLine> productionLines;

    @XmlElementWrapper(name = "Parcels")
    @XmlElement(name = "Parcel")
    private List<Parcel> parcels;

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void setRawMaterials(List<RawMaterial> rawMaterials) {
        this.rawMaterials = rawMaterials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public void setProductionOrders(List<ProductionOrder> productionOrders) {
        this.productionOrders = productionOrders;
    }

    public void setMachines(List<Machine> machines) {
        this.machines = machines;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    public void setProductionLines(List<ProductionLine> productionLines) {
        this.productionLines = productionLines;
    }

    public void setParcels(List<Parcel> parcels) {
        this.parcels = parcels;
    }


}


