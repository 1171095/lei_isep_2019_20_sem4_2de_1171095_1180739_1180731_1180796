package eapli.base.xmlmanagement;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileOutputStream;
import java.io.OutputStream;



public class ExporterService {
    private static final String XML_IN_PATH="./XSD_XML/xml/factoryFloor.xml";

    public boolean transformXML(String outFilePath, String xslFilePath){
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Source xslDoc = new StreamSource(xslFilePath);
            Source xml = new StreamSource(XML_IN_PATH);
            OutputStream outputStream = new FileOutputStream(outFilePath);
            Transformer transformer = transformerFactory.newTransformer(xslDoc);
            transformer.transform(xml, new StreamResult(outputStream));

            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void transformRelat(String outFilePath, String xslFilePath, String relatorioXML) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Source xslDoc = new StreamSource(xslFilePath);
            Source xml = new StreamSource(relatorioXML);
            OutputStream outputStream = new FileOutputStream(outFilePath);
            Transformer transformer = transformerFactory.newTransformer(xslDoc);
            transformer.transform(xml, new StreamResult(outputStream));
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
