/**
 *
 */
package eapli.base.infrastructure.persistence;

import eapli.base.chargebackmanagement.repositories.ChargebackRepository;
import eapli.base.clientusermanagement.repositories.ClientUserRepository;

import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.consumemanagement.repositories.ConsumeRepository;
import eapli.base.depositmanagement.repositories.DepositRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationArchivedRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.messagemanagement.repositories.*;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.productionordermanagement.repositories.OrderRepository;
import eapli.base.productionordermanagement.repositories.ProductionOrderRepository;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.rawmaterialmanagement.repositories.MaterialRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {


    /**
	 * factory method to create a transactional context to use in the repositories
	 *
	 * @return
	 */
	TransactionalContext newTransactionalContext();

	/**
	 *
	 * @param autoTx the transactional context to enrol
	 * @return
	 */
	UserRepository users(TransactionalContext autoTx);

	/**
	 * repository will be created in auto transaction mode
	 *
	 * @return
	 */
	UserRepository users();

	/**
	 *
	 * @param autoTx the transactional context to enroll
	 * @return
	 */
	ClientUserRepository clientUsers(TransactionalContext autoTx);

	/**
	 * repository will be created in auto transaction mode
	 *
	 * @return
	 */
	ClientUserRepository clientUsers();

	/**
	 *
	 * @param autoTx the transactional context to enroll
	 * @return
	 */
	SignupRequestRepository signupRequests(TransactionalContext autoTx);

	/**
	 * repository will be created in auto transaction mode
	 *
	 * @return
	 */
	SignupRequestRepository signupRequests();


	ProductRepository product();
	ProductRepository product(TransactionalContext autoTx);

	RawMaterialCategoryRepository rawMaterialCategory ();

	RawMaterialRepository rawMaterial();

	MaterialRepository material();

	DepositRepository deposit();

	ProductionLineRepository productionLine();
	ProductionLineRepository productionLine(TransactionalContext txCtx);

	MachineRepository machine();

	ProductionFormRepository productionFile();
    ProductionFormRepository productionFile(TransactionalContext txCtx);

    ProductionOrderRepository productionOrder();
	ProductionOrderRepository productionOrder(TransactionalContext txCtx);

	ErrorNotificationUntreatedRepository errorNotificationUntreated();
	ErrorNotificationArchivedRepository errorNotificationArchived();

	ChargebackMessageRepository chargebackMessage();
	ConsumeMessageRepository ConsumeMessage();
	EndActivityMessageRepository endActivity();
	ErrorMessageRepository errorMessage();
	ProductionDeliveryMessageRepository productionDeliveryMessage();
	ProductionMessageRepository productionMessage();
	RestartActivityMessageRepository restartActivityMessage();
	StartMessageRepository startActivityMessage();

	ChargebackMessageRepository chargebackMessage(TransactionalContext txCtx);
	ConsumeMessageRepository ConsumeMessage(TransactionalContext txCtx);
	EndActivityMessageRepository endActivity(TransactionalContext txCtx);
	ErrorMessageRepository errorMessage(TransactionalContext txCtx);
	ProductionDeliveryMessageRepository productionDeliveryMessage(TransactionalContext txCtx);
	ProductionMessageRepository productionMessage(TransactionalContext txCtx);
	RestartActivityMessageRepository restartActivityMessage(TransactionalContext txCtx);
	StartMessageRepository startActivityMessage(TransactionalContext txCtx);

	ChargebackRepository chargeback();
	ConsumeRepository consume();

    OrderRepository order(TransactionalContext txCtx);
	OrderRepository order();
}
