package eapli.base.errornotificationmanagement.domain;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public abstract class ErrorNotification implements AggregateRoot<Long>, DTOable {

    @Id
    private Long errorNotificationID;
    @Enumerated(EnumType.STRING)
    private ErrorType errorType;
    private ErrorDate errorDate;
    @ManyToOne
    private ProductionLine prodLine;
    @ManyToOne
    private Machine machine;

    /**
     * empty constructor for ORM
     */
    public ErrorNotification(){}

    /**
     * @param errorType is the type of the error notification
     * @param errorDate is the date of the error notification creation
     * @param prodLine is the prodution line that this notification is from
     * @param machine is the machine that this notification is from
     */
    public ErrorNotification(Long errorNotificationID, ErrorType errorType, ErrorDate errorDate, ProductionLine prodLine, Machine machine){
        this.errorNotificationID=errorNotificationID;
        this.errorType=errorType;
        this.errorDate=errorDate;
        this.prodLine=prodLine;
        this.machine=machine;
    }


    /**
     * copy constructor
     * @param errorNotification object to copy
     */
    public ErrorNotification(ErrorNotification errorNotification) {
        this.errorNotificationID=errorNotification.errorNotificationID;
        this.errorType= errorNotification.errorType;
        this.errorDate=errorNotification.errorDate;
        this.prodLine=errorNotification.prodLine;
        this.machine=errorNotification.machine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorNotification that = (ErrorNotification) o;
        return errorNotificationID.equals(that.errorNotificationID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorNotificationID);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    @Override
    public Long identity() {
        return this.errorNotificationID;
    }

    @Override
    public String toString() {
        return "{" +
                "ErrorNotificationID=" + errorNotificationID +
                ", errorType=" + errorType +
                ", errorDate=" + errorDate +
                ", prodLineID=" + prodLine +
                ", machineID=" + machine +
                '}';
    }

    @Override
    public Object toDTO(){
        return new ErrorNotificationDTO(errorNotificationID, errorType.toString(), errorDate.value(), prodLine.toString(),
                machine.toString());
    }
}
