package eapli.base.errornotificationmanagement.domain;

import eapli.framework.domain.model.ValueObject;

import javax.persistence.Embeddable;
import java.awt.*;
import java.sql.Date;
import java.util.Calendar;

@Embeddable
public class ErrorDate implements ValueObject {
    private Calendar errorDate;

    protected ErrorDate(){}

    public ErrorDate(Calendar errorDate){
        this.errorDate=errorDate;
    }

    public String value(){ return errorDate.toString();}
}
