package eapli.base.errornotificationmanagement.domain;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.Entity;

@Entity
public class ErrorNotificationArchived extends ErrorNotification implements AggregateRoot<Long>, DTOable {

    /**
     * empty constructor for ORM
     */
    public ErrorNotificationArchived(){super();}

    /**
     * @param errorType is the type of the error notification
     * @param errorDate is the date of the error notification creation
     * @param prodLine is the prodution line that this notification is from
     * @param machine is the machine that this notification is from
     */
    public ErrorNotificationArchived(Long errorNotificationID, ErrorType errorType, ErrorDate errorDate, ProductionLine prodLine, Machine machine){
        super(errorNotificationID, errorType, errorDate, prodLine, machine);
    }

    /**
     * copy constructor
     * @param errorNotificationUntreated object to copy
     */
    public ErrorNotificationArchived(ErrorNotificationUntreated errorNotificationUntreated){
        super(errorNotificationUntreated);
    }

    @Override
    public String toString() {
        return "ErrorNotificationArchived" + super.toString();
    }

}
