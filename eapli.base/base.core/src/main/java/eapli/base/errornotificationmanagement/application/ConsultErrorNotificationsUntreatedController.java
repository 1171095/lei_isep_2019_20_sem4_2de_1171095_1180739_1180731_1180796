package eapli.base.errornotificationmanagement.application;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;

public class ConsultErrorNotificationsUntreatedController {

    private ErrorNotificationManagementService enms = new ErrorNotificationManagementService();


    //Error Notifications Untreated
    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreated(){
        return enms.listErrorNotificationsUntreated();
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByErrorType(ErrorType errorType){
        return enms.listErrorNotificationsUntreatedByErrorType(errorType);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByErrorDate(ErrorDate errorDate){
        return enms.listErrorNotificationsUntreatedByErrorDate(errorDate);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByProdLine(ProductionLine pd){
        return enms.listErrorNotificationsUntreatedByProdLine(pd);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByMachine(Machine machine){
        return enms.listErrorNotificationsUntreatedByMachine(machine);
    }

    public Iterable<ErrorDate> getAllErrorDates() {
        return enms.getAllErrorDatesUntreated();
    }
}
