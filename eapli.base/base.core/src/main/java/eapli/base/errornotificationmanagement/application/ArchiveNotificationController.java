package eapli.base.errornotificationmanagement.application;

import eapli.base.errornotificationmanagement.domain.*;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;

import java.sql.Date;
import java.util.Iterator;

public class ArchiveNotificationController {
    private ErrorNotificationManagementService enms = new ErrorNotificationManagementService();



    public Iterable<ErrorNotificationUntreated> listAllUntreatedNotifications (){
        return enms.listErrorNotificationsUntreated();
    }

    public ErrorNotificationUntreated findErrorNotificationUntreatedByID (Long id){
        return enms.findErrorNotificationUntreatedByID(id).get();
    }

    public void archiveNotification (ErrorNotificationUntreated enu){
        ErrorNotificationArchived ena = new ErrorNotificationArchived(enu);
        enms.archiveErrorNotification(ena);
        enms.removeErrorNotificationUntreated(enu);
    }
}
