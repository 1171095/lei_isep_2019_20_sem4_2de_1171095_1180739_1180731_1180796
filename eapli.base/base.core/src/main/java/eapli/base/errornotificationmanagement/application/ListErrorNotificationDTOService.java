package eapli.base.errornotificationmanagement.application;

import eapli.base.errornotificationmanagement.domain.ErrorNotification;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.ArrayList;

public class ListErrorNotificationDTOService {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();


    public Iterable<ErrorNotificationDTO> getErrorNotificationDTOS(Iterable<ErrorNotification> lstProducts) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER,BaseRoles.POWER_USER);
        ArrayList<ErrorNotificationDTO> lstErrorNotiDTO = new ArrayList<>();

        for(ErrorNotification en: lstProducts){
            ErrorNotificationDTO enDTO = (ErrorNotificationDTO)en.toDTO();
            lstErrorNotiDTO.add(enDTO);
        }
        return lstErrorNotiDTO;
    }

    public Iterable<ErrorNotificationDTO> getErrorNotificationUntreatedDTOS(Iterable<ErrorNotificationUntreated> lstProducts) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER,BaseRoles.POWER_USER);
        ArrayList<ErrorNotificationDTO> lstErrorNotiDTO = new ArrayList<>();

        for(ErrorNotification en: lstProducts){
            ErrorNotificationDTO enDTO = (ErrorNotificationDTO)en.toDTO();
            lstErrorNotiDTO.add(enDTO);
        }
        return lstErrorNotiDTO;
    }

    public Iterable<ErrorNotificationDTO> getErrorNotificationArchivedDTOS(Iterable<ErrorNotificationArchived> lstProducts) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER,BaseRoles.POWER_USER);
        ArrayList<ErrorNotificationDTO> lstErrorNotiDTO = new ArrayList<>();

        for(ErrorNotification en: lstProducts){
            ErrorNotificationDTO enDTO = (ErrorNotificationDTO)en.toDTO();
            lstErrorNotiDTO.add(enDTO);
        }
        return lstErrorNotiDTO;
    }

}
