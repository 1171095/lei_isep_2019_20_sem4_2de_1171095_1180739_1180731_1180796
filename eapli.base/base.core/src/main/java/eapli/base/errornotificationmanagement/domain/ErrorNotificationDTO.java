package eapli.base.errornotificationmanagement.domain;

public class ErrorNotificationDTO {

    private Long errorNotificationID;
    private String errorType;
    private String errorDate;
    private String prodLine;
    private String machine;

    public ErrorNotificationDTO(Long errorNotificationID, String errorType, String errorDate, String prodLine, String machine){
        this.errorNotificationID=errorNotificationID;
        this.errorType=errorType;
        this.errorDate=errorDate;
        this.prodLine=prodLine;
        this.machine=machine;
    }

    public Long getErrorNotificationID() {
        return errorNotificationID;
    }

    public String getErrorType() { return errorType; }

    public String getErrorDate() {
        return errorDate;
    }

    public String getProdLine() {
        return prodLine;
    }

    public String getMachine() {
        return machine;
    }

    @Override
    public String toString() {
        return String.format(errorNotificationID + ";" + errorType + ";" + errorDate + ";" + prodLine
                + ";" + machine);
    }
}
