package eapli.base.errornotificationmanagement.domain;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.representations.dto.DTOable;

import javax.persistence.Entity;

@Entity
public class ErrorNotificationUntreated extends ErrorNotification implements AggregateRoot<Long>, DTOable {

    private static Long counter = 0L;
    /**
     * empty constructor for ORM
     */
    public ErrorNotificationUntreated(){super();}

    /**
     * @param errorType is the type of the error notification
     * @param errorDate is the date of the error notification creation
     * @param prodLine is the prodution line that this notification is from
     * @param machine is the machine that this notification is from
     */
    public ErrorNotificationUntreated(Long errorNotificationID, ErrorType errorType, ErrorDate errorDate, ProductionLine prodLine, Machine machine){
        super(errorNotificationID, errorType, errorDate, prodLine, machine);
        counter++;
    }

    //fast fix, need to solve this
    public static Long getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "ErrorNotificationUntreated" + super.toString();
    }

}
