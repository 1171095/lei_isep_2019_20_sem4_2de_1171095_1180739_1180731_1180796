package eapli.base.errornotificationmanagement.repositories;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ErrorNotificationUntreatedRepository extends DomainRepository<Long, ErrorNotificationUntreated> {

    public Optional<ErrorNotificationUntreated> findByErrorNotificationUntreatedID(Long id);

    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreated();

    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByErrorType(ErrorType errorType);

    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByErrorDate(ErrorDate errorDate);

    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByProdLine(ProductionLine pd);

    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByMachine(Machine machine);

    public Iterable<ErrorDate> getAllErrorDates();

    public ErrorNotificationUntreated addErroNotificationUntreated(ErrorNotificationUntreated enu);

    public void removeErrorNotificationUntreated (ErrorNotificationUntreated enu);
}
