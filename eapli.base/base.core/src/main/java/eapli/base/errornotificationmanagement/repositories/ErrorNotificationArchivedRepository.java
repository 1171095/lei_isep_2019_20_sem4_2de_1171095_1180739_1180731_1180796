package eapli.base.errornotificationmanagement.repositories;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.domain.repositories.DomainRepository;

import java.util.Optional;

public interface ErrorNotificationArchivedRepository extends DomainRepository<Long, ErrorNotificationArchived> {

    public Optional<ErrorNotificationArchived> findByErrorNotificationArchivedID(Long id);

    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchived();

    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByErrorType(ErrorType errorType);

    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByErrorDate(ErrorDate errorDate);

    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByProdLine(ProductionLine productionLine);

    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByMachine(Machine machine);

    public Iterable<ErrorDate> getAllErrorDates();

    public ErrorNotificationArchived archiveErrorNotification(ErrorNotificationArchived ena);
}
