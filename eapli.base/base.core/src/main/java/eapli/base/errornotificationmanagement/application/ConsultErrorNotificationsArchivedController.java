package eapli.base.errornotificationmanagement.application;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;

public class ConsultErrorNotificationsArchivedController {

    private ErrorNotificationManagementService enms = new ErrorNotificationManagementService();

    //Error Notifications Archived
    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchived(){
        return enms.listErrorNotificationsArchived();
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByErrorType(ErrorType errorType){
        return enms.listErrorNotificationsArchivedByErrorType(errorType);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByErrorDate(ErrorDate errorDate){
        return enms.listErrorNotificationsArchivedByErrorDate(errorDate);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByProdLine(ProductionLine productionLine){
        return enms.listErrorNotificationsArchivedByProdLine(productionLine);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByMachine(Machine machine){
        return enms.listErrorNotificationsArchivedByMachine(machine);
    }

    public Iterable<ErrorDate> getAllErrorDates() {
        return enms.getAllErrorDatesArchived();
    }
}
