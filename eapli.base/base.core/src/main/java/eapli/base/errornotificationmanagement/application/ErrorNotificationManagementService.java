package eapli.base.errornotificationmanagement.application;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationArchivedRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

import java.util.Optional;

public class ErrorNotificationManagementService {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ErrorNotificationUntreatedRepository repoUntreated = PersistenceContext.repositories().errorNotificationUntreated();
    private final ErrorNotificationArchivedRepository repoArchived = PersistenceContext.repositories().errorNotificationArchived();



    //Error Notifications Untreated
    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreated(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorNotificationUntreated();
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByErrorType(ErrorType errorType){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorNotificationUntreatedByErrorType(errorType);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByErrorDate(ErrorDate errorDate){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorNotificationUntreatedByErrorDate(errorDate);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByProdLine(ProductionLine pd){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorNotificationUntreatedByProdLine(pd);
    }

    public Iterable<ErrorNotificationUntreated> listErrorNotificationsUntreatedByMachine(Machine machine){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorNotificationUntreatedByMachine(machine);
    }

    public Iterable<ErrorDate> getAllErrorDatesUntreated() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoUntreated.getAllErrorDates();
    }

    //Error Notifications Archived
    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchived(){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorNotificationArchived();
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByErrorType(ErrorType errorType){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorNotificationArchivedByErrorType(errorType);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByErrorDate(ErrorDate errorDate){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorNotificationArchivedByErrorDate(errorDate);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByProdLine(ProductionLine productionLine){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorNotificationArchivedByProdLine(productionLine);
    }

    public Iterable<ErrorNotificationArchived> listErrorNotificationsArchivedByMachine(Machine machine){
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorNotificationArchivedByMachine(machine);
    }

    public Iterable<ErrorDate> getAllErrorDatesArchived() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.POWER_USER);
        return repoArchived.getAllErrorDates();
    }

    public Optional<ErrorNotificationUntreated> findErrorNotificationUntreatedByID (Long id){
        return repoUntreated.findByErrorNotificationUntreatedID(id);
    }

    public ErrorNotificationArchived archiveErrorNotification (ErrorNotificationArchived ena){
        return repoArchived.archiveErrorNotification(ena);
    }

    public ErrorNotificationUntreated addErrorNotificationUntreated (ErrorNotificationUntreated enu){
        return repoUntreated.addErroNotificationUntreated(enu);
    }

    public void removeErrorNotificationUntreated (ErrorNotificationUntreated enu){
        repoUntreated.removeErrorNotificationUntreated(enu);
    }
}
