package eapli.base.depositmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepositDTOTest {
    private DepositDTO d1 = new DepositDTO(1, "madeiras");
    private DepositDTO d2 = new DepositDTO(2, "vidros");
    private DepositDTO d3 = new DepositDTO(3, "metais");
    private DepositDTO d4 = new DepositDTO(1, "madeiras");


    @Test
    void testGetId() {
        assertEquals(2, d2.getId());
    }

    @Test
    void testGetDescription() {
        assertEquals("madeiras", d1.getDescription());
    }

    @Test
    void testEquals() {
        assertTrue(d1.equals(d4));
    }
}