package eapli.base.depositmanagement.domain;

import eapli.base.productmanagement.domain.Description;

import static org.junit.jupiter.api.Assertions.*;

class DepositTest {
    private Description desc1= new Description("madeiras");
    private Description desc2= new Description("vidros");
    private Description desc3= new Description("metais");

    private Deposit d1 = new Deposit(desc1);
    private Deposit d2 = new Deposit(desc2);
    private Deposit d3 = new Deposit(desc3);
    private Deposit d4 = new Deposit(desc1);

    @org.junit.jupiter.api.Test
    void testEquals() {
        assertEquals(d1,d1);
    }

    @org.junit.jupiter.api.Test
    void getDescTrue() {
        assertEquals(d1.getDescription(), desc1);
    }
    @org.junit.jupiter.api.Test
    void getDescFalse() {
        assertNotEquals(d1.getDescription(), desc2);
    }

    @org.junit.jupiter.api.Test
    void testHashCodeTrue() {
        assertEquals(d1.hashCode(), d1.hashCode());
    }

    @org.junit.jupiter.api.Test
    void testHashCodefalse() {
        assertEquals(d1.hashCode(), d2.hashCode());
    }

    @org.junit.jupiter.api.Test
    void identity() {
    }
}