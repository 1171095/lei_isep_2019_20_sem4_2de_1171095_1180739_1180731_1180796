package eapli.base.rawmaterialmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RawMaterialDTOTest {

    private RawMaterialDTO r1 = new RawMaterialDTO("0001","cat1");
    private RawMaterialDTO r2 = new RawMaterialDTO("0001","cat1");
    private RawMaterialDTO r3 = new RawMaterialDTO("0003","cat1");
    private RawMaterialDTO r4 = new RawMaterialDTO("0001","cat2");

    @Test
    void getRawMaterialID() {
        assertEquals("0001",r1.getRawMaterialID());
    }

    @Test
    void getCategory() {
        assertEquals("cat1",r1.getCategory());
    }

    @Test
    void testEquals() {
        assertEquals(r1,r2);
        assertEquals(r1,r4);
        assertNotEquals(r1,r3);
        assertEquals(r1,r4);
    }

    @Test
    void testHashCode() {
        assertEquals(r1.hashCode(),r2.hashCode());
        assertNotEquals(r1.hashCode(),r3.hashCode());
    }
}