package eapli.base.rawmaterialmanagement.domain;

import eapli.base.productmanagement.domain.Description;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaterialTest {

    private Material m1 = new Material("0001", new Description("vidro"));
    private Material m2 = new Material("0001", new Description("vidro"));
    private Material m3 = new Material("0002", new Description("vidro"));
    private Material m4 = new Material("0001", new Description("azeite"));
    private Material m5 = new Material();

    @Test
    void testEquals() {
        assertEquals(m1,m2);
        assertNotEquals(m1,m3);
        assertEquals(m1,m4);
        assertNotEquals(m1,m5);
    }

    @Test
    void testHashCode() {
        assertEquals(m1.hashCode(),m2.hashCode());
        assertNotEquals(m1.hashCode(),m5.hashCode());
    }

    @Test
    void testIdentity() {
        String exp = "0001";
        String result = m1.identity();
        assertEquals(exp,result);
    }
}