package eapli.base.rawmaterialmanagement.domain;

import eapli.base.productmanagement.domain.Description;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RawMaterialCategoryTest {
    private Description desc1 = new Description("Prensados");
    private Description desc2 = new Description("Liquidos");
    private Description desc3 = new Description("Cortados");
    private RawMaterialCategory rmc1 = new RawMaterialCategory(desc1);
    private RawMaterialCategory rmc2 = new RawMaterialCategory(desc2);
    private RawMaterialCategory rmc3 = new RawMaterialCategory(desc3);


    @Test
    void testEquals() {
        assertEquals(rmc1,rmc1);
    }

    @Test
    void testHashCodetrue() {
        assertEquals(rmc1.hashCode(),rmc1.hashCode());
    }


    @Test
    void retDesc() {
        assertEquals(desc1,rmc1.retDesc());
    }

}