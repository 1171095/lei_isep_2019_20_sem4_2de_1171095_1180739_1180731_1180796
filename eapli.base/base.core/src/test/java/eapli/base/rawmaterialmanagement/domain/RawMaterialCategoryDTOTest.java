package eapli.base.rawmaterialmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RawMaterialCategoryDTOTest {

    private String desc = "madeiras";
    private Long id = new Long(1000);
    private RawMaterialCategoryDTO rmcdto1 = new RawMaterialCategoryDTO(id, desc);


    @Test
    void getDesc() {
        assertEquals("madeiras",rmcdto1.getDesc());
    }

    @Test
    void getId() {
        assertEquals(id,rmcdto1.getId());
    }
}