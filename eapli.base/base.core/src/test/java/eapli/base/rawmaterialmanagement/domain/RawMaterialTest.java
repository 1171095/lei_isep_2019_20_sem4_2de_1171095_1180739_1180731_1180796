package eapli.base.rawmaterialmanagement.domain;

import eapli.base.productmanagement.domain.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RawMaterialTest {

    private Material m1 = new Material("0001", new Description("vidro"));
    private Material m2 = new Material("0002", new Description("vidro"));

    private Product p1 = new Product("00001",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));
    private Product p2 = new Product("00003",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));

    private RawMaterial r1 = new RawMaterial(m1,new RawMaterialCategory(new Description("desc")));
    private RawMaterial r2 = new RawMaterial(m1,new RawMaterialCategory(new Description("desc")));
    private RawMaterial r3 = new RawMaterial(m2,new RawMaterialCategory(new Description("desc")));
    private RawMaterial r4 = new RawMaterial(p1,new RawMaterialCategory(new Description("desc")));
    private RawMaterial r5 = new RawMaterial(p1,new RawMaterialCategory(new Description("desc")));
    private RawMaterial r6 = new RawMaterial(p2,new RawMaterialCategory(new Description("desc")));

    @Test
    void testEquals() {
        assertEquals(r1,r2);
        assertEquals(r4,r5);
        assertNotEquals(r1,r3);
        assertNotEquals(r1,r4);
        assertNotEquals(r1,r5);
        assertEquals(r4,r5);
        assertNotEquals(r5,r6);
    }

    @Test
    void testHashCode() {
        assertEquals(r1.hashCode(),r2.hashCode());
        assertNotEquals(r1.hashCode(),r5.hashCode());
    }

}