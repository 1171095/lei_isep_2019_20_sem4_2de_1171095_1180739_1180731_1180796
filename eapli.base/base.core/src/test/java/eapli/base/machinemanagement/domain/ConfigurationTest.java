package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class ConfigurationTest {
    File f1 = new File("f1");
    Configuration c = new Configuration("d1", f1);

    @Test
    void value() {
        assertEquals("d1", c.value());
    }



    @Test
    void taVazio() {
        assertFalse(c.taVazio());
    }


    @Test
    void getDescription() {
        assertEquals("d1", c.getDescription());
    }

    @Test
    void getFile() {
        assertEquals(f1, c.getFile());
    }

    @Test
    void setDescription() {
        String newDesc = "d2";
        c.setDescription(newDesc);
        assertEquals(newDesc, c.getDescription());
    }

    @Test
    void setFile() {
        File newFile = new File("f2");
        c.setFile(newFile);
        assertEquals(newFile, c.getFile());
    }
}