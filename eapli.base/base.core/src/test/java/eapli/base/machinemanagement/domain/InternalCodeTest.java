package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InternalCodeTest {

    private InternalCode i1 = new InternalCode("it1");
    private InternalCode i2 = new InternalCode("");

    @Test
    void value() {
        assertEquals("it1", i1.value());
    }

    @Test
    void taVazio() {
        assertTrue(i2.taVazio());
    }
}