package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MachineDTOTest {

    private MachineDTO m1 = new MachineDTO(111L,"st1","st1","st1",
            "st1","st1","st1");

    @Test
    void getId() {
        assertEquals(111L, m1.getId());
    }

    @Test
    void getInternalCode() {
        assertEquals("st1", m1.getInternalCode());
    }

    @Test
    void getSerialNumber() {
        assertEquals("st1", m1.getSerialNumber());
    }

    @Test
    void getDescription() {
        assertEquals("st1", m1.getDescription());
    }

    @Test
    void getInstallationDate() {
        assertEquals("st1", m1.getInstallationDate());
    }

    @Test
    void getBrand() {
        assertEquals("st1", m1.getBrand());
    }

    @Test
    void getModel() {
        assertEquals("st1", m1.getModel());
    }
}