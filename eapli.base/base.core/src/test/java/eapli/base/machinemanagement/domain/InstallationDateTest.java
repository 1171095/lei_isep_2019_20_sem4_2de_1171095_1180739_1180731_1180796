package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

class InstallationDateTest {

    private Date d1 = new Date(2001,10,25);
    private InstallationDate i1 = new InstallationDate(d1);

    @Test
    void value() {
        assertEquals(d1.toString(),i1.value());
    }
}