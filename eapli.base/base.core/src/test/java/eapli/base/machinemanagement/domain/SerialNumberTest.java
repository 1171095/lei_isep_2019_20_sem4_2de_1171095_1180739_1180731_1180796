package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SerialNumberTest {

    private SerialNumber s1 = new SerialNumber("1234");
    private SerialNumber s2 = new SerialNumber("");

    @Test
    void value() {
        assertEquals("1234", s1.value());
    }

    @Test
    void taVazio() {
        assertTrue(s2.taVazio());
    }
}