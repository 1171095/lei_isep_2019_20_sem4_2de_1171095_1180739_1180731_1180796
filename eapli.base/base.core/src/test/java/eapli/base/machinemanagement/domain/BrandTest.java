package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BrandTest {

    private Brand b1 = new Brand("brand1");
    private Brand b2 = new Brand("");

    @Test
    void value() {
        assertEquals("brand1",b1.value());
    }

    @Test
    void taVazio() {
        assertTrue(b2.taVazio());
    }
}