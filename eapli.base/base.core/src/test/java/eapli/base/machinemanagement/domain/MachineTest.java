package eapli.base.machinemanagement.domain;

import eapli.base.productmanagement.domain.Description;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MachineTest {
    private Machine m = new Machine(new SerialNumber("1234"), new Description("wood cutter"),
            new InstallationDate(new Date(2014,05,03)),new Brand("nike"),
            new Model("AirForce"),new InternalCode("4490"));

    private Machine m2 = new Machine(new SerialNumber("123456"), new Description("Metal cutter"),
            new InstallationDate(new Date(2000,11,18)),new Brand("Adidas"),
            new Model("LeBronXI"),new InternalCode("4490-496"));

    private Machine m3 = new Machine(new SerialNumber("3456765456"), new Description("Plastic cutter"),
            new InstallationDate(new Date(2005,07,25)),new Brand("Puma"),
            new Model("Tigre"),new InternalCode("987896545"));

    @Test
    void addConfiguration() {
    }

    @Test
    void testEquals() {
    }

    @Test
    void testHashCode() {
    }

    @Test
    void setModel() {
    }

    @Test
    void sameAs() {
    }

    @Test
    void identity() {
    }

    @Test
    void toDTO() {
    }

    @Test
    void testAddConfiguration() {
        Configuration c = new Configuration("d1", new File("f1"));
        assertTrue(m.addConfiguration(c));
    }

    @Test
    void hasRequestedConfigurationFalse() {
        assertFalse(m.hasRequestedConfiguration());
    }

    @Test
    void hasRequestedConfigurationTrue() {
        File f1 = new File("f1");
        m.setRequestedConfiguration(new Configuration("d1", f1));
        assertTrue(m.hasRequestedConfiguration());
    }

    @Test
    void getRequestedConfiguration() {
        File f1 = new File("f1");
        m.setRequestedConfiguration(new Configuration("d1", f1));
        assertEquals(f1, m.getRequestedConfiguration());
    }

    @Test
    void setRequestedConfiguration() {
        File f1 = new File("f1");
        m.setRequestedConfiguration(new Configuration("d1", f1));
        assertEquals(f1, m.getRequestedConfiguration());
    }
}