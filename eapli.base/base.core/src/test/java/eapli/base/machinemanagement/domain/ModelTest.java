package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ModelTest {

    private Model m1 = new Model("model1");
    private Model m2 = new Model("");

    @Test
    void value() {
        assertEquals("model1", m1.value());
    }

    @Test
    void taVazio() {
            assertTrue(m2.taVazio());
    }
}