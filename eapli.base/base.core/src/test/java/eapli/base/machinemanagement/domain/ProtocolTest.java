package eapli.base.machinemanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProtocolTest {

    Protocol p = new Protocol("p1");

    @Test
    void value() {
        assertEquals("p1", p.value());
    }



    @Test
    void taVazio() {
        assertFalse(p.taVazio());
    }
}