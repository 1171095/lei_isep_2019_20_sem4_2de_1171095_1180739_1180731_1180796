package eapli.base.productionordermanagement.domain;

import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

class ExecutionDatePredictionTest {

    Date date = new Date(20,10, 2020);
    ExecutionDatePrediction d = new ExecutionDatePrediction(date);

    @Test
    void value() {
        assertEquals(date.toString(), d.value());
    }
}