package eapli.base.productionordermanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductQuantityTest {
    ProductQuantity pq = new ProductQuantity(10);

    @Test
    void value() {
        assertEquals(10, pq.value());
    }

}