package eapli.base.productionordermanagement.domain;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProductionOrderDTOTest {
    //Long productionOrderID, String emissionDate, String executionDatePrediction,
    //                              String product,String unit, int productQuantity,
    //                              List<String> listOrders, String productionOrderState
    ProductionOrderDTO poDTO = new ProductionOrderDTO(1L, "10102020",
            "12102020", "0001", "metros",
            30, "PENDING");
    @Test
    void getProductionOrderID() {
        assertEquals(1L, poDTO.getProductionOrderID());
    }

    @Test
    void getEmissionDate() {
        assertEquals("10102020", poDTO.getEmissionDate());
    }

    @Test
    void getExecutionDatePrediction() {
        assertEquals("12102020", poDTO.getExecutionDatePrediction());
    }

    @Test
    void getProduct() {
        assertEquals("0001", poDTO.getProduct());
    }

    @Test
    void getUnit() {
        assertEquals("metros", poDTO.getUnit());
    }

    @Test
    void getProductQuantity() {
        assertEquals(30, poDTO.getProductQuantity());
    }

    @Test
    void getProductionOrderState() {
        assertEquals("PENDING", poDTO.getProductionOrderState());
    }
}