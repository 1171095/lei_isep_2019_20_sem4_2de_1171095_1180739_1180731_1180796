package eapli.base.productionordermanagement.domain;

import eapli.base.productmanagement.domain.*;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProductionOrderTest {

    private Product p1 = new Product("00001",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));

    ProductionOrder po = new ProductionOrder(1L, new EmissionDate(new Date(11, 11, 2020))
            ,new ExecutionDatePrediction(new Date(11, 11, 2020)), p1, new Unit("metros"),new ProductQuantity(30));

    ProductionOrder po1 = new ProductionOrder(2L, new EmissionDate(new Date(11, 11, 2020))
            ,new ExecutionDatePrediction(new Date(11, 11, 2020)), p1, new Unit("metros"),new ProductQuantity(30));

    @Test
    void sameAsFalse() {
        assertFalse(po.sameAs(po1));
    }

    @Test
    void sameAsTrue() {
        assertTrue(po.sameAs(po));
    }

    @Test
    void identity() {
        Long exp = 00001L;
        Long result = po.identity();
        assertEquals(exp,result);
    }

}