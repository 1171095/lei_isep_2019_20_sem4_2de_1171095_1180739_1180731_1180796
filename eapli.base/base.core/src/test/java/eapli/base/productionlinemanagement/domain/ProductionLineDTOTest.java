package eapli.base.productionlinemanagement.domain;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.domain.*;
import eapli.base.productmanagement.domain.Description;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ProductionLineDTOTest {

    private Machine m = new Machine(new SerialNumber("1234"), new Description("wood cutter"),
            new InstallationDate(new Date(2014,05,03)),new Brand("nike"),
            new Model("AirForce"),new InternalCode("4490"));

    private Machine m2 = new Machine(new SerialNumber("123456"), new Description("Metal cutter"),
            new InstallationDate(new Date(2000,11,18)),new Brand("Adidas"),
            new Model("LeBronXI"),new InternalCode("4490-496"));

    private Machine m3 = new Machine(new SerialNumber("3456765456"), new Description("Plastic cutter"),
            new InstallationDate(new Date(2005,07,25)),new Brand("Puma"),
            new Model("Tigre"),new InternalCode("987896545"));



    private Deposit d1 = new Deposit(new Description("grande"));
    private Deposit d2 = new Deposit(new Description("pequeno"));
    private Deposit d3 = new Deposit(new Description("médio"));

    private Set<Deposit> lstDeposit = new HashSet<>();

}