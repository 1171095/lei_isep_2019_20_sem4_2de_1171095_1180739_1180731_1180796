package eapli.base.productionlinemanagement.domain;

import org.junit.jupiter.api.Test;

import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

class ProcessingRequestTest {

    @Test
    void hasRequest() {
        ProcessingRequest pr1 = new ProcessingRequest();
        assertEquals(pr1.HasRequest(),false);
    }

    @Test
    void setHasRequest() {
        ProcessingRequest pr1 = new ProcessingRequest();
        pr1.setHasRequest(true);
        assertEquals(pr1.HasRequest(),true);
    }

    @Test
    void getInitial_date() {
        ProcessingRequest pr1 = new ProcessingRequest();
        assertEquals(pr1.getInitial_date(),new GregorianCalendar());
    }

    @Test
    void setInitial_date() {
        ProcessingRequest pr1 = new ProcessingRequest();
        pr1.setInitial_date(new GregorianCalendar());
        assertEquals(pr1.getInitial_date(),new GregorianCalendar());
    }

    @Test
    void getFinal_date() {
        ProcessingRequest pr1 = new ProcessingRequest();
        assertEquals(pr1.getFinal_date(),new GregorianCalendar());
    }

    @Test
    void setFinal_date() {
        ProcessingRequest pr1 = new ProcessingRequest();
        pr1.setFinal_date(new GregorianCalendar());
        assertEquals(pr1.getFinal_date(),new GregorianCalendar());
    }
}