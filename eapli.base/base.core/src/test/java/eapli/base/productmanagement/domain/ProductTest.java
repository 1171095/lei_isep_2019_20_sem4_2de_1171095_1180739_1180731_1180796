package eapli.base.productmanagement.domain;

import eapli.base.productionformmanagement.domain.ItemUsed;
import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private Product p1 = new Product("00001",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));
    private Product p2 = new Product("00001",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));
    private Product p5 = new Product("00003",new CommercialCode("11110"),
            new BriefDescription("brief"),new Description("desc"),new Unit("unit"),
            new ProductCategory("cat"));
    private Product p3 = new Product();
    private BriefDescription p4 = new BriefDescription();
    private ItemUsed iu = new ItemUsed(new RawMaterial(p1,new RawMaterialCategory(new Description("desc"))),19.2);
    List<ItemUsed> lstItemsUsed = new ArrayList<>();

    private ProductionForm pf;





    @Test
    void testEquals() {
        assertEquals(p1,p2);
        assertNotEquals(p1,p5);
        assertNotEquals(p1,p3);
        assertNotEquals(p1,p4);
    }

    @Test
    void testHashCode() {
        assertEquals(p1.hashCode(),p2.hashCode());
        assertNotEquals(p1.hashCode(),p5.hashCode());

    }


    @Test
    void identity() {
        String exp = "00001";
        String result = p1.identity();
        assertEquals(exp,result);
    }

    @Test
    void toDTO() {

    }

    @Test
    void hasForm() {
        boolean exp = false;
        boolean result = p1.hasForm();
        assertEquals(exp,result);
    }

    @Test
    void setForm() {
        lstItemsUsed.add(iu);
        pf = new ProductionForm(lstItemsUsed);
        p1.setForm(pf);

        assertTrue(p1.hasForm());

    }
}