package eapli.base.productmanagement.domain;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class BriefDescriptionTest {

    private BriefDescription b1= new BriefDescription("brief1");
    private BriefDescription b2= new BriefDescription("brief2");
    private BriefDescription b3= new BriefDescription("brief2");
    private BriefDescription empty= new BriefDescription();
    private Description d3= new Description("brief2");

    @Test
    public void testValue(){
        assertNotEquals(b1.value(),b2.value());
        assertEquals(b3.value(),b2.value());
    }

}