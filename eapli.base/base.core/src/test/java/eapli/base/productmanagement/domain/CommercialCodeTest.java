package eapli.base.productmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommercialCodeTest {

    private CommercialCode c1 = new CommercialCode("12312");
    private CommercialCode c2 = new CommercialCode("12312");
    private CommercialCode c3 = new CommercialCode("55555");
    private CommercialCode empty = new CommercialCode();
    private BriefDescription brief = new BriefDescription("12312");



    @Test
    public void value() {
        assertEquals(c1.value(),c2.value());
        assertNotEquals(c1.value(),c3.value());
    }

}