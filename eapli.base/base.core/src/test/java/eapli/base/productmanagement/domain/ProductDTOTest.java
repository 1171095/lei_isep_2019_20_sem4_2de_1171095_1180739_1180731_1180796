package eapli.base.productmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductDTOTest {

    ProductDTO p = new ProductDTO("id", "Code", "desc", "unit", "cat", "brief");

    @Test
    void getId() {
        assertEquals("id", p.getId());
    }

    @Test
    void getBrief() {
        assertEquals("brief", p.getBrief());
    }

    @Test
    void getDesc() {
        assertEquals("desc", p.getDesc());
    }

    @Test
    void getUnit() {
        assertEquals("unit", p.getUnit());
    }

    @Test
    void getCat() {
        assertEquals("cat", p.getCat());
    }

    @Test
    void getCommercialCode() {
        assertEquals("Code", p.getCommercialCode());
    }
}