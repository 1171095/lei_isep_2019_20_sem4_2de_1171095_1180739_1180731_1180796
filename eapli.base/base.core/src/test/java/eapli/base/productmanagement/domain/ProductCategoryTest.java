package eapli.base.productmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductCategoryTest {

    ProductCategory c = new ProductCategory("s");

    @Test
    void value() {
        assertEquals("s", c.value());
    }
}