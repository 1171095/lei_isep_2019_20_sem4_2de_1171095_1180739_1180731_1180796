package eapli.base.productmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DescriptionTest {

    Description d = new Description("d1");

    @Test
    void value() {
        assertEquals("d1", d.value());
    }

    @Test
    void testToString() {
        assertEquals("d1", d.toString());
    }

    @Test
    void taVazio() {
        assertFalse(d.taVazio());
    }
}