package eapli.base.productmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UnitTest {

    Unit u = new Unit("a");

    @Test
    void value() {
        assertEquals(u.value(), "a");
    }
}