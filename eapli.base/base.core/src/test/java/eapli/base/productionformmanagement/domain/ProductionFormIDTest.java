package eapli.base.productionformmanagement.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductionFormIDTest {

    ProductionFormID id1 = new ProductionFormID(1L);
    ProductionFormID id2 = new ProductionFormID(2L);
    ProductionFormID id3 = new ProductionFormID(1L);

    @Test
    void compareToPos() {
        assertTrue(id1.compareTo(id2)<0);
    }
    @Test
    void compareToNeg() {
        assertTrue(id2.compareTo(id1)>0);
    }
    @Test
    void compareToZero() {
        assertTrue(id1.compareTo(id3)==0);
    }
}