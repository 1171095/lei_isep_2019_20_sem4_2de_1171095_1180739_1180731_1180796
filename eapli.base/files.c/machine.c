#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/conf.h>
#include <openssl/x509.h>

#define BUF_SIZE 512
#define SERVER_PORT "9999"
#define VERSION_OFFSET 0
#define CODE_OFFSET 1
#define ID_OFFSET 2
#define DATA_LENGTH_OFFSET 4
#define RAW_DATA_OFFSET 6
#define HELLO_REQUEST 0
#define MSG_REQUEST 1
#define CONFIG_REQUEST 2
#define RESET_REQUEST 3
#define ACK 150
#define NACK 151
#define THREADS 3
#define CONFIG_FILE "machineConfig.txt"

#define SERVER_SSL_CERT_FILE_JAVA "serverJ.pem"

#define SERVER_SSL_CERT_FILE_C "serverC.pem"
#define SERVER_SSL_KEY_FILE "serverC.key"
#define AUTH_CLIENTS_SSL_CERTS_FILE "authentic-clients.pem"

typedef struct {
    unsigned char msgCodeCS;
    char rawData[506];
    unsigned short machineID;
    int flag;
    char server_IP[30];
    char fileName[50];
    unsigned int msgDelay;
    char sslName[50];
} shared_data;

typedef struct {
    char VERSION;
    unsigned char CODE;
    unsigned short ID;
    short DATALENGTH;
    char RAWDATA[506];
} struct_msg;


pthread_mutex_t mutex_reset, mutex_sleep;


//udp Server
void* machine_udp_srv ( void* args){
    shared_data* lastMSGInformation = (shared_data*) args;
    struct sockaddr_storage client;
    int err, sock, res, i;
    unsigned int adl;
    char line[BUF_SIZE];
    char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE];
    struct addrinfo  req, *list;

    bzero((char *)&req,sizeof(req));
    // request a IPv6 local address will allow both IPv4 and IPv6 clients to use it
    req.ai_family = AF_INET;
    req.ai_socktype = SOCK_DGRAM;
    req.ai_flags = AI_PASSIVE;	// local address

    err=getaddrinfo(NULL, SERVER_PORT , &req, &list);

    if(err) {
        printf("Failed to get local address, error: %s\n",gai_strerror(err));
        exit(1);
    }

    sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
    if(sock==-1) {
        perror("Failed to open socket");
        freeaddrinfo(list);
        exit(1);
    }

    if(bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1) {
        perror("Bind failed");
        close(sock);
        freeaddrinfo(list);
        exit(1);
    }

    freeaddrinfo(list);

    puts("Listening for UDP requests (both over IPv6 or IPv4) from SMM. Use CTRL+C to terminate the server");

    adl=sizeof(client);
    while(1){
        res=recvfrom(sock,line,BUF_SIZE,0,(struct sockaddr *)&client,&adl);
        if(!getnameinfo((struct sockaddr *)&client,adl,
                        cliIPtext,BUF_SIZE,cliPortText,BUF_SIZE,NI_NUMERICHOST|NI_NUMERICSERV))
            printf("Machine %hu : Request from node %s, port number %s from SMM\n", lastMSGInformation->machineID, cliIPtext, cliPortText);
        else puts("Got request, but failed to get client address");

        //response to *HELLO_REQUEST*
        if(line[CODE_OFFSET]==HELLO_REQUEST){
            printf("Machine %hu : HELLO Request from node %s, port number %s from SMM\n", lastMSGInformation->machineID, cliIPtext, cliPortText);
            //defines version to default == 0
            line[VERSION_OFFSET] = 0;
            //saves the id of the last message sent from central system
            /*works in order to the monitorization service knows the response of the
            last request that the machine got*/
            line[CODE_OFFSET] = lastMSGInformation->msgCodeCS;
            //spliting the machineID between 2 bytes
            unsigned char byteFirstPartOfID = (unsigned) lastMSGInformation->machineID & 0xff;
            unsigned char byteSecondPartOfID = (unsigned) lastMSGInformation->machineID >> 8;
            //assign the bytes to the respective part of the line that refers to the id offset
            line[ID_OFFSET] = byteFirstPartOfID;
            line[ID_OFFSET + 1] = byteSecondPartOfID;

            //check if there is information on rawData
            if(lastMSGInformation->flag == 1){
                //get sizeof rawData
                int size = strlen(lastMSGInformation->rawData);
                //spliting the size of the raw data between 2 bytes
                unsigned char byteFirstPartOfDataSize = (unsigned) size & 0xff;
                unsigned char byteSecondPartOfDataSize = (unsigned) size >> 8;
                line[DATA_LENGTH_OFFSET] = byteFirstPartOfDataSize;
                line[DATA_LENGTH_OFFSET + 1] = byteSecondPartOfDataSize;

                //fill in the raw_data field
                for(i = 0; i<size; i++){
                    line[RAW_DATA_OFFSET + i] = lastMSGInformation->rawData[i];
                }

            }else {
                //when there is no information on rawData
                line[DATA_LENGTH_OFFSET] = 0;
            }

            //sends the message
            err = sendto(sock,line,res,0,(struct sockaddr *)&client,adl);
            //checks if there was an error when sending the message
            if (err < 0){
                perror("Error sending message");
                exit(1);
            }
            //resets the msg code to ack
            lastMSGInformation->msgCodeCS = ACK;
            //response to RESET_REQUEST
        }else if(line[CODE_OFFSET] == RESET_REQUEST){
            printf("Machine %hu : RESET REQUEST from node %s, port number %s from SMM\n", lastMSGInformation->machineID, cliIPtext,cliPortText);
            //unlocking the mutex because the other thread is trying to lock that mutex
            pthread_mutex_unlock(&mutex_reset);
            //locking the sleep mutex so it is forced to wait 15 seconds to simulate the reset of the machine
            pthread_mutex_lock(&mutex_sleep);
            sleep(15);
            //after waitng 15seconds it is now ready to resume the activity
            pthread_mutex_unlock(&mutex_sleep);

            line[VERSION_OFFSET] = 0;
            line[CODE_OFFSET] = lastMSGInformation->msgCodeCS;
            //check if there is information on rawData
            if(lastMSGInformation->flag == 1){
                //get sizeof rawData
                int size = strlen(lastMSGInformation->rawData);
                //spliting the size of the raw data between 2 bytes
                unsigned char byteFirstPartOfDataSize = (unsigned) size & 0xff;
                unsigned char byteSecondPartOfDataSize = (unsigned) size >> 8;
                line[DATA_LENGTH_OFFSET] = byteFirstPartOfDataSize;
                line[DATA_LENGTH_OFFSET + 1] = byteSecondPartOfDataSize;

                //fill in the raw_data field
                for(i = 0; i<size; i++){
                    line[RAW_DATA_OFFSET + i] = lastMSGInformation->rawData[i];
                }

            }else {
                //when there is no information on rawData
                line[DATA_LENGTH_OFFSET] = 0;
            }

            //sends the message
            err = sendto(sock,line,res,0,(struct sockaddr *)&client,adl);
            //checks if there was an error when sending the message
            if (err < 0){
                perror("Error sending message");
                exit(1);
            }
            //resets the msg code to ack
            lastMSGInformation->msgCodeCS = ACK;

        }else{
            printf("Invalid Request");
        }
    }
    //closing the socket
    close(sock);
    printf("\nMachine %hu : UDP Server shutdown\n", lastMSGInformation->machineID );
    exit(0);
}




void *machine_tcp_cli(void *args) {
    shared_data *machine = (shared_data *) args;
    struct_msg *msg = (struct_msg *) malloc(sizeof(struct_msg));
    int err, sock;
    struct addrinfo req, *list;
    //char sslLine[50];

    bzero((char *) &req, sizeof(req));
    // let getaddrinfo set the family depending on the supplied server address
    req.ai_family = AF_UNSPEC;
    req.ai_socktype = SOCK_STREAM;
    err = getaddrinfo(machine->server_IP, SERVER_PORT, &req, &list);
    if (err) {
        printf("Failed to get server address, error: %s\n", gai_strerror(err));
        exit(1);
    }
    sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
    if (sock == -1) {
        perror("Failed to open socket");
        freeaddrinfo(list);
        exit(1);
    }
    if (connect(sock, (struct sockaddr *) list->ai_addr, list->ai_addrlen) == -1) {
        perror("Failed connect");
        freeaddrinfo(list);
        close(sock);
        exit(1);
    }

    const SSL_METHOD *method = SSLv23_client_method();
    SSL_CTX *ctx = SSL_CTX_new(method);


    // Load client's certificate and key
    char sslLine[50];

    strcpy(sslLine, machine->sslName);
    strcat(sslLine, ".pem");
    SSL_CTX_use_certificate_file(ctx, sslLine, SSL_FILETYPE_PEM);
    strcpy(sslLine, machine->sslName);
    strcat(sslLine, ".key");
    SSL_CTX_use_PrivateKey_file(ctx, sslLine, SSL_FILETYPE_PEM);
    if (!SSL_CTX_check_private_key(ctx)) {
        puts("Error loading client's certificate/key");
        close(sock);
        exit(1);
    }


    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);

    // THE SERVER'S CERTIFICATE IS TRUSTED
    SSL_CTX_load_verify_locations(ctx, SERVER_SSL_CERT_FILE_JAVA, NULL);

    // Restrict TLS version and cypher suites
    SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);
    SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");

    SSL *sslConn = SSL_new(ctx);
    SSL_set_fd(sslConn, sock);

    if (SSL_connect(sslConn) != 1) {
        puts("TLS handshake error");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }
    printf("TLS version: %s\nCypher suite: %s\n", SSL_get_cipher_version(sslConn), SSL_get_cipher(sslConn));

    if (SSL_get_verify_result(sslConn) != X509_V_OK) {
        puts("Sorry: invalid server certificate");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }

    X509 *cert = SSL_get_peer_certificate(sslConn);
    X509_free(cert);

    if (cert == NULL) {
        puts("Sorry: no certificate provided by the server");
        SSL_free(sslConn);
        close(sock);
        exit(1);
    }

    //enviar HELLO REQUEST
    machine->flag = 0;
    msg->VERSION = 0;
    msg->CODE = HELLO_REQUEST;
    msg->ID = machine->machineID;
    msg->DATALENGTH = 0;
    printf("\nMachine %hu : Hello Request sent to SCM\n", machine->machineID);
    SSL_write(sslConn, msg, 6 + msg->DATALENGTH);

    SSL_read(sslConn, msg, 6 + msg->DATALENGTH);
    if (msg->CODE == ACK) {
        machine->msgCodeCS = msg->CODE;
        printf("\nMachine %hu : ACK Response received from SCM\n", machine->machineID);
    } else if (msg->CODE == NACK) {
        machine->msgCodeCS = msg->CODE;
        printf("\nMachine %hu : NACK Response received from SCM\n", machine->machineID);
        printf("\nMachine %hu : TCP Client shutdown\n", machine->machineID);
        close(sock);
        pthread_exit((void *) 1);
    } else {
        printf("\nMachine %hu : Invalid Response received from SCM\n", machine->machineID);
        printf("\nMachine %hu : TCP Client shutdown\n", machine->machineID);
        close(sock);
        pthread_exit((void *) 1);
    }
    SSL_free(sslConn);
    close(sock);

    //ler ficheiro e enviar MSG REQUEST
    char line[500];
    int len = 500;
    char nameFile[50];
    strcpy(nameFile, machine->fileName);
    FILE *file = fopen(nameFile, "r");
    if (file == NULL) {
        exit(1);
    }

    while (fgets(line, len, file) != 0) {
        sleep(machine->msgDelay);

        bzero((char *) &req, sizeof(req));
        // let getaddrinfo set the family depending on the supplied server address
        req.ai_family = AF_UNSPEC;
        req.ai_socktype = SOCK_STREAM;
        err = getaddrinfo(machine->server_IP, SERVER_PORT, &req, &list);
        if (err) {
            printf("Failed to get server address, error: %s\n", gai_strerror(err));
            exit(1);
        }
        sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
        if (sock == -1) {
            perror("Failed to open socket");
            freeaddrinfo(list);
            exit(1);
        }
        if (connect(sock, (struct sockaddr *) list->ai_addr, list->ai_addrlen) == -1) {
            perror("Failed connect");
            freeaddrinfo(list);
            close(sock);
            exit(1);
        }

        method = SSLv23_client_method();
        ctx = SSL_CTX_new(method);


        // Load client's certificate and key
        strcpy(sslLine, machine->sslName);
        strcat(sslLine, ".pem");
        SSL_CTX_use_certificate_file(ctx, sslLine, SSL_FILETYPE_PEM);
        strcpy(sslLine, machine->sslName);
        strcat(sslLine, ".key");
        SSL_CTX_use_PrivateKey_file(ctx, sslLine, SSL_FILETYPE_PEM);
        if (!SSL_CTX_check_private_key(ctx)) {
            puts("Error loading client's certificate/key");
            close(sock);
            exit(1);
        }


        SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, NULL);

        // THE SERVER'S CERTIFICATE IS TRUSTED
        SSL_CTX_load_verify_locations(ctx, SERVER_SSL_CERT_FILE_JAVA, NULL);

        // Restrict TLS version and cypher suites
        SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);
        SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");

        sslConn = SSL_new(ctx);
        SSL_set_fd(sslConn, sock);

        if (SSL_connect(sslConn) != 1) {
            puts("TLS handshake error");
            SSL_free(sslConn);
            close(sock);
            exit(1);
        }
        printf("TLS version: %s\nCypher suite: %s\n", SSL_get_cipher_version(sslConn), SSL_get_cipher(sslConn));

        if (SSL_get_verify_result(sslConn) != X509_V_OK) {
            puts("Sorry: invalid server certificate");
            SSL_free(sslConn);
            close(sock);
            exit(1);
        }

        cert = SSL_get_peer_certificate(sslConn);
        X509_free(cert);

        if (cert == NULL) {
            puts("Sorry: no certificate provided by the server");
            SSL_free(sslConn);
            close(sock);
            exit(1);
        }

        msg->VERSION = 0;
        msg->CODE = MSG_REQUEST;
        msg->ID = machine->machineID;
        msg->DATALENGTH = (unsigned short) strlen(line);
        strcpy(msg->RAWDATA, line);
        printf("\nMachine %hu Message sent: %s\n", machine->machineID, msg->RAWDATA);
        SSL_write(sslConn, msg, 6 + msg->DATALENGTH);

        SSL_read(sslConn, msg, 6 + msg->DATALENGTH);

        if (msg->DATALENGTH != 0) {
            strcpy(machine->rawData, msg->RAWDATA);
            machine->flag = 1;
        }
        if (msg->CODE == NACK) {
            machine->msgCodeCS = NACK;
            printf("\nMachine %hu : NACK Response received from SCM\n", machine->machineID);
            printf("\nMachine %hu : TCP Client shutdown\n", machine->machineID);
            close(sock);
            pthread_exit((void *) 1);
        } else if (msg->CODE == ACK) {
            printf("\nMachine %hu : ACK Response received from SCM\n", machine->machineID);
        } else {
            printf("\nMachine %hu : Invalid Response received from SCM\n", machine->machineID);
            SSL_free(sslConn);
            close(sock);
            pthread_exit((void *) 0);
        }
        SSL_free(sslConn);
        close(sock);
    }

    fclose(file);

    close(sock);
    printf("\nMachine %hu : TCP Client shutdown\n", machine->machineID);
    pthread_exit((void *) 0);
}

void *machine_tcp_srv(void *args) {
  shared_data *machine = (shared_data *) args;
  struct_msg *msg = (struct_msg *) malloc(sizeof(struct_msg));

  struct sockaddr_storage from;
  int err, newSock, sock;
  unsigned int adl;
  char cliIPtext[BUF_SIZE], cliPortText[BUF_SIZE];
  struct addrinfo  req, *list;

  bzero((char *)&req,sizeof(req));
  // requesting a IPv6 local address will allow both IPv4 and IPv6 clients to use it
  req.ai_family = AF_INET6;
  req.ai_socktype = SOCK_STREAM;
  req.ai_flags = AI_PASSIVE;      // local address

  err=getaddrinfo(NULL, SERVER_PORT , &req, &list);

  if(err) {
    printf("Failed to get local address, error: %s\n",gai_strerror(err)); exit(1);
  }

  sock=socket(list->ai_family,list->ai_socktype,list->ai_protocol);
  if(sock==-1) {
    perror("Failed to open socket"); freeaddrinfo(list); exit(1);}

    if(bind(sock,(struct sockaddr *)list->ai_addr, list->ai_addrlen)==-1) {
      perror("Bind failed");close(sock);freeaddrinfo(list);exit(1);
    }

    freeaddrinfo(list);

    listen(sock,SOMAXCONN);

    const SSL_METHOD *method;
        SSL_CTX *ctx;

	method = SSLv23_server_method();
        ctx = SSL_CTX_new(method);

	// Load the server's certificate and key
        SSL_CTX_use_certificate_file(ctx, SERVER_SSL_CERT_FILE_C, SSL_FILETYPE_PEM);
        SSL_CTX_use_PrivateKey_file(ctx, SERVER_SSL_KEY_FILE, SSL_FILETYPE_PEM);
        if (!SSL_CTX_check_private_key(ctx)) {
                puts("Error loading server's certificate/key");
		close(sock);
                exit(1);
        }

	// THE CLIENTS' CERTIFICATES ARE TRUSTED
        SSL_CTX_load_verify_locations(ctx,AUTH_CLIENTS_SSL_CERTS_FILE,NULL);

	// Restrict TLS version and cypher suite
        SSL_CTX_set_min_proto_version(ctx,TLS1_2_VERSION);
        SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");

	// The client must provide a certificate and it must be trusted, the handshake will fail otherwise
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);

    puts("Accepting TCP connections (both over IPv6 or IPv4). Use CTRL+C to terminate the server");

    adl=sizeof(from);
	for(;;) {
        	newSock=accept(sock,(struct sockaddr *)&from,&adl);
        	if(!fork()) {
                close(sock);
			    getnameinfo((struct sockaddr *)&from,adl,cliIPtext,BUF_SIZE,cliPortText,BUF_SIZE,
				NI_NUMERICHOST|NI_NUMERICSERV);
                printf("New connection from node %s, port number %s\n", cliIPtext, cliPortText);
			    SSL *sslConn = SSL_new(ctx);
        		SSL_set_fd(sslConn, newSock);
			    if(SSL_accept(sslConn)!=1) {
				    puts("TLS handshake error: client not authorized");
				    SSL_free(sslConn);
                	close(newSock);
				    exit(1);
				}
        		printf("TLS version: %s\nCypher suite: %s\n",
				SSL_get_cipher_version(sslConn),SSL_get_cipher(sslConn));
			    X509* cert=SSL_get_peer_certificate(sslConn);
        		X509_free(cert);
			    X509_NAME_oneline(X509_get_subject_name(cert),cliIPtext,BUF_SIZE);
        		printf("Client's certificate subject: %s\n",cliIPtext);

                FILE *fp;
                fp = fopen(CONFIG_FILE, "w");
                if(fp==NULL){
                    printf("\nError opening config file...\n");
                    exit(1);
                }

                SSL_read(sslConn,msg,BUF_SIZE);
                if(machine->machineID == msg->ID){
                    printf("\nMachine %hu : CONFIG REQUEST accepted...\n", machine->machineID);
                    fprintf(fp, "%s", msg->RAWDATA);
                    msg->VERSION = 0;
                    msg->CODE = ACK;
                    msg->ID = machine->machineID;
                    msg->DATALENGTH = 0;
                    char empty[506];
                    strcpy(msg->RAWDATA , empty);
                }else{
                    printf("\nMachine %hu : CONFIG REQUEST denied because machine ID don't match...\n", machine->machineID);
                    msg->VERSION = 0;
                    msg->CODE = NACK;
                    msg->ID = machine->machineID;
                    msg->DATALENGTH = 0;
                    char empty[506];
                    strcpy(msg->RAWDATA , empty);
                }

                SSL_write(sslConn,msg,BUF_SIZE);
                SSL_free(sslConn);
                close(newSock);
                printf("Connection from node %s, port number %s closed\n", cliIPtext, cliPortText);
                fclose(fp);
                exit(0);
            }
        close(newSock);
    }
    close(sock);

    printf("\nMachine %hu : TCP Server shutdown\n", machine->machineID);
    pthread_exit((void *) 0);
  }



//args:  id unico da machine, ip do servidor, nome do ficheiro, cadencia(delay)
int main(int argc, char *argv[]) {
    pthread_t threads[THREADS];
    int ret;
    int ret1;
    int ret2;

    if (argc != 6) {
        printf("\nNot enough arguments");
        printf("\nArguments required: machine_ID, server_IP, File_Name, Message_Delay, pem/key file name\n");
        exit(1);
    }
    shared_data *machine = (shared_data *) malloc(sizeof(shared_data));

    machine->machineID = (unsigned short) atoi(argv[1]);
    strcpy(machine->server_IP, argv[2]);
    strcpy(machine->fileName, argv[3]);
    machine->msgDelay = (unsigned int) atoi(argv[4]);
    strcpy(machine->sslName, argv[5]);

    //  US1011
    if (pthread_create(&threads[0], NULL, machine_tcp_cli, (void *) machine) == -1) {
        perror("Could not create thread");
        exit(1);
    }

    //  US1012
    if (pthread_create(&threads[1], NULL, machine_udp_srv, (void *) machine) == -1) {
        perror("Could not create thread");
        exit(1);
    }

    //  US1014
    if (pthread_create(&threads[2], NULL, machine_tcp_srv, (void *) machine) == -1) {
        perror("Could not create thread");
        exit(1);
    }

    //wait for the threads to close
    pthread_join(threads[0], (void *) &ret);
    pthread_join(threads[1], (void *) &ret1);
    pthread_join(threads[2], (void *) &ret2);

    return 0;
}
