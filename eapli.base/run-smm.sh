#!/usr/bin/env bash

#REM set the class path,
#REM assumes the build was executed with maven copy-dependencies
export BASE_CP=base.app.comunications/target/base.app.comunications-1.3.0-SNAPSHOT.jar:base.app.comunications/target/dependency/*;

#REM call the java VM, e.g,
java -cp $BASE_CP eapli.base.smm.MainSMM