package eapli.base.infrastructure.bootstrapers;

import eapli.base.machinemanagement.application.DefiningNewMachineController;
import eapli.base.machinemanagement.domain.*;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productmanagement.domain.Description;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;

public class MachineBootstrapper implements Action {

    private final DefiningNewMachineController dnmc = new DefiningNewMachineController();
    private static final Logger LOGGER = LogManager.getLogger(MachineBootstrapper.class);

    private void definingNewMachine(String internalCode, String serialNumber, Date dataInst, String model,String brand, String desc){
        try {
           Machine ma = dnmc.definingNewMachine(new SerialNumber(serialNumber),new Description(desc),new InstallationDate(dataInst), new Brand(brand),new Model(model),new InternalCode(internalCode));
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated value
            ex.printStackTrace();
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", internalCode);
            LOGGER.trace("Assuming existing record", ex);
        }

    }

    @Override
    public boolean execute() {
        definingNewMachine("18273612837","534534",new Date(2014-1900,05,03),"WoodCutter","CAT","wood cutter");
        definingNewMachine("17892348923","234234",new Date(2020-1900,12,03),"MetalShredder","MachinesLDA","metal shredder");
        definingNewMachine("98347593823","456456",new Date(2015-1900,10,03),"PlasticMelter","PM1002","plastic melter");
        definingNewMachine("90843597345","678676",new Date(2014-1900,05,12),"treta","CAT","glass ");
        definingNewMachine("12312312312","567433",new Date(2011-1900,05,12),"tretaCutter","CAT","ttreta cutter");
        definingNewMachine("23453423412","346566",new Date(2002-1900,05,12),"gluer","CAT","glues");
        definingNewMachine("98673423423","436345",new Date(2010-1900,05,12),"melter","CAT","melts");
        definingNewMachine("68431846163","123433",new Date(2008-1900,05,12),"s","CAT","package");

        return true;
    }
}
