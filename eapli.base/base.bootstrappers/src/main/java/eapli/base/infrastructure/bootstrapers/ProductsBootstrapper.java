package eapli.base.infrastructure.bootstrapers;

import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productmanagement.application.AddProductToCatalogController;
import eapli.base.productmanagement.domain.*;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductsBootstrapper implements Action {
    private final AddProductToCatalogController apc = new AddProductToCatalogController();
    private static final Logger LOGGER = LogManager.getLogger(ProductsBootstrapper.class);

    private void register(String id, String desc, String brief, String codCom, String pUnidade, String cat){

        try {
            apc.registerProduct(id ,new CommercialCode(codCom), new BriefDescription(brief), new Description(desc), new Unit(pUnidade), new ProductCategory(cat));
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated product
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", brief);
            LOGGER.trace("Assuming existing record", ex);
        }
    }


    @Override
    public boolean execute() {
        register("MET1","tubo de metal", "tubo", "19823891","UNIT","METAIS");
        register("PLAST1","folhas de plastico", "folhaPLa", "23423422","UNIT","PLASTICOS");
        register("VIDR1","vidro temperado", "VidTemp", "24564564","UNIT","VIDROS");
        register("MAD1","tabuas de madeira", "tabuas", "19823453","UNIT","MADEIRAS");

        return true;
    }
}
