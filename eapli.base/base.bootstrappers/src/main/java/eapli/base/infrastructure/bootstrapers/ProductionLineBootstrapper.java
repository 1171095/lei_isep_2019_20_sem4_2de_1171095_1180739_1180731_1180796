package eapli.base.infrastructure.bootstrapers;

import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.application.ProductionLineController;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.framework.actions.Action;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;

public class ProductionLineBootstrapper implements Action {
    ProductionLineController snplc = new ProductionLineController();
    private static final Logger LOGGER = LogManager.getLogger(ProductionLineBootstrapper.class);

    private void registerProductionLine (Long machine1ID, Long machine2ID, Long machine3ID){
        snplc.listMachines();
        snplc.addMachineByID(machine1ID,0);
        snplc.addMachineByID(machine3ID,1);
        snplc.addMachineByID(machine2ID,2);
        snplc.save();

    }

    @Override
    public boolean execute() {
        registerProductionLine(15L,16L,18L);
        return true;
    }
}
