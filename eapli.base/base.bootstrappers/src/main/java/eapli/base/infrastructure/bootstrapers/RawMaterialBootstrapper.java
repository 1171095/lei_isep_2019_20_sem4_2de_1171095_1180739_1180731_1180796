package eapli.base.infrastructure.bootstrapers;

import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Product;
import eapli.base.rawmaterialmanagement.application.DefineNewRawMaterialCategoryController;
import eapli.base.rawmaterialmanagement.application.DefiningNewRawMaterialController;
import eapli.base.rawmaterialmanagement.application.RawMaterialCategoryManagementService;
import eapli.base.rawmaterialmanagement.application.RawMaterialManagementService;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;


public class RawMaterialBootstrapper implements Action {


    private static final Logger LOGGER = LogManager.getLogger(RawMaterialBootstrapper.class);

    private void definingNewRawMaterialMaterial(Long idCat, String idMAterial, String desc){
        final DefiningNewRawMaterialController controller = new DefiningNewRawMaterialController();

        try{
            controller.selectRawMaterialCategory(idCat);
            controller.addMaterialRawMaterial(idMAterial, desc);
        }catch(final IntegrityViolationException | ConcurrencyException ex){
            LOGGER.warn("Assuming {} already exists(activate ttrace log for details)", desc);
            LOGGER.trace("Assuming existing record", ex);
        }


    }

    private void definingNewRawMaterialProduct(Long idCat, String idProd){
        final RawMaterialCategoryManagementService catService = new RawMaterialCategoryManagementService();
        final RawMaterialManagementService rawMatService = new RawMaterialManagementService();
        final ProductManagementService prodService = new ProductManagementService();

        RawMaterialCategory cat;
        Product prod;

        try{
            Optional<RawMaterialCategory> optCat = catService.getCategotyByID(idCat);
            if(!optCat.isPresent()){
                cat = null;
            }
            cat = optCat.get();

            Optional<Product> optProd = prodService.getProductByID(idProd);
            prod = optProd.get();
            rawMatService.definingRawMaterialProduct(prod, cat);
        }catch(final IntegrityViolationException | ConcurrencyException ex){
            LOGGER.warn("Assuming {} already exists(activate ttrace log for details)", idProd);
            LOGGER.trace("Assuming existing record", ex);
        }
    }

    @Override
    public boolean execute() {
        definingNewRawMaterialMaterial(10L,"Mat1", "MAterial 1-1");
        definingNewRawMaterialMaterial(11L,"Mat1", "MAterial 1-2");
        definingNewRawMaterialMaterial(12L,"Mat2", "MAterial 1-3");
        definingNewRawMaterialMaterial(13L,"Mat3", "MAterial 1-4");


        definingNewRawMaterialProduct(9L, "MET1");
        definingNewRawMaterialProduct(14L, "PLAST1");

        return true;
    }
}
