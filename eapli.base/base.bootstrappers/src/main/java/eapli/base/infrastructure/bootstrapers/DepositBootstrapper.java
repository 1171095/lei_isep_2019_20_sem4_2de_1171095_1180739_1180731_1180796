package eapli.base.infrastructure.bootstrapers;

import eapli.base.depositmanagement.application.SpecifyNewDepositController;
import eapli.base.productmanagement.domain.Description;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DepositBootstrapper implements Action {
    private final SpecifyNewDepositController sndc = new SpecifyNewDepositController();
    private static final Logger LOGGER = LogManager.getLogger(DepositBootstrapper.class);

    private void  specifyNewDeposit(String string){
        try{
            sndc.specifyNewDeposit(new Description(string));
        } catch (final IntegrityViolationException | ConcurrencyException ex){
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated value
            ex.printStackTrace();
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", string);
            LOGGER.trace("Assuming existing record", ex);
        }

    }


    @Override
    public boolean execute() {
        specifyNewDeposit("Madeiras");
        specifyNewDeposit("Plasticos");
        specifyNewDeposit("Vidros");
        specifyNewDeposit("Metais");

        return true;
    }
}
