package eapli.base.infrastructure.bootstrapers;

import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.application.DefineNewRawMaterialCategoryController;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.framework.actions.Action;

import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RawMaterialCategoryBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(RawMaterialCategory.class);
    DefineNewRawMaterialCategoryController dnrmc = new DefineNewRawMaterialCategoryController();

    private void registerRawMaterialCategory (String cat){
        final DefineNewRawMaterialCategoryController controller = new DefineNewRawMaterialCategoryController();

        try{
            controller.addRawMaterialCategory(new Description(cat));
        }catch(final IntegrityViolationException | ConcurrencyException ex){
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", cat);
            LOGGER.trace("Assuming existing record", ex);
        }



    }

    @Override
    public boolean execute() {
        registerRawMaterialCategory("plasticos");
        registerRawMaterialCategory("madeiras");
        registerRawMaterialCategory("tecidos");
        registerRawMaterialCategory("metais");
        registerRawMaterialCategory("minerios");
        registerRawMaterialCategory("vidros");

        return true;
    }
}
