<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xs="http://www.dei.isep.ipp.pt/lprog"
				version="2.0">
	<xsl:output method="html" cdata-section-elements="xs:bloco"/>
	<xsl:template match="/">
		<html>
            <head>
                <style>
            html,body {
            background: url(background.jpg) no-repeat center center fixed;
            -webkit-background-size: cover; /* For WebKit*/
            -moz-background-size: cover; /* Mozilla*/
            -o-background-size: cover; /* Opera*/
            background-size: cover; /* Generic*/
            }
            div.a {
            text-align: center;
            }
                </style>
            </head>
			<body>
				<h1 style="font-family:verdana" align="center">Relatório</h1>
				<center><img align="middle" src="logotipoDEI.png" width="300" height="100"/></center>
				<br/>
				<xsl:apply-templates select="//xs:relatório/xs:páginaRosto"/>
				<h3 align="center">---------------------------------------------------------------------------</h3>
				<h3 align="center">Índice</h3>
                <ul >
                    <li><a href="#div_introducao">Introdução</a></li>
                    <li><a href="#div_analise">Análise do Problema</a></li>
                    <li><a href="#div_linguagem">Linguagem</a></li>
                    <li><a href="#div_transformacoes">Transformações XSLT</a></li>
                    <li><a href="#div_conclusao">Conclusão</a></li>
                    <li><a href="#div_refs">Referências</a></li>
                    <li><a href="#div_anexos">Anexos</a></li>
                </ul>
				<h3 align="center">---------------------------------------------------------------------------</h3>
				<xsl:apply-templates select="//xs:relatório/xs:corpo"/>
                <h3 align="center">---------------------------------------------------------------------------</h3>
                <div id="div_anexos">
                <xsl:apply-templates select="//xs:relatório/xs:anexos"/>
                </div>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="//xs:relatório/xs:páginaRosto">

		<h2 align="center"><xsl:value-of select="xs:tema"/></h2>
		<h3><xsl:value-of select="@disciplina"/></h3>
		<ul align="center" style="list-style-type:none">
			<li><xsl:value-of select="xs:disciplina/xs:designação"/></li>
			<li><xsl:value-of select="xs:disciplina/xs:anoCurricular"/>º Ano</li>
		</ul>
		<h4>Professores:</h4>
			<table border = "1">
				<xsl:for-each select="xs:professor">
					<tr>
						<td><xsl:value-of select="@sigla"/></td>
						<td><xsl:value-of select="."/></td>
						<td><xsl:value-of select="@tipoAula"/></td>
					</tr>
				</xsl:for-each>
			</table>
		<br/>
		<h4>Autores:</h4>
			<table border = "2">
				<tr>
					<td rowspan="0"><xsl:value-of select="xs:turma"/></td>
				</tr>
				<xsl:for-each select="xs:autor">
					<tr>
						<td><xsl:value-of select="./xs:nome"/></td>
						<td><xsl:value-of select="./xs:número"/></td>
						<td><xsl:value-of select="./xs:mail"/></td>
					</tr>
				</xsl:for-each>
			</table>
	</xsl:template>

    <xsl:template match="//xs:relatório/xs:corpo">

    <div id="div_introducao">
        <xsl:apply-templates select="xs:introdução"/>
    </div>
		
    
        <xsl:apply-templates select="xs:outrasSecções"/>
   

    <div id="div_conclusao">
        <xsl:apply-templates select="xs:conclusão"/>
    </div>

    <div id="div_refs">
        <xsl:apply-templates select="xs:referências"/>
    </div>

	</xsl:template>


	<xsl:template match="xs:introdução">
		<h3 align="center">Introdução </h3>
		<xsl:apply-templates select="xs:parágrafo"/>
	</xsl:template>

	<xsl:template match="xs:outrasSecções">
        <div id="div_analise">
            <xsl:apply-templates select="xs:análise"/>
        </div>

        <div id="div_linguagem">
            <xsl:apply-templates select="xs:linguagem"/>
        </div>

        <div id="div_transformacoes">
            <xsl:apply-templates select="xs:transformações"/>
        </div>
	</xsl:template>

	<xsl:template match="xs:análise | xs:linguagem | xs:transformações | xs:conclusão | xs:anexos">
		<h3 align="center"><xsl:value-of select="@tituloSecção"/></h3>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="xs:parágrafo">
		<p><xsl:apply-templates/></p>
	</xsl:template>

	<xsl:template match="xs:itálico">
		<i><xsl:value-of select="."/></i>
	</xsl:template>

	<xsl:template match="xs:subsecção">
		<h4><xsl:value-of select="."/></h4>
	</xsl:template>

	<xsl:template match="xs:listaItems">
		<ul style="list-style-type:none">
			<xsl:for-each select="xs:item">
				<li><xsl:value-of select="."/></li>
			</xsl:for-each>
		</ul>
	</xsl:template>

	<xsl:template match="xs:figura">
		<img src="{@src}" alt="{@descrição}"/>
	</xsl:template>
	
	<xsl:template match="xs:codigo">
		<xsl:apply-templates select="xs:bloco" />
	</xsl:template>

	<xsl:template match="xs:bloco">
		<pre>
			<xsl:value-of select="."></xsl:value-of> 

		</pre>
			
	</xsl:template>

	<xsl:template match="xs:referências">
		<h3 align="center"><xsl:value-of select="@tituloSecção"/></h3>
		<center><table border = "1">
			<tr bgcolor = "#c6cbd3">
				<th>URL</th>
				<th>Descrição</th>
				<th>Consultado em</th>
			</tr>
			<xsl:for-each select="xs:refWeb">
				<tr>
					<td><xsl:value-of select="xs:URL"/></td>
					<td><xsl:value-of select="xs:descrição"/></td>
					<td><xsl:value-of select="xs:consultadoEm"/></td>
				</tr>
			</xsl:for-each>
		</table></center>
	</xsl:template>

</xsl:stylesheet>
