<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="text"/>

    <xsl:template match="FactoryFloor">
        <xsl:text>=====================</xsl:text>

        <xsl:text>&#xa;||  Parcels: nr-</xsl:text>
        <xsl:value-of select="count(Parcels/Parcel)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>=====================</xsl:text>
        <xsl:apply-templates select="Parcels"/>

    </xsl:template>

    <xsl:template match="Parcels">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Parcel"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="Parcel">
        <xsl:text>========================================================================================================</xsl:text>
        <xsl:text>&#xa;Parcel ID: </xsl:text>
        <xsl:value-of select="parcelID"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;   =>Production Orders: </xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <li>
            
            <xsl:apply-templates select="productionOrder"/>
        </li>
       
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="productionOrder">
        <li>
            <xsl:text>&#xa;        -> Production Order ID : <xsl:value-of select="productionOrderID"/></xsl:text>
            <xsl:text>&#xa;        -> Emission Date : <xsl:value-of select="emissionDate/emissionDate"/></xsl:text>
            <xsl:text>&#xa;        -> Execution Prediction Date: <xsl:value-of select="executionDatePrediction/executionDatePrediction"/></xsl:text>
            <xsl:text>&#xa;        -> Product: <xsl:value-of select="product/productionID"/></xsl:text>
            <xsl:text>&#xa;        -> Unit: <xsl:value-of select="unit/unit"/></xsl:text>
            <xsl:text>&#xa;        -> Product Quantity: <xsl:value-of select="productQuantity/productQuantity"/></xsl:text>
            <xsl:text>&#xa;        -> Production Order State: <xsl:value-of select="productionOrderState"/></xsl:text>
            <xsl:text>&#xa;____________________________________________________________</xsl:text>
        </li>
    </xsl:template>

</xsl:stylesheet>