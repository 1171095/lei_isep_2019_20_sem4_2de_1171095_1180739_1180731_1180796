<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="text"/>


    <xsl:template match="FactoryFloor">

        <xsl:text>============================</xsl:text>

        <xsl:text>&#xa;||  Products: nr-    </xsl:text>

        <xsl:value-of select="count(Products/Product)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>============================</xsl:text>
        <xsl:apply-templates select="Products"/>
        
        

        <xsl:text>============================</xsl:text>

        <xsl:text>&#xa;||  Raw Materials: nr-   </xsl:text>
        <xsl:value-of select="count(RawMaterials/RawMaterial)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>============================</xsl:text>
        <xsl:apply-templates select="RawMaterials"></xsl:apply-templates>
        

        <xsl:text>=======================</xsl:text>

        <xsl:text>&#xa;||  Materials: nr-</xsl:text>
        <xsl:value-of select="count(Materials/Material)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>=======================</xsl:text>
        <xsl:apply-templates select="Materials"></xsl:apply-templates>
        

        <xsl:text>===============================</xsl:text>
        <xsl:text>&#xa;|| Production Orders: nr-</xsl:text>
        <xsl:value-of select="count(ProductionOrders/ProductionOrder)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>===============================</xsl:text>
        <xsl:apply-templates select="ProductionOrders"></xsl:apply-templates>
 

        <xsl:text>======================</xsl:text>

        <xsl:text>&#xa;||  Machines: nr-</xsl:text>
        <xsl:value-of select="count(Machines/Machine)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>======================</xsl:text>
        <xsl:apply-templates select="Machines"></xsl:apply-templates>


        <xsl:text>======================</xsl:text>
        
        <xsl:text>&#xa;||  Deposits: nr-</xsl:text>
        <xsl:value-of select="count(Deposits/Deposit)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>======================</xsl:text>
        <xsl:apply-templates select="Deposits"></xsl:apply-templates>


        <xsl:text>=============================</xsl:text>
        
        <xsl:text>&#xa;||  ProductionLines: nr-</xsl:text>
        <xsl:value-of select="count(ProductionLines/ProductionLine)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>=============================</xsl:text>
        <xsl:apply-templates select="ProductionLines"></xsl:apply-templates>


        <xsl:text>=====================</xsl:text>

        <xsl:text>&#xa;||  Parcels: nr-</xsl:text>
        <xsl:value-of select="count(Parcels/Parcel)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>=====================</xsl:text>
        <xsl:apply-templates select="Parcels"></xsl:apply-templates>

    </xsl:template>

    <xsl:template match="Products">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Product"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="RawMaterials">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="RawMaterial"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="Materials">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Material"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="ProductionOrders">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="ProductionOrder"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="Machines">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Machine"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="Deposits">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Deposit"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="ProductionLines">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="ProductionLine"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    <xsl:template match="Parcels">
        <xsl:text>&#xa;</xsl:text>
        <xsl:apply-templates select="Parcel"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="Product">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Production ID: </xsl:text>
        <xsl:value-of select="productionID"/>
        <xsl:text>&#xa;Commercial Code: </xsl:text>
        <xsl:value-of select="commercialID/code"/>
        <xsl:text>&#xa;Brief Description: </xsl:text>
        <xsl:value-of select="briefDescription/briefDescription"/>
        <xsl:text>&#xa;Description: </xsl:text>
        <xsl:value-of select="productDescription/description"/>
        <xsl:text>&#xa;Unit: </xsl:text>
        <xsl:value-of select="unit/unit"/>
        <xsl:text>&#xa;Product Category: </xsl:text>
        <xsl:value-of select="productCategory/productCategory"/>
        <xsl:text>&#xa;Poduction Form: </xsl:text>
        <xsl:choose>
            <xsl:when test="productionForm !=''">
                <xsl:value-of select="productionForm/productionFormId"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>no production form</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>&#xa;</xsl:text>
        
    </xsl:template>

    <xsl:template match="RawMaterial">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Raw Material ID: </xsl:text>
        <xsl:value-of select="rawMaterialID"/>
        <xsl:text>&#xa;Raw Material Category: </xsl:text>
        <xsl:value-of select="rawMaterialCategory/rawMaterialCategoryID"/>
        <xsl:text>&#xa;</xsl:text>

    </xsl:template>

    <xsl:template match="Material">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Material ID: </xsl:text>
        <xsl:value-of select="materialID"/>
        <xsl:text>&#xa;Material description: </xsl:text>
        <xsl:value-of select="description/description"/>
        <xsl:text>&#xa;</xsl:text>

    </xsl:template>

    <xsl:template match="ProductionOrder">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Production Order ID: </xsl:text>
        <xsl:value-of select="productionOrderID"/>
        <xsl:text>&#xa;Emission Date: </xsl:text>
        <xsl:value-of select="emissionDate/emissionDate"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Execution Date Prediction: </xsl:text>
        <xsl:value-of select="executionDatePrediction/executionDatePrediction"/>
        <xsl:text>&#xa;Product ID: </xsl:text>
        <xsl:value-of select="product/productionID"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Unit: </xsl:text>
        <xsl:value-of select="unit/unit"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Product Quantity: </xsl:text>
        <xsl:value-of select="productQuantity/productQuantity"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Production Order State: </xsl:text>
        <xsl:value-of select="productionOrderState"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="Machine">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Machine ID: </xsl:text>
        <xsl:value-of select="machineID"/>
        <xsl:text>&#xa;Internal Code: </xsl:text>
        <xsl:value-of select="internalCode/internalCode"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Serial Number: </xsl:text>
        <xsl:value-of select="serialNumber/serialNumber"/>
        <xsl:text>&#xa;Description: </xsl:text>
        <xsl:value-of select="description/description"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Installation Date: </xsl:text>
        <xsl:value-of select="installationDate/installationDate"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Brand: </xsl:text>
        <xsl:value-of select="brand/brand"/>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Model: </xsl:text>
        <xsl:value-of select="model/model"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="Deposit">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Deposit ID: </xsl:text>
        <xsl:value-of select="depositID"/>
        <xsl:text>&#xa;Description: </xsl:text>
        <xsl:value-of select="description/description"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="ProductionLine">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Production Line ID: </xsl:text>
        <xsl:value-of select="productionLineID"/>
        <xsl:text>&#xa;Machines: </xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <xsl:for-each select="machineMap/entry/value/machineID">
            <li>
                <xsl:text>-> </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text>&#xa;</xsl:text>
            </li>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="Parcel">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Parcel ID: </xsl:text>
        <xsl:value-of select="parcelID"/>
        <xsl:text>&#xa;Production Orders: </xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <xsl:for-each select="productionOrder/productionOrderID">
            <li>
                <xsl:text>-> </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text>&#xa;</xsl:text>
            </li>
        </xsl:for-each>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>


</xsl:stylesheet>