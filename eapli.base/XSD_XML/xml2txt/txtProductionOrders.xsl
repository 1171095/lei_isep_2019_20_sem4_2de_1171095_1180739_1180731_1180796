<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="text"/>

    <xsl:template match="FactoryFloor">

        <xsl:text>===============================</xsl:text>
        <xsl:text>&#xa;|| Production Orders: nr-</xsl:text>
        <xsl:value-of select="count(ProductionOrders/ProductionOrder)"/>
        <xsl:text>  || &#xa;</xsl:text>
        <xsl:text>===============================</xsl:text>
        <xsl:apply-templates select="ProductionOrders"></xsl:apply-templates>

    </xsl:template>

    <xsl:template match="ProductionOrders">
        <xsl:text>&#xa;</xsl:text>
        <li>
            <xsl:apply-templates select="ProductionOrder"/>
        </li>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="ProductionOrder">
        <xsl:text>----------------------------------------</xsl:text>
        <xsl:text>&#xa;Production Order ID: <xsl:value-of select="productionOrderID"/> </xsl:text>

        <xsl:text>&#xa;Emission Date: <xsl:value-of select="emissionDate/emissionDate"/></xsl:text>

        
        <xsl:text>&#xa;Execution Date Prediction: <xsl:value-of select="executionDatePrediction/executionDatePrediction"/></xsl:text>
        <xsl:text>&#xa;</xsl:text>
        <xsl:text>&#xa;Product: <xsl:apply-templates select="product"/></xsl:text>
        
        
        <xsl:text>&#xa;Unit: <xsl:value-of select="unit/unit"/></xsl:text>

 
        <xsl:text>&#xa;Product Quantity: <xsl:value-of select="productQuantity/productQuantity"/> </xsl:text>


        <xsl:text>&#xa;Production Order State: <xsl:value-of select="productionOrderState"/></xsl:text>

        <xsl:text>&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="product">
        
        <xsl:text>&#xa;     Production ID: </xsl:text>
        <xsl:value-of select="productionID"/>
        <xsl:text>&#xa;     Commercial Code: </xsl:text>
        <xsl:value-of select="commercialID/code"/>
        <xsl:text>&#xa;     Brief Description: </xsl:text>
        <xsl:value-of select="briefDescription/briefDescription"/>
        <xsl:text>&#xa;     Description: </xsl:text>
        <xsl:value-of select="productDescription/description"/>
        <xsl:text>&#xa;     Unit: </xsl:text>
        <xsl:value-of select="unit/unit"/>
        <xsl:text>&#xa;     Product Category: </xsl:text>
        <xsl:value-of select="productCategory/productCategory"/>
        <xsl:text>&#xa;     Poduction Form: </xsl:text>
        <xsl:choose>
            <xsl:when test="productionForm !=''">
                <xsl:value-of select="productionForm/productionFormId"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>no production form</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>&#xa;</xsl:text>

    </xsl:template>



</xsl:stylesheet>