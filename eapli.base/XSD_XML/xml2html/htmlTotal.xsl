<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>

    <xsl:template match="/FactoryFloor">
        <html>
            <body>
                <u style="color:#e36700">
                <h1 style="font-size:400%; color:#e36700; font-family:verdana;"  align="center">FactoryFloor</h1>
                </u>
            
            <head>
                <style style="opacity:0.5">
            html,body {
            background: url(pl.png) no-repeat center center fixed;
            -webkit-background-size: cover; /* For WebKit*/
            -moz-background-size: cover; /* Mozilla*/
            -o-background-size: cover; /* Opera*/
            background-size: cover; /* Generic*/
            }
            div.a {
            text-align: center;
            }
                </style>
            </head>
            </body>
            <xsl:apply-templates select="/FactoryFloor/Products"></xsl:apply-templates>
            <xsl:apply-templates select="/FactoryFloor/RawMaterials"/>
            <xsl:apply-templates select="/FactoryFloor/Materials"/>
            <xsl:apply-templates select="/FactoryFloor/ProductionOrders"/>
            <xsl:apply-templates select="/FactoryFloor/Machines"/>
            <xsl:apply-templates select="/FactoryFloor/Deposits"/>
            <xsl:apply-templates select="/FactoryFloor/ProductionLines"/>
            <xsl:apply-templates select="/FactoryFloor/Parcels"/>
        </html>
        
    </xsl:template>

    <xsl:template match="/FactoryFloor/Products">
        <html>
            <body>
                <h2 style="color:#ffad33; font-family:verdana" align="center">Products</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead >
                        <tr bgcolor="#ffad33">
                            <th style="text-align:left">ProductionId</th>
                            <th style="text-align:left">CommercialCode</th>
                            <th style="text-align:left">BriefDescription</th>
                            <th style="text-align:left">Description</th>
                            <th style="text-align:left">Unit</th>
                            <th style="text-align:left">ProductCategory</th>
                            <th style="text-align:left">PoductionForm</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="Product"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Product">
        <tr>
            <td>
                <xsl:value-of select="productionID"/>
            </td>
            <td>
                <xsl:value-of select="commercialID/code"/>
            </td>
            <td>
                <xsl:value-of select="briefDescription/briefDescription"/>
            </td>
            <td>
                <xsl:value-of select="productDescription/description"/>
            </td>
            <td>
                <xsl:value-of select="unit/unit"/>
            </td>
            <td>
                <xsl:value-of select="productCategory/productCategory"/>
            </td>
            <td>
                <xsl:choose>
                    <xsl:when test="productionForm !=''">
                        <xsl:value-of select="productionForm/productionFormId"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>no production form</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="/FactoryFloor/RawMaterials">
        <html>
            <body>
                <h2 style="color:#008080; font-family:verdana" align="center">Raw Materials</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#008080">
                            <th style="text-align:left">RawMaterialID</th>
                            <th style="text-align:left">Category</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="RawMaterial"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="RawMaterial">
        <tr>
            <td>
                <xsl:value-of select="rawMaterialID"/>
            </td>
            <td>
                <xsl:value-of select="rawMaterialCategory/rawMaterialCategoryID"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="FactoryFloor/Materials">
        <html>
            <body>
                <h2 style="color:#d24dff; font-family:verdana" align="center">Materials</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#d24dff">
                            <th style="text-align:left">MaterialID</th>
                            <th style="text-align:left">Description</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="Material"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Material">
        <tr>
            <td>
                <xsl:value-of select="materialID"/>
            </td>
            <td>
                <xsl:value-of select="description/description"/>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="FactoryFloor/ProductionOrders">´
        <html>
            <body>
                <h2 style="color:#cc4400; font-family:verdana" align="center">Production Orders</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#cc4400">
                            <th style="text-align:left">ProductionOrderID</th>
                            <th style="text-align:left">EmissionDate</th>
                            <th style="text-align:left">ExecutionDatePrediction</th>
                            <th style="text-align:left">ProductID</th>
                            <th style="text-align:left">Unit</th>
                            <th style="text-align:left">ProductQuantity</th>
                            <th style="text-align:left">State</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="ProductionOrder"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="ProductionOrder">
        <tr>
            <td>
                <xsl:value-of select="productionOrderID"/>
            </td>
            <td>
                <xsl:value-of select="emissionDate/emissionDate"/>
            </td>
            <td>
                <xsl:value-of select="executionDatePrediction/executionDatePrediction"/>
            </td>
            <td>
                <xsl:value-of select="product/productionID"/>
            </td>
            <td>
                <xsl:value-of select="unit/unit"/>
            </td>
            <td>
                <xsl:value-of select="productQuantity/productQuantity"/>
            </td>
            <td>
                <xsl:value-of select="productionOrderState"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="FactoryFloor/Machines">
        <html>
            <body>
                <h2 style="color:#c44dff; font-family:verdana" align="center">Machines</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#c44dff">
                            <th style="text-align:left">MachineID</th>
                            <th style="text-align:left">InternalCode</th>
                            <th style="text-align:left">SerialNumber</th>
                            <th style="text-align:left">Description</th>
                            <th style="text-align:left">InstallationDate</th>
                            <th style="text-align:left">brand</th>
                            <th style="text-align:left">model</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="Machine"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Machine">
        <tr>
            <td>
                <xsl:value-of select="machineID"/>
            </td>
            <td>
                <xsl:value-of select="internalCode/internalCode"/>
            </td>
            <td>
                <xsl:value-of select="serialNumber/serialNumber"/>
            </td>
            <td>
                <xsl:value-of select="description/description"/>
            </td>
            <td>
                <xsl:value-of select="installationDate/installationDate"/>
            </td>
            <td>
                <xsl:value-of select="brand/brand"/>
            </td>
            <td>
                <xsl:value-of select="model/model"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="/FactoryFloor/Deposits">
        <html>
            <body>
                <h2 style="color:#9acd32; font-family:verdana" align="center">Deposits</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#9acd32">
                            <th style="text-align:left">DepositID</th>
                            <th style="text-align:left">Description</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="Deposit"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Deposit">
        <tr>
            <td>
                <xsl:value-of select="depositID"/>
            </td>
            <td>
                <xsl:value-of select="description/description"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="/FactoryFloor/ProductionLines">
        <html>
            <body>
                <h2 style="color:#ee5a3a; font-family:verdana" align="center">Production Lines</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#ee5a3a">
                            <th style="text-align:left">ProductionLineID</th>
                            <th style="text-align:left">MachineID</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="ProductionLine"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="ProductionLine">
        <tr>
            <td>
                <xsl:value-of select="productionLineID"/>
            </td>
            <td>
                <ul>
                    <xsl:for-each select="machineMap/entry/value/machineID">
                        <li>
                            <xsl:value-of select="."/>
                        </li>
                    </xsl:for-each>
                </ul>
            </td>
        </tr>

    </xsl:template>
    <xsl:template match="/FactoryFloor/Parcels">
        <html>
            <body>
                <h2 style="color:#4053f3; font-family:verdana" align="center">Parcels</h2>
                <table border="1" style="background-color: rgba(255,255,255,0.7)" align="center">
                    <thead>
                        <tr bgcolor="#4053f3">
                            <th style="text-align:left">ParcelID</th>
                            <th style="text-align:left">ProductionOrders</th>
                        </tr>
                    </thead>
                    <xsl:apply-templates select="Parcel"/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Parcel">
        <tr>
            <td>
                <xsl:value-of select="parcelID"/>
            </td>
            <td>
                <ul>
                    <xsl:for-each select="productionOrder/productionOrderID">
                        <li>
                            <xsl:value-of select="."/>
                        </li>
                    </xsl:for-each>
                </ul>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>