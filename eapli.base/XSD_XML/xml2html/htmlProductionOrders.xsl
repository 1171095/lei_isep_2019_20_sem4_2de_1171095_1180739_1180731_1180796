<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/FactoryFloor/ProductionOrders">
    <html>
        <head>
            <style style="opacity:0.5">
        html,body {
        background: url(po.png) no-repeat center center fixed;
        -webkit-background-size: cover; /* For WebKit*/
        -moz-background-size: cover; /* Mozilla*/
        -o-background-size: cover; /* Opera*/
        background-size: cover; /* Generic*/
        }
        div.a {
        text-align: center;
        }
            </style>
        </head>
        <body>
            <u>
                <h1 style="font-size:400%; color:black; font-family:verdana;" align="center">Production Orders</h1>
            </u>
            
                <xsl:for-each select="ProductionOrder">
                    <h2>Production order - <xsl:value-of select="productionOrderID"/> </h2>
                    <ul>
                        
                        <li> <p>Emission Date: <xsl:value-of select="emissionDate/emissionDate"/></p></li>
                        <li> <p>Execution Prediction Date: <xsl:value-of select="executionDatePrediction/executionDatePrediction"/> </p></li>
                        <li> <p>Product:<xsl:apply-templates select="product"/></p></li>
                        <li> <p>Unit: <xsl:value-of select="unit/unit"/></p></li>
                        <li> <p>Product Quantity: <xsl:value-of select="productQuantity/productQuantity"/></p></li>
                        <li> <p>Production Order State: <xsl:value-of select="productionOrderState"/></p></li>
                        
                    </ul>
                    <hr></hr>
                </xsl:for-each>
           
        </body>
    </html>

    </xsl:template>
    <xsl:template match="product">
        <ul>
            <li><p>Product ID: <xsl:value-of select="productionID"/></p></li>
            <li><p>Commercial Code: <xsl:value-of select="commercialID/code"/></p></li>
            <li><p>Brief Description: <xsl:value-of select="briefDescription/briefDescription"/></p></li>
            <li> <p>Description: <xsl:value-of select="productDescription/description"/></p></li>
            <li><p>Unit: <xsl:value-of select="unit/unit"/></p></li>
            <li> <p>Product Category: <xsl:value-of select="productCategory/productCategory"/></p></li>
            <li><p>
            Production Form: <xsl:choose>
                <xsl:when test="productionForm !=''">
                    <xsl:value-of select="productionForm/productionFormId"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>no production form</xsl:text>
                </xsl:otherwise>
            </xsl:choose> </p></li>
    </ul>

    </xsl:template>
    <xsl:template match="/FactoryFloor/RawMaterials"/>
    <xsl:template match="/FactoryFloor/Materials"/>
    <xsl:template match="/FactoryFloor/Products"/>
    <xsl:template match="/FactoryFloor/Machines"/>
    <xsl:template match="/FactoryFloor/Deposits"/>
    <xsl:template match="/FactoryFloor/ProductionLines"/>
    <xsl:template match="/FactoryFloor/Parcels"/>




</xsl:stylesheet>