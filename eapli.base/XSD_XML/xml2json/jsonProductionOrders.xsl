<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    <xsl:param name="pPat">"</xsl:param>
    <xsl:param name="rPat">pol</xsl:param>

    <xsl:template match="/FactoryFloor/Products"/>
    

    <xsl:template match="/FactoryFloor/RawMaterials"/>
    <xsl:template match="/FactoryFloor/Materials"/>
    <xsl:template match="/FactoryFloor/ProductionOrders">{
        "<xsl:value-of select="name()"/>" : { "ProductionOrder" : [<xsl:apply-templates select="ProductionOrder"/>]}}
    </xsl:template>
    <xsl:template match="/FactoryFloor/Machines"/>
    <xsl:template match="/FactoryFloor/Deposits"/>
    <xsl:template match="/FactoryFloor/ProductionLines"/>
    <xsl:template match="/FactoryFloor/Parcels"/>

    <xsl:template match="ProductionOrder">
        <xsl:text>{"productionOrderID" : "</xsl:text><xsl:value-of select="productionOrderID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"emissionDate" : "</xsl:text><xsl:value-of select="emissionDate/emissionDate"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"executionPredictionDate" : "</xsl:text><xsl:value-of select="executionDatePrediction/executionDatePrediction"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"product" : {</xsl:text><xsl:apply-templates select="product"/>},<xsl:text>&#09;</xsl:text>
        <xsl:text>"unit" : "</xsl:text><xsl:value-of select="unit/unit"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productquantity" : "</xsl:text><xsl:value-of select="productQuantity/productQuantity"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"state" : "</xsl:text><xsl:value-of select="productionOrderState"/>"}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="emissionDate">
        <xsl:text>{"date" : "</xsl:text><xsl:value-of select="/FactoryFloor/ProductionOrders/ProductionOrder/emissionDate/emissionDate"/>"}
    </xsl:template>
    <xsl:template match="executionDatePrediction">
        <xsl:text>{"date" : "</xsl:text><xsl:value-of select="/FactoryFloor/ProductionOrders/ProductionOrder/executionDatePrediction/executionDatePrediction"/>"}
    </xsl:template>

    <xsl:template match="product">
        <xsl:text>"productionID" : "</xsl:text><xsl:value-of select="productionID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"commercialCode" : "</xsl:text><xsl:value-of select="commercialID/code"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"briefDescription" : "</xsl:text><xsl:value-of select="translate(briefDescription/briefDescription,$pPat,$rPat)"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productDescription" : "</xsl:text><xsl:value-of select="translate(productDescription/description,$pPat,$rPat)"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"unit" : "</xsl:text><xsl:value-of select="unit/unit"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productCategory" : "</xsl:text><xsl:value-of select="productCategory/productCategory"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productionForm" : "</xsl:text><xsl:choose>
            <xsl:when test="productionForm !=''">
                <xsl:value-of select="productionForm/productionFormId"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>no production form</xsl:text>
            </xsl:otherwise>
        </xsl:choose>"
       
    </xsl:template>
</xsl:stylesheet>