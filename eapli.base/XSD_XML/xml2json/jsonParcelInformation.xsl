<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    <xsl:param name="pPat">"</xsl:param>
    <xsl:param name="rPat">pol</xsl:param>

    <xsl:template match="/FactoryFloor/Products"/>
    <xsl:template match="/FactoryFloor/RawMaterials"/>
    <xsl:template match="/FactoryFloor/Materials"/>
    <xsl:template match="/FactoryFloor/ProductionOrders"/>
    <xsl:template match="/FactoryFloor/Machines"/>
    <xsl:template match="/FactoryFloor/Deposits"/>
    <xsl:template match="/FactoryFloor/ProductionLines"/>
    <xsl:template match="/FactoryFloor/Parcels">{
        "<xsl:value-of select="name()"/>" : { "Parcel" : [<xsl:apply-templates select="Parcel"/>]}}
    </xsl:template>

    <xsl:template match="Parcel">
        <xsl:text>{"parcelID" : "</xsl:text><xsl:value-of select="parcelID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"ProductionOrders" : [</xsl:text><xsl:apply-templates select="productionOrder"/>]}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    <xsl:template match="productionOrder">
        <xsl:text>{"productionOrderID" : "</xsl:text><xsl:value-of select="productionOrderID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"emissionDate" : "</xsl:text><xsl:value-of select="emissionDate/emissionDate"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"executionPredictionDate" : "</xsl:text><xsl:value-of select="executionDatePrediction/executionDatePrediction"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"productID" : "</xsl:text><xsl:value-of select="product/productionID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"unit" : "</xsl:text><xsl:value-of select="unit/unit"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productquantity" : "</xsl:text><xsl:value-of select="productQuantity/productQuantity"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"state" : "</xsl:text><xsl:value-of select="productionOrderState"/>"}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

</xsl:stylesheet>