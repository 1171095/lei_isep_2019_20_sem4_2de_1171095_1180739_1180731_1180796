<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>
    <xsl:param name="pPat">"</xsl:param>
    <xsl:param name="rPat">pol</xsl:param>
    
    <xsl:template match="/FactoryFloor/Products">{
        "<xsl:value-of select="name()"/>" : { "Product" : [<xsl:apply-templates select="Product"/>]},
    </xsl:template>

    <xsl:template match="/FactoryFloor/RawMaterials">
        "<xsl:value-of select="name()"/>" : { "RawMaterial" : [<xsl:apply-templates select="RawMaterial"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/Materials">
        "<xsl:value-of select="name()"/>" : { "Material" : [<xsl:apply-templates select="Material"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/ProductionOrders">
        "<xsl:value-of select="name()"/>" : { "ProductionOrder" : [<xsl:apply-templates select="ProductionOrder"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/Machines">
        "<xsl:value-of select="name()"/>" : { "Machine" : [<xsl:apply-templates select="Machine"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/Deposits">
        "<xsl:value-of select="name()"/>" : { "Deposit" : [<xsl:apply-templates select="Deposit"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/ProductionLines">
        "<xsl:value-of select="name()"/>" : { "ProductionLine" : [<xsl:apply-templates select="ProductionLine"/>]},
    </xsl:template>
    <xsl:template match="/FactoryFloor/Parcels">
        "<xsl:value-of select="name()"/>" : { "Parcel" : [<xsl:apply-templates select="Parcel"/>]}}
    </xsl:template>
    

    <xsl:template match="Product">
        <xsl:text>{"productionID" : "</xsl:text><xsl:value-of select="productionID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"commercialCode" : "</xsl:text><xsl:value-of select="commercialID/code"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"briefDescription" : "</xsl:text><xsl:value-of select="translate(briefDescription/briefDescription,$pPat,$rPat)"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productDescription" : "</xsl:text><xsl:value-of select="translate(productDescription/description,$pPat,$rPat)"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"unit" : "</xsl:text><xsl:value-of select="unit/unit"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productCategory" : "</xsl:text><xsl:value-of select="productCategory/productCategory"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productionForm" : "</xsl:text><xsl:choose>
            <xsl:when test="productionForm !=''">
                <xsl:value-of select="productionForm/productionFormId"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>no production form</xsl:text>
            </xsl:otherwise>
        </xsl:choose>"}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="RawMaterial">


        <xsl:text>{"rawMaterialID" : "</xsl:text><xsl:value-of select="rawMaterialID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"rawMaterialCategory" : "</xsl:text><xsl:value-of select="rawMaterialCategory/rawMaterialCategoryID"/>"}
  
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="Material">


        <xsl:text>{"materialID" : "</xsl:text><xsl:value-of select="materialID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"description" : "</xsl:text><xsl:value-of select="description/description"/>"}
  
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="ProductionOrder">
        <xsl:text>{"productionOrderID" : "</xsl:text><xsl:value-of select="productionOrderID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"emissionDate" : "</xsl:text><xsl:value-of select="emissionDate/emissionDate"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"executionPredictionDate" : "</xsl:text><xsl:value-of select="executionDatePrediction/executionDatePrediction"/>",<xsl:text>&#09;</xsl:text>

        <xsl:text>"productID" : "</xsl:text><xsl:value-of select="product/productionID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"unit" : "</xsl:text><xsl:value-of select="unit/unit"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"productquantity" : "</xsl:text><xsl:value-of select="productQuantity/productQuantity"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"state" : "</xsl:text><xsl:value-of select="productionOrderState"/>"}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>
    
    <xsl:template match="emissionDate">
        <xsl:text>{"date" : "</xsl:text><xsl:value-of select="/FactoryFloor/ProductionOrders/ProductionOrder/emissionDate/emissionDate"/>"}
    </xsl:template>
    <xsl:template match="executionDatePrediction">
        <xsl:text>{"date" : "</xsl:text><xsl:value-of select="/FactoryFloor/ProductionOrders/ProductionOrder/executionDatePrediction/executionDatePrediction"/>"}
    </xsl:template>
   
    
    <xsl:template match="Machine">
        <xsl:text>{"machineID" : "</xsl:text><xsl:value-of select="machineID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"internalCode" : "</xsl:text><xsl:value-of select="internalCode/internalCode"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"serialNumber" : "</xsl:text><xsl:value-of select="serialNumber/serialNumber"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"description" : "</xsl:text><xsl:value-of select="description/description"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"installationDate" : "</xsl:text><xsl:value-of select="installationDate/installationDate"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"brand" : "</xsl:text><xsl:value-of select="brand/brand"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"model" : "</xsl:text><xsl:value-of select="model/model"/>"}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="Deposit">


        <xsl:text>{"depositID" : "</xsl:text><xsl:value-of select="depositID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"description" : "</xsl:text><xsl:value-of select="description/description"/>"}
  
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="ProductionLine">
        <xsl:text>{"productionLineID" : "</xsl:text><xsl:value-of select="productionLineID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"Machines" : [</xsl:text><xsl:apply-templates select="machineMap"/>]}
  
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

    <xsl:template match="machineMap">
        <xsl:for-each select="entry"> 
            <xsl:text>{"position" : "</xsl:text><xsl:value-of select="key"/>",<xsl:text>&#09;</xsl:text>
            <xsl:text>"machineID" : "</xsl:text><xsl:value-of select="value/machineID"/>"}
            
            <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:for-each>
    <xsl:if test="following-sibling::*">,</xsl:if>
    
        
    </xsl:template>

    <xsl:template match="Parcel">
        <xsl:text>{"parcelID" : "</xsl:text><xsl:value-of select="parcelID"/>",<xsl:text>&#09;</xsl:text>
        <xsl:text>"ProductionOrders" : [</xsl:text><xsl:for-each select="productionOrder">
            <xsl:text>{"productionOrderID ": "</xsl:text><xsl:value-of select="productionOrderID"/>"}
            <xsl:if test="following-sibling::*">,</xsl:if>

        </xsl:for-each>]}
        <xsl:if test="following-sibling::*">,</xsl:if>
    </xsl:template>

   

</xsl:stylesheet>