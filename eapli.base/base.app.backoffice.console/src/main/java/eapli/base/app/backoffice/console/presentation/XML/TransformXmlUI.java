package eapli.base.app.backoffice.console.presentation.XML;

import eapli.base.xmlmanagement.TransformXmlController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TransformXmlUI extends AbstractUI {
    @Override
    protected boolean doShow() {
        TransformXmlController controller = new TransformXmlController();
        List<String> options = new ArrayList<>();
        options.add("html");
        options.add("txt");
        options.add("json");
        options.add("report");
        SelectWidget<String> stringSelectWidget = new SelectWidget<>("Transformation Options", options);
        String selectedOption;
        stringSelectWidget.show();
        switch (stringSelectWidget.selectedOption()) {
            case 1:
                selectedOption = htmlOptions();
                controller.transform(selectedOption);
                break;
            case 2:
                selectedOption = txtOptions();
                controller.transform(selectedOption);
                break;
            case 3:
                selectedOption = jsonOptions();
                controller.transform(selectedOption);
                break;
            case 4:
                controller.transform("relat");
                break;
            case 0:
                break;


        }

        return false;
    }
    private String jsonOptions() {
        List<String> jOptions = new ArrayList<>();
        jOptions.add("total");
        jOptions.add("Parcel Information");
        jOptions.add("Production Orders");
        SelectWidget<String> jsonSelectWidget = new SelectWidget<>("Transformation Options",jOptions);
        String outString = "";
        jsonSelectWidget.show();
        switch (jsonSelectWidget.selectedOption()){
            case 1:
                outString= "json_total";
                break;
            case 2:
                outString= "json_ParcelInformation";
                break;
            case 3:
                outString= "json_ProductionOrders";
                break;
            case 0:
                break;

        }
        return outString;



    }

    private String txtOptions() {
        List<String> txtOptions = new ArrayList<>();
        txtOptions.add("total");
        txtOptions.add("Parcel Information");
        txtOptions.add("Production Orders");
        SelectWidget<String> jsonSelectWidget = new SelectWidget<>("Transformation Options",txtOptions);
        String outString = "";
        jsonSelectWidget.show();
        switch (jsonSelectWidget.selectedOption()){
            case 1:
                outString= "txt_total";
                break;
            case 2:
                outString= "txt_ParcelInformation";
                break;
            case 3:
                outString= "txt_ProductionOrders";
                break;
            case 0:
                break;

        }
        return outString;
    }

    private String htmlOptions() {
        List<String> htmlOptions = new ArrayList<>();
        htmlOptions.add("total");
        htmlOptions.add("Parcel Information");
        htmlOptions.add("Production Orders");
        SelectWidget<String> jsonSelectWidget = new SelectWidget<>("Transformation Options",htmlOptions);
        String outString = "";
        jsonSelectWidget.show();
        switch (jsonSelectWidget.selectedOption()){
            case 1:
                outString= "html_total";
                break;
            case 2:
                outString= "html_ParcelInformation";
                break;
            case 3:
                outString= "html_ProductionOrders";
                break;
            case 0:
                break;

        }
        return outString;
    }

    @Override
    public String headline() {
        return "Transform XML";
    }
}
