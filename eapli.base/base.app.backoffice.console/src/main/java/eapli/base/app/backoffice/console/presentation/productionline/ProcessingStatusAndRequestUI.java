package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.productionlinemanagement.application.ProcessingStatusAndRequestController;
import eapli.base.productionlinemanagement.application.SpecifyNewProductionLineController;
import eapli.base.productionlinemanagement.domain.ProcessingStatus;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ProcessingStatusAndRequestUI extends AbstractUI {

    ProcessingStatusAndRequestController psarc = new ProcessingStatusAndRequestController();


    @Override
    protected boolean doShow() {
        SelectWidget<ProductionLine> selectProdLine = new SelectWidget<>("Select A Production Line",psarc.getAllProductionLines());
        selectProdLine.show();
        if(selectProdLine.selectedOption() != 0){
            System.out.println("1 - Check Status Of Recurrent Processing\n" +
                    "2 - Request a Processing\n" +
                    "0 - Exit\n");
            int option = Console.readInteger("What do you wish to do?");
            if(option != 1 && option != 2){
                return false;
            }else if(option == 1){
                System.out.println("This Production Line has the Recurrent Processing "
                        + selectProdLine.selectedElement().getProcessingStatus() + "\n");
                boolean check = Console.readBoolean("Do you want to change it? (y/n)");
                if(check){
                    if(selectProdLine.selectedElement().getProcessingStatus() == ProcessingStatus.ACTIVE){
                        selectProdLine.selectedElement().setProcessingStatus(ProcessingStatus.INACTIVE);
                        psarc.save(selectProdLine.selectedElement());

                    }else{
                        selectProdLine.selectedElement().setProcessingStatus(ProcessingStatus.ACTIVE);
                        psarc.save(selectProdLine.selectedElement());
                    }
                }
            }else{
                if(selectProdLine.selectedElement().getProcessingRequest().HasRequest()){
                    System.out.println("There is already a processing request");
                    return false;
                }else{
                    int initialYear = Console.readInteger("Initial Year:");
                    int initialMonth = Console.readInteger("Initial Month:");
                    int initialDay = Console.readInteger("Initial Day:");
                    int initialHour = Console.readInteger("Initial Hour:");
                    int initialMinute = Console.readInteger("Initial Minute:");
                    Calendar initialDate = new GregorianCalendar(initialYear,initialMonth,initialDay,initialHour,initialMinute);
                    int finalYear = Console.readInteger("Final Year:");
                    int finalMonth = Console.readInteger("Final Month:");
                    int finalDay = Console.readInteger("Final Day:");
                    int finalHour = Console.readInteger("Final Hour:");
                    int finalMinute = Console.readInteger("Final Minute:");
                    Calendar finalDate = new GregorianCalendar(finalYear, finalMonth, finalDay,finalHour,finalMinute);
                    selectProdLine.selectedElement().getProcessingRequest().setInitial_date(initialDate);
                    selectProdLine.selectedElement().getProcessingRequest().setFinal_date(finalDate);
                    selectProdLine.selectedElement().getProcessingRequest().setHasRequest(true);
                    psarc.save(selectProdLine.selectedElement());
                }
            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Processing Status Of Production Line";
    }
}
