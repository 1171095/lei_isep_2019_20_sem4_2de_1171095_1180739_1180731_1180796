package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterialmanagement.application.ListRawMaterialDTOService;
import eapli.base.rawmaterialmanagement.application.RawMaterialCategoryManagementService;
import eapli.base.rawmaterialmanagement.application.RawMaterialManagementService;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListRawMaterialUI extends AbstractListUI<RawMaterialDTO> {
    private RawMaterialManagementService list = new RawMaterialManagementService();
    private ListRawMaterialDTOService lrmds = new ListRawMaterialDTOService();

    @Override
    public String headline() {
        return "List Users";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<RawMaterialDTO> elements() {
        return lrmds.rawMaterialDTO(list.listRawMaterial());
    }

    @Override
    protected Visitor<RawMaterialDTO> elementPrinter() {
        return new RawMaterialPrinter();
    }

    @Override
    protected String elementName() {
        return "Raw Material Category";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s", "Raw Material Category ID", "Description");
    }
}
