package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinemanagement.application.RequestConfigurationToMachineController;
import eapli.base.machinemanagement.domain.Configuration;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.util.Set;

public class RequestConfigurationToMachineUI extends AbstractUI {

    private RequestConfigurationToMachineController controller = new RequestConfigurationToMachineController();

    @Override
    protected boolean doShow() {
        //Machine
        Iterable<MachineDTO> listMachines = controller.getMachines();
        final SelectWidget<MachineDTO> machineDTOSelectWidget = new SelectWidget<>("Machine:", listMachines, new MachinePrinter());
        machineDTOSelectWidget.show();
        if (machineDTOSelectWidget.selectedOption() != 0) {
            MachineDTO machineDTO = machineDTOSelectWidget.selectedElement();

            Set<Configuration> listConfigurations = controller.getMachineConfigurations(machineDTO.getId());

            SelectWidget<Configuration> configurationSelectWidget = new SelectWidget<>("Configuration:", listConfigurations, new ConfigurationPrinter());
            configurationSelectWidget.show();
            if (configurationSelectWidget.selectedOption() != 0) {
                Configuration configuration = configurationSelectWidget.selectedElement();

                controller.setRequestedConfiguration(configuration);

            }

        }
        return false;
    }

    @Override
    public String headline() {
        return "Request Configuration To Machine";
    }
}
