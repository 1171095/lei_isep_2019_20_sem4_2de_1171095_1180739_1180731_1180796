package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.app.backoffice.console.presentation.product.ProductPrinter;
import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.rawmaterialmanagement.application.DefiningNewRawMaterialController;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

public class DefineNewRawMaterialProductUI extends AbstractUI {

    private final DefiningNewRawMaterialController controller = new DefiningNewRawMaterialController();
    private final ProductManagementService pms= new ProductManagementService();
    private final ListProductDTOService lpds= new ListProductDTOService();

    @Override
    protected boolean doShow() {

        final Iterable<RawMaterialCategoryDTO> categories = controller.listRawMaterialCategories();
        final Iterable<ProductDTO> productDTOS = this.lpds.getProductDTOS(pms.listAll());

        final SelectWidget<RawMaterialCategoryDTO> rawMaterialCategoryDTOSelectWidget = new SelectWidget<>("Raw material categories:", categories, new RawMaterialCategoryPrinter());
        rawMaterialCategoryDTOSelectWidget.show();
        if (rawMaterialCategoryDTOSelectWidget.selectedOption() != 0) {
            final RawMaterialCategoryDTO rawDTO = rawMaterialCategoryDTOSelectWidget.selectedElement();
            this.controller.selectRawMaterialCategory(rawDTO.getId());
            final SelectWidget<ProductDTO> selector = new SelectWidget<>("Products:", productDTOS, new ProductPrinter());
            selector.show();
            final ProductDTO productDTO = selector.selectedElement();


            try {
                boolean success = this.controller.addProductRawMaterial(productDTO);
                if (success)
                {
                    System.out.println("Registered product as raw material successfully ");
                }else{
                    System.out.println("Error registering raw material successfully");
                }
            } catch (final IntegrityViolationException | ConcurrencyException e) {
                System.out.println("ERror creating raw material ");
            }
        }


        return false;
    }

    @Override
    public String headline() { return "Defining Product as Raw Material";
    }
}
