package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.framework.actions.Action;

public class ListErrorNotificationArchivedAction implements Action {
    @Override
    public boolean execute() {
        return new ListErrorNotificationArchivedUI().show();
    }
}
