package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.framework.visitor.Visitor;

public class MachinePrinter implements Visitor<MachineDTO> {

    @Override
    public void visit(MachineDTO visitee) {
        System.out.printf("%s-%s-%s-%s-%s-%s-%s", visitee.getId(), visitee.getInternalCode(),visitee.getSerialNumber(),
                visitee.getDescription(), visitee.getInstallationDate(),visitee.getBrand(),visitee.getModel());
    }
}
