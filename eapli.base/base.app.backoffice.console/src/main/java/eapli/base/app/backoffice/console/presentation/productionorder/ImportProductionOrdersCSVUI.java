package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productionordermanagement.application.ImportProductionOrdersCSVController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class ImportProductionOrdersCSVUI extends AbstractUI {

    private ImportProductionOrdersCSVController controller = new ImportProductionOrdersCSVController();
    @Override
    protected boolean doShow() {
        final String fileName = Console.readLine("File name:");

        try {
            if(this.controller.selectInputFile(fileName)){
                String output = this.controller.importProductionOrders();

                System.out.println(output);
            }
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("There was an error.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Import Production Orders From CSV";
    }
}
