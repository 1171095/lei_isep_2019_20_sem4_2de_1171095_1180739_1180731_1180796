package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.app.backoffice.console.presentation.deposit.ListDepositsUI;
import eapli.framework.actions.Action;

public class ListProductsAction implements Action {
    @Override
    public boolean execute() {
        return new ListProductsUI().show();
    }
}
