package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.app.backoffice.console.presentation.product.ProductPrinter;
import eapli.base.machinemanagement.application.AddConfigurationToMachineController;
import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Configuration;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.io.File;

public class AddConfigurationToMachineUI extends AbstractUI {

    AddConfigurationToMachineController addConfigurationToMachineController = new AddConfigurationToMachineController();
    MachineManagementService mms = new MachineManagementService();
    MachineDTOListService listDTO = new MachineDTOListService();

    @Override
    protected boolean doShow() {

        //Machine
        final SelectWidget<MachineDTO> machineDTOSelectWidget = new SelectWidget<>("Machine:", listDTO.dtoMachines(mms.listMachines()), new MachinePrinter());
        machineDTOSelectWidget.show();
        if (machineDTOSelectWidget.selectedOption() != 0) {
            final MachineDTO machineDTO = machineDTOSelectWidget.selectedElement();
            final Machine machine = mms.getMachineByID(machineDTO.getId()).get();

            //Description
            final String description = Console.readLine("Configuration's description:");

            //File Name
            final String fileName = Console.readLine("Configuration's file:");

            //Create and Add Configuration
            if (description != null && fileName != null) {
                File file = new File(fileName);
                if(file.exists() && !file.isDirectory()) {
                    Configuration configuration = new Configuration(description, file);
                    addConfigurationToMachineController.addConfigurationToMachine(machine, configuration);
                }else{
                    System.out.println("Error adding configuration to machine: File not found");
                }
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Add Configuration To Machine";
    }
}
