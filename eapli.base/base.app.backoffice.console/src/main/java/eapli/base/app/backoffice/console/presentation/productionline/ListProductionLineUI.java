package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.app.backoffice.console.presentation.rawmaterial.RawMaterialCategoryPrinter;
import eapli.base.productionlinemanagement.application.ListProductionLineDTOService;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.rawmaterialmanagement.application.RawMaterialCategoryManagementService;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListProductionLineUI extends AbstractListUI<ProductionLineDTO> {
    private ProductionLineManagementService list = new ProductionLineManagementService();
    private ListProductionLineDTOService listDTO = new ListProductionLineDTOService();

    @Override
    public String headline() {
        return "List Production Lines";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<ProductionLineDTO> elements() {
        return listDTO.getProductionLineDTOS(list.listProductionLines());
    }

    @Override
    protected Visitor<ProductionLineDTO> elementPrinter() {
        return new ProductionLinePrinter();
    }

    @Override
    protected String elementName() {
        return "Production Line";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s", "List of Machines");
    }
}
