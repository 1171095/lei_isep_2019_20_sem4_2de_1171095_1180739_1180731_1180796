package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.framework.actions.Action;

public class ConsultErrorNotificationsUntreatedAction implements Action {

    @Override
    public boolean execute() { return new ConsultErrorNotificationsUntreatedUI().show(); }
}
