package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.framework.actions.Action;

public class IntroduceNewProductionOrderAction implements Action {
    @Override
    public boolean execute() {
        return new IntroduceNewProductionOrderUI().show();
    }
}
