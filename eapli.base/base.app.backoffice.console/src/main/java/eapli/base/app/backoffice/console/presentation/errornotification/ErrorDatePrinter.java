package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.framework.visitor.Visitor;

public class ErrorDatePrinter implements Visitor<ErrorDate> {

    @Override
    public void visit(ErrorDate visitee) {
        System.out.printf("%s", visitee.value());
    }
}
