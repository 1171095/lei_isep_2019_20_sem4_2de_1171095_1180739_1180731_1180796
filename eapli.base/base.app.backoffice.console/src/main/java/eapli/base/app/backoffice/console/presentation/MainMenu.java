/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.app.backoffice.console.presentation;


import eapli.base.app.backoffice.console.presentation.XML.ExportToXmlAction;
import eapli.base.app.backoffice.console.presentation.XML.TransformXmlAction;
import eapli.base.app.backoffice.console.presentation.errornotification.ArchiveNotificationUI;
import eapli.base.app.backoffice.console.presentation.errornotification.ConsultErrorNotificationsArchivedAction;
import eapli.base.app.backoffice.console.presentation.errornotification.ConsultErrorNotificationsUntreatedAction;
import eapli.base.app.backoffice.console.presentation.machine.AddConfigurationToMachineAction;
import eapli.base.app.backoffice.console.presentation.machine.RequestConfigurationToMachineAction;
import eapli.base.app.backoffice.console.presentation.productionform.DefineNewProductionFormUI;
import eapli.base.app.backoffice.console.presentation.productionform.ListProductsWOutFormAction;


import eapli.base.app.backoffice.console.presentation.productionline.ListProductionLineAction;
import eapli.base.app.backoffice.console.presentation.productionline.ProcessingStatusAndRequestUI;
import eapli.base.app.backoffice.console.presentation.productionline.SpecifyNewProductionLineUI;
import eapli.base.app.backoffice.console.presentation.machine.DefineNewMachineAction;
import eapli.base.app.backoffice.console.presentation.machine.ListMachineAction;

import eapli.base.app.backoffice.console.presentation.productionorder.*;
import eapli.base.app.backoffice.console.presentation.rawmaterial.*;
import eapli.base.app.backoffice.console.presentation.deposit.ListDepositsAction;
import eapli.base.app.backoffice.console.presentation.deposit.SpecifyNewDepositAction;
import eapli.base.app.backoffice.console.presentation.product.AddProductToCatalogAction;
import eapli.base.app.backoffice.console.presentation.product.ImportCatalogFromCSVAction;
import eapli.base.app.backoffice.console.presentation.product.ListProductsAction;
import eapli.base.app.backoffice.console.presentation.rawmaterial.DefineNewRawMaterialCategoryUI;
import eapli.base.app.backoffice.console.presentation.rawmaterial.ListRawMaterialCategoryAction;
import eapli.base.app.common.console.presentation.authz.MyUserMenu;
import eapli.base.Application;
import eapli.base.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.base.app.backoffice.console.presentation.authz.DeactivateUserAction;
import eapli.base.app.backoffice.console.presentation.authz.ListUsersAction;
import eapli.base.app.backoffice.console.presentation.clientuser.AcceptRefuseSignupRequestAction;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // SETTINGS
    private static final int SET_KITCHEN_ALERT_LIMIT_OPTION = 1;

    // Raw Material Category
    private static final int RAW_MATERIAL_CATEGORY_REGISTER_OPTION = 1;
    private static final int RAW_MATERIAL_CATEGORY_LIST_OPTION = 2;
    private static final int RAW_MATERIAL_CATEGORY_CHANGE_OPTION = 3;
    private static final int RAW_MATERIAL_CATEGORY_ACTIVATE_DEACTIVATE_OPTION = 4;

    // Deposit
    private static final int DEPOSIT_REGISTER_OPTION = 1;
    private static final int DEPOSIT_LIST_OPTION = 2;

    // Product
    private static final int PRODUCT_REGISTER_OPTION = 1;
    private static final int PRODUCT_LIST_OPTION = 2;
    private static final int PRODUCT_IMPORT_CSV = 3;

    //Production Form
    private static final int PRODUCTION_FORM_LIST_PRODUCTS_WITHOUT_FORM = 1;
    private static final int PRODUCTION_FORM_REGISTER_OPTION  =2 ;

    // RAW MATERIALS
    private static final int RAW_MATERIAL_REGISTER_OPTION = 3;
    private static final int RAW_MATERIAL_PRODUCT_REGISTER_OPTION = 4;
    private static final int RAW_MATERIAL_LIST_OPTION = 5;

    // PRODUCTION LINE
    private static final int PRODUCTION_LINE_REGISTER_OPTION = 1;
    private static final int LIST_PRODUCTION_LINE_OPTION = 2;
    private static final int PROCESSING_STATUS_AND_REQUEST = 3;

    // MACHINE
    private static final int MACHINE_REGISTER_OPTION = 1;
    private static final int MACHINE_LIST_OPTION = 2;
    private static final int MACHINE_ADD_CONFIGURATION = 3;
    private static final int MACHINE_ACTIVATE_DEACTIVATE_OPTION = 4;
    private static final int MACHINE_REQUEST_CONFIGURATION = 5;

    //PRODUCTION ORDER
    private static final int PRODUCTION_ORDER_IMPORT_OPTION = 1;
    private static final int PRODUCTION_ORDER_ADD_OPTION = 2;
    private static final int PRODUCTION_ORDER_LIST_OPTION = 3;
    private static final int PRODUCTION_ORDER_OF_AN_ORDER = 4;
    private static final int PRODUCTION_ORDER_CONSULT_STATE_OPTION = 5;

    //ERROR NOTIFICATION
    private static final int ERROR_NOTIFICATION_CONSULT_UNTREARED_OPTION = 1;
    private static final int ERROR_NOTIFICATION_CONSULT_ARCHIVED_OPTION = 2;
    private static final int ERROR_NOTIFICATION_ARCHIVE_NOTIFICATION_OPTION = 3;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int SETTINGS_OPTION = 3;
    private static final int RAW_MATERIAL_OPTION = 4;
    private static final int MACHINE_OPTION = 5;
    private static final int DEPOSIT_OPTION = 6;
    private static final int PRODUCT_OPTION = 7;
    private static final int PRODUCTION_FORM_OPTION = 8;
    private static final int PRODUCTION_LINE_OPTION = 9;
    private static final int PRODUCTION_ORDER_OPTION = 10;
    private static final int ERROR_NOTIFICATION_OPTION =11;

    //xml
    private static final int EXPORT_OPTION=12;
    private static final int TRANSFORM_OPTION=13;


    private static final String SEPARATOR_LABEL = "--------------";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return renderer.render();
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "Base [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("Base [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.addSubMenu(USERS_OPTION, usersMenu);
            final Menu settingsMenu = buildAdminSettingsMenu();
            mainMenu.addSubMenu(SETTINGS_OPTION, settingsMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER)){
            final Menu rawMaterialCategoryMenu = buildRawMaterialMenu();
            mainMenu.addSubMenu(RAW_MATERIAL_OPTION, rawMaterialCategoryMenu);
        }
        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER)){
            final Menu machineMenu = buildMachineMenu();
            mainMenu.addSubMenu(MACHINE_OPTION, machineMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER)){
            final Menu depositMenu = buildDepositMenu();
            mainMenu.addSubMenu(DEPOSIT_OPTION, depositMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER)){
            final Menu productMenu = buildProductMenu();
            mainMenu.addSubMenu(PRODUCT_OPTION, productMenu);
        }
        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER)){
            final Menu productionMenu = buildProductionFormMenu();
            mainMenu.addSubMenu(PRODUCTION_FORM_OPTION, productionMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER)){
            final Menu productionLineMenu = buildProductionLineMenu();
            mainMenu.addSubMenu(PRODUCTION_LINE_OPTION,productionLineMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.PRODUCTION_MANAGER)){
            final Menu productionOrderMenu = buildProductionOrderMenu();
            mainMenu.addSubMenu(PRODUCTION_ORDER_OPTION,productionOrderMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER)){
            final Menu errorNotificationMenu = buildErrorNotificationMenu();
            mainMenu.addSubMenu(ERROR_NOTIFICATION_OPTION,errorNotificationMenu);
        }

        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.PRODUCTION_MANAGER, BaseRoles.FACTORY_FLOOR_MANAGER)) {
            final Menu export = buildExportToXml();
            mainMenu.addSubMenu(EXPORT_OPTION, export);
        }
        if(authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.FACTORY_FLOOR_MANAGER, BaseRoles.PRODUCTION_MANAGER, BaseRoles.FACTORY_FLOOR_MANAGER)) {
          final Menu transform = buildTransformXML();
            mainMenu.addSubMenu(TRANSFORM_OPTION, transform);

        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Bye, Bye"));

        return mainMenu;
    }



    private Menu buildAdminSettingsMenu() {
        final Menu menu = new Menu("Settings >");

        menu.addItem(SET_KITCHEN_ALERT_LIMIT_OPTION, "Set kitchen alert limit",
                new ShowMessageAction("Not implemented yet"));
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.addItem(ADD_USER_OPTION, "Add User", new AddUserUI()::show);
        menu.addItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction());
        menu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction());
        menu.addItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildRawMaterialMenu(){
        final Menu menu = new Menu("Raw Material >");

        menu.addItem(RAW_MATERIAL_CATEGORY_REGISTER_OPTION, "Define Raw Material Category",new DefineNewRawMaterialCategoryUI()::show);
        menu.addItem(RAW_MATERIAL_CATEGORY_LIST_OPTION, "List all Raw Material Categories", new ListRawMaterialCategoryAction());

        // raw materials
        menu.addItem(RAW_MATERIAL_REGISTER_OPTION, "Define new Raw Material", new DefineNewRawMaterialAction());
        menu.addItem(RAW_MATERIAL_PRODUCT_REGISTER_OPTION, "Define Raw Material by Product", new DefineNewRawMaterialProductAction());
        menu.addItem(RAW_MATERIAL_LIST_OPTION, "List all Raw Materials", new ListRawMaterialAction());


        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildDepositMenu() {
        final Menu menu = new Menu("Deposit >");

        menu.addItem(DEPOSIT_REGISTER_OPTION, "Specify new deposit",new SpecifyNewDepositAction());
        menu.addItem(DEPOSIT_LIST_OPTION, "List all deposits", new ListDepositsAction());

        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildMachineMenu(){
        final Menu menu = new Menu("Machine >");

        menu.addItem(MACHINE_REGISTER_OPTION, "Define new Machine", new DefineNewMachineAction());
        menu.addItem(MACHINE_LIST_OPTION, "List all Machines", new ListMachineAction());
        menu.addItem(MACHINE_ADD_CONFIGURATION, "Add configuration to machine", new AddConfigurationToMachineAction());
        menu.addItem(MACHINE_REQUEST_CONFIGURATION, "Request Configuration to machine", new RequestConfigurationToMachineAction());

        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }


    private Menu buildProductionLineMenu (){
        final Menu menu = new Menu ("Production Line >");

        menu.addItem(PRODUCTION_LINE_REGISTER_OPTION, "Specify new Production Line", new SpecifyNewProductionLineUI()::show);
        menu.addItem(LIST_PRODUCTION_LINE_OPTION, "List All Production Lines",new ListProductionLineAction());
        menu.addItem(PROCESSING_STATUS_AND_REQUEST, "Message Processing", new ProcessingStatusAndRequestUI()::show);



        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionFormMenu() {
        final Menu menu = new Menu("Production Form >");

        menu.addItem(PRODUCTION_FORM_LIST_PRODUCTS_WITHOUT_FORM, "List all products without production form", new ListProductsWOutFormAction());
        menu.addItem(PRODUCTION_FORM_REGISTER_OPTION, "Register new product production form", new DefineNewProductionFormUI()::show);

        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }


    private Menu buildProductMenu() {
        final Menu menu = new Menu("Product >");

        menu.addItem(PRODUCT_REGISTER_OPTION, "Add new product to catalog",new AddProductToCatalogAction());
        menu.addItem(PRODUCT_LIST_OPTION, "List all products", new ListProductsAction());
        menu.addItem(PRODUCT_IMPORT_CSV, "Import catalog from CSV", new ImportCatalogFromCSVAction());

        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProductionOrderMenu(){
        final Menu menu = new Menu("Production Order >");

        menu.addItem(PRODUCTION_ORDER_IMPORT_OPTION, "Import production orders from CSV", new ImportProductionOrdersCSVAction());
        menu.addItem(PRODUCTION_ORDER_ADD_OPTION, "Add production order", new IntroduceNewProductionOrderAction());
        menu.addItem(PRODUCTION_ORDER_LIST_OPTION, "List all production orders", new ListProductionOrdesAction());
        menu.addItem(PRODUCTION_ORDER_OF_AN_ORDER, "Consult All Production Orders Of a Selected Order", new ConsultProductionOrderOfAnOrderUI()::show);
        menu.addItem(PRODUCTION_ORDER_CONSULT_STATE_OPTION,"Consult production orders in a given state", new ConsultProductionOrdersByStateAction());

        menu.addItem(EXIT_OPTION,RETURN_LABEL,Actions.SUCCESS);

        return menu;
    }

    private Menu buildErrorNotificationMenu(){
        final Menu menu = new Menu("Error Notification >");

        menu.addItem(ERROR_NOTIFICATION_CONSULT_UNTREARED_OPTION, "Consult untreated error notifications", new ConsultErrorNotificationsUntreatedAction());
        menu.addItem(ERROR_NOTIFICATION_CONSULT_ARCHIVED_OPTION, "Consult archived error notifications", new ConsultErrorNotificationsArchivedAction());
        menu.addItem(ERROR_NOTIFICATION_ARCHIVE_NOTIFICATION_OPTION, "Archive an Error Notification", new ArchiveNotificationUI()::show);

        menu.addItem(EXIT_OPTION,RETURN_LABEL,Actions.SUCCESS);
        return menu;
    }

    private Menu buildExportToXml(){
        final Menu menu = new Menu("export to xml");

        menu.addItem(1,"export",new ExportToXmlAction() );
        return menu;
    }

    private Menu buildTransformXML(){
        final Menu menu = new Menu("Transform XML");
        menu.addItem(1, "transform xml into html/txt/json", new TransformXmlAction());
        menu.addItem(EXIT_OPTION,RETURN_LABEL,Actions.SUCCESS);

        return menu;
    }

}
