package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productionordermanagement.domain.ProductionOrderDTO;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.visitor.Visitor;

public class ProductionOrderPrinter implements Visitor<ProductionOrderDTO> {
    @Override
    public void visit(ProductionOrderDTO visitee) {
        System.out.printf("# %d-%s-%s-%s-%f-%s-%s", visitee.getProductionOrderID(), visitee.getEmissionDate(), visitee.getExecutionDatePrediction(), visitee.getProduct(), visitee.getProductQuantity(), visitee.getUnit(), visitee.getProductionOrderState());
    }
}
