package eapli.base.app.backoffice.console.presentation.product;


import eapli.base.productmanagement.domain.ProductDTO;

import eapli.framework.visitor.Visitor;

public class ProductPrinter implements Visitor<ProductDTO> {
    @Override
    public void visit(ProductDTO visitee) {
        System.out.printf("%s - %s - %s - %s - %s - %s", visitee.getId(), visitee.getCommercialCode(), visitee.getBrief(), visitee.getDesc(), visitee.getUnit(), visitee.getCat());
    }
}
