package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.app.backoffice.console.presentation.rawmaterial.ListRawMaterialCategoryUI;
import eapli.framework.actions.Action;

public class ListProductionLineAction implements Action {
    @Override
    public boolean execute() {
        return new ListProductionLineUI().show();
    }
}
