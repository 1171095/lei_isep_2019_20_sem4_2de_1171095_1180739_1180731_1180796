package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinemanagement.application.DefiningNewMachineController;
import eapli.base.machinemanagement.domain.*;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.application.DefineNewRawMaterialCategoryController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.sql.Date;

public class DefineNewMachineUI extends AbstractUI {
    private final DefiningNewMachineController dnmc = new DefiningNewMachineController();
    private final ProductionLineManagementService productionLineManagementService = new ProductionLineManagementService();

    @Override
    protected boolean doShow (){
        final InternalCode internalCode = new InternalCode(Console.readLine("Internal Code"));
        final SerialNumber serialNumber = new SerialNumber(Console.readLine("Serial Number"));
        final Description description = new Description(Console.readLine("Description"));
        System.out.println("Installation Date");
        final int year = Console.readInteger("Year");
        final int month = Console.readInteger("Month");
        final int day = Console.readInteger("Day");
        final Date date = new Date(year, month, day);
        final InstallationDate installationDate = new InstallationDate(date);
        final Brand brand = new Brand(Console.readLine("Brand"));
        final Model model = new Model(Console.readLine("Model"));

        SelectWidget<ProductionLine> selectWidget = new SelectWidget<>("what production line does the machine go to",productionLineManagementService.listProductionLines());
        selectWidget.show();
        try {
            Machine machine = this.dnmc.definingNewMachine(serialNumber,description,installationDate,brand,model,internalCode);
            int position = Console.readInteger("Machine position in production line");
            dnmc.addMachineToProdLine(selectWidget.selectedElement(),machine,position);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            System.out.println("There was an error in the process.");
        }



        return false;
    }

    @Override
    public String headline() {
        return "Define New Machine";
    }
}
