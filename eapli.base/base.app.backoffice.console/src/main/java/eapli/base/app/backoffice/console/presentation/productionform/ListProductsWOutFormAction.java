package eapli.base.app.backoffice.console.presentation.productionform;

import eapli.framework.actions.Action;

public class ListProductsWOutFormAction implements Action {
    @Override
    public boolean execute() {
        return new ListProductsWOutFormUI().doShow();
    }
}
