package eapli.base.app.backoffice.console.presentation.productionform;


import eapli.base.productmanagement.application.ListProductsWOutFormController;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.visitor.Visitor;

public class ListProductsWOutFormUI extends AbstractUI {

    private ListProductsWOutFormController controller = new ListProductsWOutFormController();

    @Override
    protected boolean doShow() {
        final Iterable<ProductDTO> listaProdutosDTO = this.controller.getLstProductsWOutForm();

        System.out.println("Produtos:");
        for(ProductDTO prod: listaProdutosDTO) {
            System.out.println("    " + prod.toString());
        }

        return false;
    }

    @Override
    public String headline() {
        return null;
    }
}
