package eapli.base.app.backoffice.console.presentation.machine;

import eapli.framework.actions.Action;

public class RequestConfigurationToMachineAction implements Action {
    @Override
    public boolean execute() {
        return new RequestConfigurationToMachineUI().show();
    }
}
