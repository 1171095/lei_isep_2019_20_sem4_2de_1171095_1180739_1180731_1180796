package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.framework.actions.Action;

public class ImportProductionOrdersCSVAction implements Action {
    @Override
    public boolean execute() {
        return new ImportProductionOrdersCSVUI().show();
    }
}
