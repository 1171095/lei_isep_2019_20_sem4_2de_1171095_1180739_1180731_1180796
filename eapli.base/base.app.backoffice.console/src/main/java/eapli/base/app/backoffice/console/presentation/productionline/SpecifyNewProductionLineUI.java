package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.app.backoffice.console.presentation.machine.MachinePrinter;
import eapli.base.depositmanagement.application.DepositManagementService;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.productionlinemanagement.application.ProductionLineController;
import eapli.base.productionlinemanagement.application.SpecifyNewProductionLineController;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;


import java.util.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class SpecifyNewProductionLineUI extends AbstractUI {
    SpecifyNewProductionLineController snplc = new SpecifyNewProductionLineController();
    MachineManagementService mms = new MachineManagementService();


    DepositManagementService dms = new DepositManagementService();

    @Override
    protected boolean doShow() {
        SelectWidget<Machine> machineSelectWidget = new SelectWidget<>("Machines for production line", mms.listMachines());
        final Map<Integer,Machine> lstMachine = new HashMap<>();
        int position = 0;
        do{
            machineSelectWidget.show();
            if (machineSelectWidget.selectedOption()==0)
                break;
            Machine machine = machineSelectWidget.selectedElement();
            position = Console.readInteger("position for machine to go (whole numbers)");
            lstMachine.put(position,machine);
            lstMachine.toString();



        }while (true);
//        boolean add = true;
//        while(add){
//            System.out.println(mms.listMachines());
//            System.out.println("Escolha uma máquina para adicionar à linha de produção(id)");
//            Long id = Console.readLong("MachineID");
//            int posistion = Console.readInteger("in what position");
//            lstMachine.add(posistion,mms.getMachineByID(id).get());
//            System.out.println("Quer adicionar mais máquinas?");
//            add = Console.readBoolean("Y/N");
//        }
//        add =true;
//        while (add){
//            System.out.println(dms.listDeposits());
//            System.out.println("Escolha um depósito para adicionar à linha de produção(id)");
//            Long id = Console.readLong("DepositID");
//            lstDeposit.add(dms.getDepositByID(id).get());
//            System.out.println("Quer adicionar mais depósitos?");
//            add = Console.readBoolean("Y/N");
//        }

        try {
            this.snplc.registerProductionLine(lstMachine);

        } catch (final IntegrityViolationException | ConcurrencyException e) {
            System.out.println("there was an error in the process");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Specify new Production Line";
    }
}
