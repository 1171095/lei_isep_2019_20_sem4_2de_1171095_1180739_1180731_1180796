package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.framework.actions.Action;

public class ConsultErrorNotificationsArchivedAction implements Action {

    @Override
    public boolean execute() { return new ConsultErrorNotificationsArchivedUI().show(); }
}
