package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.errornotificationmanagement.application.ErrorNotificationManagementService;
import eapli.base.errornotificationmanagement.application.ListErrorNotificationDTOService;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListErrorNotificationArchivedUI extends AbstractListUI<ErrorNotificationDTO> {

    private ErrorNotificationManagementService list = new ErrorNotificationManagementService();
    private ListErrorNotificationDTOService lens = new ListErrorNotificationDTOService();

    @Override
    public String headline() {
        return "List Machines";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<ErrorNotificationDTO> elements() {
        return lens.getErrorNotificationArchivedDTOS(list.listErrorNotificationsArchived());
    }

    @Override
    protected Visitor<ErrorNotificationDTO> elementPrinter() {
        return new ErrorNotificationPrinter();
    }

    @Override
    protected String elementName() {
        return "Error Notification Archived";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s-%s-%s-%s", "Error Notification ID", "Error Type",
                "Error Date", "Production Line", "Machine");
    }
}
