package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.app.backoffice.console.presentation.authz.SystemUserPrinter;
import eapli.base.rawmaterialmanagement.application.ListCategoriesDTOService;
import eapli.base.rawmaterialmanagement.application.RawMaterialCategoryManagementService;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.base.usermanagement.application.ListUsersController;
import eapli.framework.infrastructure.authz.domain.model.SystemUser;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListRawMaterialCategoryUI extends AbstractListUI<RawMaterialCategoryDTO> {
    private RawMaterialCategoryManagementService list = new RawMaterialCategoryManagementService();
    private ListCategoriesDTOService lcds = new ListCategoriesDTOService();

    @Override
    public String headline() {
        return "List Users";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<RawMaterialCategoryDTO> elements() {
        return lcds.listRawMaterialCategoriesDTO(list.listRawMaterialCategories());
    }

    @Override
    protected Visitor<RawMaterialCategoryDTO> elementPrinter() {
        return new RawMaterialCategoryPrinter();
    }

    @Override
    protected String elementName() {
        return "Raw Material Category";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s", "Raw Material Category ID", "Description");
    }
}
