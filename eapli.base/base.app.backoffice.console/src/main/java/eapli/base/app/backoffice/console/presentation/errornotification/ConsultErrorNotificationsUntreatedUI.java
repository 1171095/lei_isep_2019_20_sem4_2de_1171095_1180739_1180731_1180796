package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.app.backoffice.console.presentation.machine.MachinePrinter;
import eapli.base.app.backoffice.console.presentation.productionline.ProductionLinePrinter;
import eapli.base.errornotificationmanagement.application.ConsultErrorNotificationsUntreatedController;
import eapli.base.errornotificationmanagement.application.ListErrorNotificationDTOService;
import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.productionlinemanagement.application.ListProductionLineDTOService;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class ConsultErrorNotificationsUntreatedUI extends AbstractUI {

    ConsultErrorNotificationsUntreatedController cenuc = new ConsultErrorNotificationsUntreatedController();
    private final ProductionLineManagementService plms= new ProductionLineManagementService();
    private final ListProductionLineDTOService lplds = new ListProductionLineDTOService();
    private final MachineManagementService mms= new MachineManagementService();
    private final MachineDTOListService lmds = new MachineDTOListService();
    private final ListErrorNotificationDTOService lends = new ListErrorNotificationDTOService();

    @Override
    protected boolean doShow() {

        Iterable<ErrorNotificationUntreated> finalErrorNotiList= new ArrayList<>();


            System.out.println("Choose a filter:" +
                    "\n 1 - All error notifications;" +
                    "\n 2 - By error type;" +
                    "\n 3 - By error date;" +
                    "\n 4 - By production line;" +
                    "\n 5 - By machine;" +
                    "\n Any other character - Exit.");

            final String filter = Console.readLine("Filter");

            switch (filter){
                case "1":
                    //mostrar todas notificaçoes ativas ou nao tratadas
                    finalErrorNotiList = cenuc.listErrorNotificationsUntreated();
                    break;
                case "2":
                    //mostrar todos os tipos de erros e deixar escolher um
                    final Iterable<ErrorType> errorTypeIterable= Arrays.asList(ErrorType.values());

                    final SelectWidget<ErrorType> errorTypeSelector = new SelectWidget<>("ErrorType:", errorTypeIterable, new ErrorTypePrinter());
                    errorTypeSelector.show();
                    if (errorTypeSelector.selectedOption() != 0) {
                        final ErrorType errorType = errorTypeSelector.selectedElement();

                        //mostrar notificaçoes com o tipo de erro escolhido
                        finalErrorNotiList = cenuc.listErrorNotificationsUntreatedByErrorType(errorType);
                    }
                    break;
                case "3":
                    //mostrar todos as datas de erros e deixar escolher um
                    final Iterable<ErrorDate> errorDateIterable = cenuc.getAllErrorDates();

                    final SelectWidget<ErrorDate> errorDateSelector = new SelectWidget<>("ErrorDate:", errorDateIterable, new ErrorDatePrinter());
                    errorDateSelector.show();
                    if (errorDateSelector.selectedOption() != 0) {
                        final ErrorDate errorDate = errorDateSelector.selectedElement();

                        //mostrar notificaçoes com a data escolhida
                        finalErrorNotiList = cenuc.listErrorNotificationsUntreatedByErrorDate(errorDate);
                    }
                    break;
                case "4":
                    //mostrar todos as linhas de produçao e deixar escolher um
                    final Iterable<ProductionLineDTO> productionLineIterable = this.lplds.getProductionLineDTOS(plms.listProductionLines());

                    final SelectWidget<ProductionLineDTO> productionLineSelector = new SelectWidget<>("ProductionLine:", productionLineIterable, new ProductionLinePrinter());
                    productionLineSelector.show();
                    if (productionLineSelector.selectedOption() != 0) {
                        final ProductionLineDTO productionLineDTO = productionLineSelector.selectedElement();

                        final Optional<ProductionLine> opl = plms.findByProductionLineID(productionLineDTO.getProdLineID());
                        final ProductionLine pd = opl.get();

                        //mostrar notificaçoes com a linha de produçao escolhida
                        finalErrorNotiList = cenuc.listErrorNotificationsUntreatedByProdLine(pd);
                    }
                    break;
                case "5":
                    //mostrar todos as maquinas e deixar escolher um
                    final Iterable<MachineDTO> machineDTOIterable = this.lmds.dtoMachines(mms.listMachines());

                    final SelectWidget<MachineDTO> machineSelector = new SelectWidget<>("Machine:", machineDTOIterable, new MachinePrinter());
                    machineSelector.show();
                    if (machineSelector.selectedOption() != 0) {
                        final MachineDTO machineDTO = machineSelector.selectedElement();

                        final Optional<Machine> om = mms.getMachineByID(machineDTO.getId());
                        final Machine m = om.get();

                        //mostrar notificaçoes com a maquina escolhida
                        finalErrorNotiList = cenuc.listErrorNotificationsUntreatedByMachine(m);
                    }
                    break;
                default: break;
            }

            Iterable<ErrorNotificationDTO> listDTO = lends.getErrorNotificationUntreatedDTOS(finalErrorNotiList);

            ListWidget<ErrorNotificationDTO> listWidget = new ListWidget<>("Error Notification Untreated", listDTO, new ErrorNotificationPrinter());
            listWidget.show();

        return false;
    }

    @Override
    public String headline() {
        return "Consult untreated error notifications";
    }
}
