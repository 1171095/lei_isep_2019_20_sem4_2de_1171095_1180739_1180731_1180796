package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.framework.visitor.Visitor;

public class ErrorNotificationPrinter implements Visitor<ErrorNotificationDTO> {

    @Override
    public void visit(ErrorNotificationDTO visitee) {
        System.out.printf("%s-%s-%s-%s-%s", visitee.getErrorNotificationID(), visitee.getErrorType(),visitee.getErrorDate(),
                visitee.getProdLine(), visitee.getMachine());
    }
}
