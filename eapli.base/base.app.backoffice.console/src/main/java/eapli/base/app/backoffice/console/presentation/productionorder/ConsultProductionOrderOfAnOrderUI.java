package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productionordermanagement.application.ConsultProductionOrderOfAnOrderController;
import eapli.base.productionordermanagement.domain.ProductionOrderDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

public class ConsultProductionOrderOfAnOrderUI extends AbstractUI {
    ConsultProductionOrderOfAnOrderController cpooaoc = new ConsultProductionOrderOfAnOrderController();



    @Override
    protected boolean doShow() {

        SelectWidget<String> ordersSelectWidget = new SelectWidget<>("Select Order ID",cpooaoc.getAllOrders() );

        do{
            ordersSelectWidget.show();
            if(ordersSelectWidget.selectedOption()==0)
                break;
            final String o = ordersSelectWidget.selectedElement();

            final Iterable<ProductionOrderDTO> allProdOrdersOfAnOrder = cpooaoc.getAllProductionOrdersOfAnOrder(o);
            for(ProductionOrderDTO pod : allProdOrdersOfAnOrder){
                System.out.println(pod.toString());
            }



        }while(true);
        return false;

    }

    @Override
    public String headline() {
        return "Consult All The Poduction Orders of an Order";
    }
}
