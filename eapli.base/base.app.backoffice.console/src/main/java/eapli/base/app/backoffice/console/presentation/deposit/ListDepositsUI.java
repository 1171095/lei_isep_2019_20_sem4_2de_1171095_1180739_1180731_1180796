package eapli.base.app.backoffice.console.presentation.deposit;


import eapli.base.depositmanagement.application.DepositManagementService;
import eapli.base.depositmanagement.application.ListDepositsDTOService;
import eapli.base.depositmanagement.domain.DepositDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListDepositsUI extends AbstractListUI<DepositDTO> {

    private ListDepositsDTOService listDTO = new ListDepositsDTOService();
    private DepositManagementService list = new DepositManagementService();

    @Override
    protected Iterable<DepositDTO> elements() {
        return listDTO.getDepositDTOS(list.listDeposits());
    }

    @Override
    protected Visitor<DepositDTO> elementPrinter() {
        return new DepositPrinter();
    }

    @Override
    protected String elementName() {
        return "Deposit";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s", "Deposit ID", "Description");
    }

    @Override
    protected String emptyMessage() {
        return "No Data.";
    }

    @Override
    public String headline() {
        return "List Deposits";
    }
}
