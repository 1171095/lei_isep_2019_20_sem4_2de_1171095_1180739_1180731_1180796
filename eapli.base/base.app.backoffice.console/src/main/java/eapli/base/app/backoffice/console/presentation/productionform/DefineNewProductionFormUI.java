package eapli.base.app.backoffice.console.presentation.productionform;

import eapli.base.app.backoffice.console.presentation.product.ProductPrinter;
import eapli.base.app.backoffice.console.presentation.rawmaterial.RawMaterialPrinter;
import eapli.base.productionformmanagement.application.SpecifyProductionFormController;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.util.Iterator;

public class DefineNewProductionFormUI extends AbstractUI {

   private final SpecifyProductionFormController controller = new SpecifyProductionFormController();


    @Override
    protected boolean doShow() {
        Iterable<ProductDTO> productsDTO = controller.lstProducts();

        SelectWidget<ProductDTO> productDTOSelectWidget = new SelectWidget<>("Products Without form:", productsDTO, new ProductPrinter());
        productDTOSelectWidget.show();
        if (productDTOSelectWidget.selectedOption() != 0){
            ProductDTO productDTO = productDTOSelectWidget.selectedElement();
            controller.product(productDTO);

            Iterable<RawMaterialDTO> rawMaterialDTOS = controller.lstRawMaterial();
            SelectWidget<RawMaterialDTO> rawMaterialDTOSelectWidget = new SelectWidget<>("Pick Raw Material used (0 to exit):", rawMaterialDTOS, new RawMaterialPrinter());

            double quantityUsed;
            boolean itemFlag = false;

            do{
                rawMaterialDTOSelectWidget.show();
                if(rawMaterialDTOSelectWidget.selectedOption()!= 0){
                    RawMaterialDTO rawMaterialDTO = rawMaterialDTOSelectWidget.selectedElement();
                    itemFlag=true; //at least one raw material picked

                    quantityUsed = Console.readDouble("Raw material quantity used:");

                    rawMaterialDTOS = controller.setRawMaterialQuantity(rawMaterialDTO,quantityUsed);
                    rawMaterialDTOSelectWidget = new SelectWidget<>("Pick Raw Material used (0 to exit):", rawMaterialDTOS, new RawMaterialPrinter());

                }
                else {
                    break;
                }
            }while(true);

            boolean saved = false;
            if(itemFlag){
//                controller.showProdForm();
                String confirm  = Console.readLine("Confirm Production form? Y/N");
                if (confirm.equalsIgnoreCase("y")){
                    try{
                        saved = controller.saveProdFile();
                    }catch(final IntegrityViolationException | ConcurrencyException ex){
                        System.out.println("error saving production form");
                    }

                }else if(confirm.equalsIgnoreCase("n")){
                    saved = false;

                }else{
                    saved = false;
                }

                if(saved){
                    System.out.println("Production file saved ");
                }else{
                    System.out.println("Production file not saved");
                }
            }else {
                System.out.println("Invalid Prodfuction form. No Raw materials selected.");
                return false;
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Production Form Definition";
    }
}
