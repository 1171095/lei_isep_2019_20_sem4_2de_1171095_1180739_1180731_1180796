package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.app.backoffice.console.presentation.product.ListProductsAction;
import eapli.base.app.backoffice.console.presentation.product.ProductPrinter;
import eapli.base.productionordermanagement.application.IntroduceNewProductionOrderController;
import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

import java.util.ArrayList;
import java.util.List;

public class IntroduceNewProductionOrderUI extends AbstractUI {

    IntroduceNewProductionOrderController introduceNewProductionOrderController = new IntroduceNewProductionOrderController();
    ProductManagementService productManagementService = new ProductManagementService();
    private ListProductDTOService listDTO = new ListProductDTOService();

    ListProductsAction listProductsAction = new ListProductsAction();

    @Override
    protected boolean doShow() {

        try {

            //ID
            final String id = Console.readLine("Production order id:");

            //Product
            final SelectWidget<ProductDTO> productDTOSelectWidget = new SelectWidget<>("Product:", listDTO.getProductDTOS(productManagementService.listAll()), new ProductPrinter());
            productDTOSelectWidget.show();
            if (productDTOSelectWidget.selectedOption() != 0) {
                final ProductDTO productDTO = productDTOSelectWidget.selectedElement();
                final Product product = productManagementService.getProductByID(productDTO.getId()).get();


                //Quantity
                final int quantity = Console.readInteger("Quantity:");

                //Unit
                final String unit = Console.readLine("Unit:");

                //Emission Date
                final String emissionDate = Console.readLine("Emission Date:");

                //Execution Date Prediction
                final String executionDatePrediction = Console.readLine("Execution Date Prediction:");

                //Orders
                List<Parcel> parcels = new ArrayList<>();
                boolean add = true;
                while (add) {
                    String orderID = Console.readLine("Order id:");
                    parcels.add(new Parcel(orderID));
                    System.out.println("Quer adicionar mais encomendas à order de produção?");
                    add = Console.readBoolean("Y/N");
                }

                introduceNewProductionOrderController.introduceNewProductionOrder(id, parcels, product, String.valueOf(quantity), unit, executionDatePrediction, emissionDate);
            }
        }catch (IllegalArgumentException e){
            System.out.println(e);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Introduce New Production Order";

    }
}
