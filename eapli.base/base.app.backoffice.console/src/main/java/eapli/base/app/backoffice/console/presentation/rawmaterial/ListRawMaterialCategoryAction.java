package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.app.backoffice.console.presentation.authz.ListUsersUI;
import eapli.framework.actions.Action;

public class ListRawMaterialCategoryAction implements Action {
    @Override
    public boolean execute() {
        return new ListRawMaterialCategoryUI().show();
    }
}
