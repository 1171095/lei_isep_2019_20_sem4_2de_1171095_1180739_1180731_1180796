package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.framework.actions.Action;

public class DefineNewRawMaterialCategoryAction implements Action {

    @Override
    public boolean execute() {
        return new DefineNewRawMaterialCategoryUI().show();
    }
}
