package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.errornotificationmanagement.application.ArchiveNotificationController;
import eapli.base.errornotificationmanagement.application.ListErrorNotificationDTOService;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.util.List;

public class ArchiveNotificationUI extends AbstractUI {

    ArchiveNotificationController anc = new ArchiveNotificationController();
    ListErrorNotificationDTOService lends = new ListErrorNotificationDTOService();

    @Override
    protected boolean doShow() {
        Iterable<ErrorNotificationUntreated> lstENU = anc.listAllUntreatedNotifications();
        Iterable<ErrorNotificationDTO> listDTO = lends.getErrorNotificationUntreatedDTOS(lstENU);
        SelectWidget<ErrorNotificationDTO> selectWidget = new SelectWidget<>("Untreated Error Notifications", listDTO, new ErrorNotificationPrinter());
        selectWidget.show();
        if(selectWidget.selectedOption() != 0){
            ErrorNotificationDTO endto = selectWidget.selectedElement();
            ErrorNotificationUntreated enu = anc.findErrorNotificationUntreatedByID(endto.getErrorNotificationID());
            anc.archiveNotification(enu);
            System.out.printf("\nError Notification with the id %d is now archived\n", endto.getErrorNotificationID());
        }

        return false;
    }

    @Override
    public String headline() {
        return "Archive Error Notification";
    }
}
