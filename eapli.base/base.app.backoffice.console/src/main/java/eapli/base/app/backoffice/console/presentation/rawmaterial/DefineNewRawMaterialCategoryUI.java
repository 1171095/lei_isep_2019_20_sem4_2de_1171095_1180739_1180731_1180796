package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.productmanagement.domain.Description;
import eapli.base.rawmaterialmanagement.application.DefineNewRawMaterialCategoryController;
import eapli.framework.actions.menu.Menu;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class DefineNewRawMaterialCategoryUI extends AbstractUI {
    DefineNewRawMaterialCategoryController dnrmcc = new DefineNewRawMaterialCategoryController();

    @Override
    protected boolean doShow (){
        final Description desc = new Description(Console.readLine("Description"));

        try {
            this.dnrmcc.addRawMaterialCategory(desc);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            System.out.println("There was an error in the process.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Define New Raw Material Category";
    }
}
