package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productionordermanagement.application.ListProductionOrderDTOService;
import eapli.base.productionordermanagement.application.ProductionOrderManagementService;
import eapli.base.productionordermanagement.domain.ProductionOrderDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListProductionOrdesUI extends AbstractListUI<ProductionOrderDTO> {

    private ListProductionOrderDTOService listDTO = new ListProductionOrderDTOService();
    private ProductionOrderManagementService list = new ProductionOrderManagementService();

    @Override
    protected Iterable<ProductionOrderDTO> elements() {
        return listDTO.getProductionOrderDTOS(list.listAll());
    }

    @Override
    protected Visitor<ProductionOrderDTO> elementPrinter() {
        return new ProductionOrderPrinter();
    }

    @Override
    protected String elementName() {
        return "Production order";
    }

    @Override
    protected String listHeader() {
        return String.format("# %s-%s-%s-%s-%s-%s-%s", "production order id", "emission date", "execution date", "product id", "quantity", "unit", "state");
    }

    @Override
    protected String emptyMessage() {
        return "No Data";
    }

    @Override
    public String headline() {
        return "List Production Orders";
    }
}
