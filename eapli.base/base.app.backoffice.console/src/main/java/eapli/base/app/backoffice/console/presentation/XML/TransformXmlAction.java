package eapli.base.app.backoffice.console.presentation.XML;

import eapli.framework.actions.Action;

public class TransformXmlAction implements Action {
    @Override
    public boolean execute() {
        return new TransformXmlUI().show();
    }
}
