package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.actions.Action;
import eapli.framework.presentation.console.AbstractListUI;

public class ListProductionOrdesAction implements Action {
    @Override
    public boolean execute()

    {
        return new ListProductionOrdesUI().show();
    }
}
