package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.productmanagement.application.ListProductDTOService;
import eapli.base.productmanagement.application.ProductManagementService;
import eapli.base.productmanagement.domain.ProductDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListProductsUI extends AbstractListUI<ProductDTO> {

    private ListProductDTOService listDTO = new ListProductDTOService();
    private ProductManagementService list = new ProductManagementService();

    @Override
    protected Iterable<ProductDTO> elements() {
        return listDTO.getProductDTOS(list.listAll());
    }

    @Override
    protected Visitor<ProductDTO> elementPrinter() {
        return new ProductPrinter();
    }

    @Override
    protected String elementName() {
        return "Product";
    }

    @Override
    protected String listHeader() {
        return String.format("# %s-%s-%s-%s-%s-%s-%s", "production ID", "commercial ID"
                , "brief description", "complete description", "unit", "category", "form");
    }

    @Override
    protected String emptyMessage() {
        return "No Data";
    }

    @Override
    public String headline() {
        return "List Products";
    }
}
