package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.app.backoffice.console.presentation.machine.MachinePrinter;
import eapli.base.errornotificationmanagement.application.ErrorNotificationManagementService;
import eapli.base.errornotificationmanagement.application.ListErrorNotificationDTOService;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListErrorNotificationUntreatedUI extends AbstractListUI<ErrorNotificationDTO> {

    private ErrorNotificationManagementService list = new ErrorNotificationManagementService();
    private ListErrorNotificationDTOService lens = new ListErrorNotificationDTOService();

    @Override
    public String headline() {
        return "List Machines";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<ErrorNotificationDTO> elements() {
        return lens.getErrorNotificationUntreatedDTOS(list.listErrorNotificationsUntreated());
    }

    @Override
    protected Visitor<ErrorNotificationDTO> elementPrinter() {
        return new ErrorNotificationPrinter();
    }

    @Override
    protected String elementName() {
        return "Error Notification Untreated";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s-%s-%s-%s", "Error Notification ID", "Error Type",
                "Error Date", "Production Line", "Machine");
    }
}
