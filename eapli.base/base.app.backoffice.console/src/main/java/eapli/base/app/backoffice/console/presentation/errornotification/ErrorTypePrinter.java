package eapli.base.app.backoffice.console.presentation.errornotification;

import eapli.base.errornotificationmanagement.domain.ErrorNotificationDTO;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.framework.visitor.Visitor;

public class ErrorTypePrinter implements Visitor<ErrorType> {

    @Override
    public void visit(ErrorType visitee) {
        System.out.printf("%s", visitee.toString());
    }
}
