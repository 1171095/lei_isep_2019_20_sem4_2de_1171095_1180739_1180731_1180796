package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.machinemanagement.domain.Configuration;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.framework.visitor.Visitor;

public class ConfigurationPrinter implements Visitor<Configuration> {
    @Override
    public void visit(Configuration visitee) {
        System.out.printf("%s-%s", visitee.getDescription(), visitee.getFile().getName());
    }
}
