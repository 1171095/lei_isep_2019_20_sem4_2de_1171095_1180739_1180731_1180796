package eapli.base.app.backoffice.console.presentation.machine;

import eapli.base.app.backoffice.console.presentation.rawmaterial.RawMaterialCategoryPrinter;
import eapli.base.machinemanagement.application.MachineDTOListService;
import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.MachineDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

public class ListMachineUI extends AbstractListUI<MachineDTO> {
    private MachineManagementService list = new MachineManagementService();
    private MachineDTOListService mdls = new MachineDTOListService();

    @Override
    public String headline() {
        return "List Machines";
    }

    @Override
    protected String emptyMessage() {
        return "No data.";
    }

    @Override
    protected Iterable<MachineDTO> elements() {
        return mdls.dtoMachines(list.listMachines());
    }

    @Override
    protected Visitor<MachineDTO> elementPrinter() {
        return new MachinePrinter();
    }

    @Override
    protected String elementName() {
        return "Machine";
    }

    @Override
    protected String listHeader() {
        return String.format("#  %s-%s-%s-%s-%s-%s-%s", "Machine ID", "Internal Code",
                "Serial Number", "Description", "Installation Date", "Brand", "Model");
    }

}
