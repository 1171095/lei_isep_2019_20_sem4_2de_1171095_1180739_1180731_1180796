package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterialmanagement.application.DefiningNewRawMaterialController;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Console;

public class DefineNewRawMaterialUI extends AbstractUI {

    private final DefiningNewRawMaterialController controller = new DefiningNewRawMaterialController();

    @Override
    protected boolean doShow() {
        final Iterable<RawMaterialCategoryDTO> categories = controller.listRawMaterialCategories();

        final SelectWidget<RawMaterialCategoryDTO> rawMaterialCategoryDTOSelectWidget = new SelectWidget<>("Raw material categories:", categories, new RawMaterialCategoryPrinter());
        rawMaterialCategoryDTOSelectWidget.show();
        if (rawMaterialCategoryDTOSelectWidget.selectedOption() != 0) {
            final RawMaterialCategoryDTO rawDTO = rawMaterialCategoryDTOSelectWidget.selectedElement();
            this.controller.selectRawMaterialCategory(rawDTO.getId());
            final String id = Console.readLine("Raw Material ID");
            final String cat = Console.readLine("Category Description");


            try {
                boolean success = this.controller.addMaterialRawMaterial(id,cat);
                if (success)
                {
                    System.out.println("Registered material as raw material successfully ");
                }else{
                    System.out.println("Error registering raw material successfully");
                }
            } catch (final IntegrityViolationException | ConcurrencyException e) {
                System.out.println("ERror creating raw material ");
            }
        }


        return false;
    }

    @Override
    public String headline() { return "Defining Material as Raw Material";}
}
