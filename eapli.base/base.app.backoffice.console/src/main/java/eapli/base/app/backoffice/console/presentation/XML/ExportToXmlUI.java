package eapli.base.app.backoffice.console.presentation.XML;

import eapli.base.xmlmanagement.ExportToXmlController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import org.xml.sax.SAXException;

import java.util.ArrayList;
import java.util.List;

public class ExportToXmlUI extends AbstractUI {

    @Override
    protected boolean doShow() {

        boolean flag = false;
        ExportToXmlController controller = new ExportToXmlController();
        List<String> availableOptions = new ArrayList<>();
        availableOptions.add("Products");            //case 1
        availableOptions.add("RawMaterials");        //case 2
        availableOptions.add("Materials");           //case 3
        availableOptions.add("ProductionOrders");    //case 4
        availableOptions.add("Machines");            //case 5
        availableOptions.add("Deposits");            //case 6
        availableOptions.add("ProductionLines");     //case 7
        availableOptions.add("Parcels");             //case 8
        availableOptions.add("EXPORT ALL");          //CASE 9
        SelectWidget<String> selectWidget = new SelectWidget<>("Available export options", availableOptions);
        List<String> selectedOptions = new ArrayList<>();
        do{
            try {
                Thread.sleep(1100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            selectWidget.show();
            if (selectWidget.selectedOption()==0)
                break;
            switch (selectWidget.selectedOption()){
                case 1:
                    controller.setProducts();
                    System.out.println("=========Products Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 2:
                    controller.setRawMaterials();
                    System.out.println("=========Raw Materials Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 3:
                    controller.setMaterials();
                    System.out.println("=========Materials Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 4:
                    controller.setProductionOrders();
                    System.out.println("=========ProductionOrders Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 5:
                    controller.setMachines();
                    System.out.println("=========Machines Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 6:
                    controller.setDeposits();
                    System.out.println("=========Deposits Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 7:
                    controller.setProductionLines();
                    System.out.println("=========Production Lines Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 8:
                    controller.setParcels();
                    System.out.println("=========Parcels Selected===========");
                    selectedOptions.add(selectWidget.selectedElement());
                    break;
                case 9:
                    controller.setAllObjects();
                    System.out.println("==========All Objects===========");
                    selectedOptions.addAll(availableOptions);
                    flag=true;
                    break;
                case 0:
                    flag=true;
                    break;

            }
        }while (!flag);

        try {
            if (controller.writeToXML()){
                System.out.println("Exported Data");
                System.out.println("=======================");
                for (String s:selectedOptions) {
                    System.out.println(s);
                }
            }
        } catch (SAXException e) {
            e.printStackTrace();
        }


        return false;
    }



    @Override
    public String headline() {
        return "Export info to xml file";
    }
}
