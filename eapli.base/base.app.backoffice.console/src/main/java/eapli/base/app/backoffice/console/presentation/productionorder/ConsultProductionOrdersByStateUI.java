package eapli.base.app.backoffice.console.presentation.productionorder;

import eapli.base.productionordermanagement.application.ConsultProductionOrdersByStateController;
import eapli.base.productionordermanagement.domain.ProductionOrderState;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

import java.util.Arrays;
import java.util.List;

public class ConsultProductionOrdersByStateUI extends AbstractUI {
    
    private final ConsultProductionOrdersByStateController consultProductionOrdersByStateController = new ConsultProductionOrdersByStateController();
    @Override
    protected boolean doShow() {

        SelectWidget<ProductionOrderState> states = new SelectWidget<>("Production orders with state:", Arrays.asList(ProductionOrderState.values()));
        do{
            states.show();
            if (states.selectedOption()==0)
                break;

            List<Long> listOrders = consultProductionOrdersByStateController.consultProductionOrdersByState(states.selectedElement().toString());

            if (listOrders.isEmpty())
                System.out.println("no production orders in selected state");

            else{
                SelectWidget<Long> productionOrderIDSelectWidget = new SelectWidget<>("Select production order for further detail:",listOrders);
                do{
                    productionOrderIDSelectWidget.show();
                    if(productionOrderIDSelectWidget.selectedOption()==0)
                        break;
                    System.out.println("Detailed Production order:" + productionOrderIDSelectWidget.selectedElement() +
                    "\n======================================================================");
                    System.out.println(consultProductionOrdersByStateController.detailedProductionOrder(productionOrderIDSelectWidget.selectedElement()).toString());

                }while (true);
            }

        }while(true);

        return true;
    }

    @Override
    public String headline() {
        return "Consult Production orders in a certain state";
    }
}
