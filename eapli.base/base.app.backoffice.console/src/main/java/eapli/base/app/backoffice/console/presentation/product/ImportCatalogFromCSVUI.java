package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.productmanagement.application.ImportCatalogFromCSVController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class ImportCatalogFromCSVUI extends AbstractUI {

    private final ImportCatalogFromCSVController theController = new ImportCatalogFromCSVController();

    @Override
    protected boolean doShow() {
        final String fileName = Console.readLine("File name:");

        try {
            if(this.theController.selectInputFile(fileName)){
                String output = this.theController.importCatalog();

                System.out.println(output);
            }
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("There was an error.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Import Catalog From CSV";
    }
}
