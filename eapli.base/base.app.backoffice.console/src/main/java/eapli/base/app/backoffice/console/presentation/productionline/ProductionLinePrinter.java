package eapli.base.app.backoffice.console.presentation.productionline;

import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.framework.visitor.Visitor;

public class ProductionLinePrinter implements Visitor<ProductionLineDTO> {
    @Override
    public void visit(ProductionLineDTO visitee) {
        System.out.printf("%d-%s", visitee.getProdLineID(),visitee.getLstMachines().toString());
    }
}
