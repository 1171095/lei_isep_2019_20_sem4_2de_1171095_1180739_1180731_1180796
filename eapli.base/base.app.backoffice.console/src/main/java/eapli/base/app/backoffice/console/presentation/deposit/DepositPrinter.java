package eapli.base.app.backoffice.console.presentation.deposit;

import eapli.base.depositmanagement.domain.DepositDTO;
import eapli.framework.visitor.Visitor;

public class DepositPrinter implements Visitor<DepositDTO> {
    @Override
    public void visit(DepositDTO visitee) {
        System.out.printf("%s - %s", visitee.getId(), visitee.getDescription());
    }
}
