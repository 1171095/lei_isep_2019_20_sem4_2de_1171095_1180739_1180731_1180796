package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.base.rawmaterialmanagement.domain.RawMaterialDTO;
import eapli.framework.visitor.Visitor;

public class RawMaterialPrinter implements Visitor<RawMaterialDTO> {


    @Override
    public void visit(RawMaterialDTO visitee) {
        System.out.printf("%s-%s", visitee.getRawMaterialID(), visitee.getCategory());
    }
}
