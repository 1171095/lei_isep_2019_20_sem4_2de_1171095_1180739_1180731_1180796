package eapli.base.app.backoffice.console.presentation.rawmaterial;

import eapli.base.rawmaterialmanagement.domain.RawMaterialCategoryDTO;
import eapli.framework.visitor.Visitor;

public class RawMaterialCategoryPrinter implements Visitor<RawMaterialCategoryDTO> {


    @Override
    public void visit(RawMaterialCategoryDTO visitee) {
        System.out.printf("%s-%s", visitee.getId(), visitee.getDesc());
    }
}
