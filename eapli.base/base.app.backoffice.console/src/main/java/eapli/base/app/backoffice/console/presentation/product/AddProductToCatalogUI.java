package eapli.base.app.backoffice.console.presentation.product;

import eapli.base.productmanagement.application.AddProductToCatalogController;
import eapli.base.productmanagement.domain.*;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class AddProductToCatalogUI extends AbstractUI {

    private final AddProductToCatalogController theController = new AddProductToCatalogController();

    @Override
    protected boolean doShow() {
        final String id = Console.readLine("Product id :");
        final CommercialCode cCode = new CommercialCode(Console.readLine("Commercial code :"));
        final BriefDescription bDesc = new BriefDescription(Console.readLine("Brief Description :"));
        final Description cDesc = new Description(Console.readLine("Complete Description :"));
        final Unit unit = new Unit(Console.readLine("Unit :"));
        final ProductCategory cat = new ProductCategory(Console.readLine("Product Category:"));

        try {
            this.theController.registerProduct(id, cCode, bDesc, cDesc, unit, cat);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("There was an error.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Add Product To Catalog";
    }
}
