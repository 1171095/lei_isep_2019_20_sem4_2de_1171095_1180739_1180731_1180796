package eapli.base.app.backoffice.console.presentation.deposit;

import eapli.base.depositmanagement.application.SpecifyNewDepositController;
import eapli.base.productmanagement.domain.Description;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class SpecifyNewDepositUI extends AbstractUI {

    private final SpecifyNewDepositController theController = new SpecifyNewDepositController();

    @Override
    protected boolean doShow() {

        final Description description = new Description(Console.readLine("Deposit Description:"));
        try {
            this.theController.specifyNewDeposit(description);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("There was an error.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Specify New Deposit";
    }
}
