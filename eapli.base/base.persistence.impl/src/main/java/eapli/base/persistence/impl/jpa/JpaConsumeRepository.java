package eapli.base.persistence.impl.jpa;

import eapli.base.consumemanagement.domain.Consume;
import eapli.base.consumemanagement.repositories.ConsumeRepository;

public class JpaConsumeRepository  extends BasepaRepositoryBase<Consume, Long, Long> implements ConsumeRepository {

    public JpaConsumeRepository() {
        super("ConsumeId");
    }

    @Override
    public Iterable<Consume> getAllConsumes() {
        return null;
    }

}
