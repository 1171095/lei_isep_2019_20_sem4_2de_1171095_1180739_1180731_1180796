package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;

public class JpaMachineRepository extends BasepaRepositoryBase<Machine, Long, Long> implements MachineRepository {

    public JpaMachineRepository(){
        super("machineID");
    }



    @Override
    public Iterable<Machine> getAllMachines() {
        return findAll();
    }

    @Override
    public Optional<Machine> findByMachineID(Long id){return findById(id);}
}
