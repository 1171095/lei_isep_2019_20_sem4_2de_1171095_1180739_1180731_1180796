/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.productionformmanagement.domain.ProductionForm;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

public class JpaProductionFormRepository
    extends JpaAutoTxRepository <ProductionForm, Long, Long>
        implements ProductionFormRepository {
    
    public JpaProductionFormRepository(TransactionalContext autoTx){
        super(autoTx, "idProdForm");
    }
    
    public JpaProductionFormRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "idProdForm");
    }


    @Override
    public Iterable<ProductionForm> lstProdFile() {
        return findAll();
    }
}
