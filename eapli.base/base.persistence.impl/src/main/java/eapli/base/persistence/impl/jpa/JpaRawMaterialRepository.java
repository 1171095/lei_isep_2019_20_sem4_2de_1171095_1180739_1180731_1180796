package eapli.base.persistence.impl.jpa;

import eapli.base.Application;

import eapli.base.rawmaterialmanagement.repositories.RawMaterialRepository;
import eapli.base.rawmaterialmanagement.domain.RawMaterial;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;

public class JpaRawMaterialRepository extends BasepaRepositoryBase<RawMaterial, String, String> implements RawMaterialRepository {


    public JpaRawMaterialRepository(){
        super("rawMaterialID");
    }

    @Override
    public Optional<RawMaterial> findByRawMaterialID(String id) {
        return findById(id);
    }

    @Override
    public Iterable<RawMaterial> getAllRawMaterials() {
        return findAll();
    }

}
