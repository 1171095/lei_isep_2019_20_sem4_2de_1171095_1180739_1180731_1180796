package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.EndActivityMessage;
import eapli.base.messagemanagement.repositories.EndActivityMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaEndActivityMessageRepository extends JpaAutoTxRepository<EndActivityMessage, Long, Long> implements EndActivityMessageRepository {

    public JpaEndActivityMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaEndActivityMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }
    @Override
    public Iterable<EndActivityMessage> getAllEndActivityMessages() {
        return findAll();
    }

    @Override
    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<EndActivityMessage> query =
                super.createQuery("select cm from EndActivityMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", EndActivityMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<EndActivityMessage> getEndActivityUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<EndActivityMessage> query =
                super.createQuery("select cm from EndActivityMessage cm where cm.machineID = :m and cm.processed = 0", EndActivityMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public EndActivityMessage updateMessage(EndActivityMessage eam) {
        return this.repo.update(eam);
    }
}
