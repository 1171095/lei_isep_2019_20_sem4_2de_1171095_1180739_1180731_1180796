package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.repositories.ChargebackMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaChargebackMessageRepository extends JpaAutoTxRepository<ChargebackMessage, Long, Long> implements ChargebackMessageRepository {

    public JpaChargebackMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaChargebackMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }

    @Override
    public Iterable<ChargebackMessage> getAllChargebackMessages() {
        return findAll();
    }

    @Override
    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<ChargebackMessage> query =
                super.createQuery("select cm from ChargebackMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", ChargebackMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<ChargebackMessage> getChargebackUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<ChargebackMessage> query =
                super.createQuery("select cm from ChargebackMessage cm where cm.machineID = :m and cm.processed = 0", ChargebackMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public ChargebackMessage updateMessage(ChargebackMessage cm) {
        return this.repo.update(cm);
    }

}
