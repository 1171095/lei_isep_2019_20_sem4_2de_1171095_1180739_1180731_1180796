package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ConsumeMessage;
import eapli.base.messagemanagement.repositories.ConsumeMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaConsumeMessageRepository extends JpaAutoTxRepository<ConsumeMessage, Long, Long> implements ConsumeMessageRepository {

    public JpaConsumeMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaConsumeMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }

    @Override
    public Iterable<ConsumeMessage> getAllConsumeMessages() {
        return findAll();
    }

    @Override
    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<ConsumeMessage> query =
                super.createQuery("select cm from ConsumeMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", ConsumeMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<ConsumeMessage> getConsumeUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<ConsumeMessage> query =
                super.createQuery("select cm from ConsumeMessage cm where cm.machineID = :m and cm.processed = 0", ConsumeMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public ConsumeMessage updateMessage(ConsumeMessage cm) {
        return this.repo.update(cm);
    }
}
