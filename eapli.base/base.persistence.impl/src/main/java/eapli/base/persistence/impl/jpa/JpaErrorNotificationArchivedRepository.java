package eapli.base.persistence.impl.jpa;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationArchivedRepository;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;

import javax.persistence.TypedQuery;
import java.util.Optional;

public class JpaErrorNotificationArchivedRepository extends BasepaRepositoryBase<ErrorNotificationArchived, Long, Long>
        implements ErrorNotificationArchivedRepository {

    public JpaErrorNotificationArchivedRepository(){ super("errorNotificationArchivedID"); }

    @Override
    public Optional<ErrorNotificationArchived> findByErrorNotificationArchivedID(Long id){ return findById(id); }

    @Override
    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchived(){return findAll();}


    @Override
    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByErrorType(ErrorType errorType){
        final TypedQuery<ErrorNotificationArchived> queryErrorNotificationArchived =
                super.createQuery("select en from ErrorNotificationArchived en where en.errorType = :et"
                        , ErrorNotificationArchived.class);

        queryErrorNotificationArchived.setParameter("et", errorType);

        return queryErrorNotificationArchived.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByErrorDate(ErrorDate errorDate){
        final TypedQuery<ErrorNotificationArchived> queryErrorNotificationArchived =
                super.createQuery("select en from ErrorNotificationArchived en where en.errorDate = :ed"
                        , ErrorNotificationArchived.class);

        queryErrorNotificationArchived.setParameter("ed", errorDate);

        return queryErrorNotificationArchived.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByProdLine(ProductionLine productionLine){
        final TypedQuery<ErrorNotificationArchived> queryErrorNotificationArchived =
                super.createQuery("select en from ErrorNotificationArchived en where en.prodLine = :pl"
                        , ErrorNotificationArchived.class);

        queryErrorNotificationArchived.setParameter("pl", productionLine);

        return queryErrorNotificationArchived.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationArchived> getAllErrorNotificationArchivedByMachine(Machine machine){
        final TypedQuery<ErrorNotificationArchived> queryErrorNotificationArchived =
                super.createQuery("select en from ErrorNotificationArchived en where en.machine = :m"
                        , ErrorNotificationArchived.class);

        queryErrorNotificationArchived.setParameter("m", machine);

        return queryErrorNotificationArchived.getResultList();
    }

    @Override
    public Iterable<ErrorDate> getAllErrorDates(){
        final TypedQuery<ErrorDate> queryErrorDate =
                super.createQuery("select en.errorDate from ErrorNotificationArchived en", ErrorDate.class);

        return queryErrorDate.getResultList();
    }

    @Override
    public ErrorNotificationArchived archiveErrorNotification(ErrorNotificationArchived ena) {
        return save(ena);
    }
}
