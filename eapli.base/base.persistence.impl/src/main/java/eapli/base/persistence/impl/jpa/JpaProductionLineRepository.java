package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;

public class JpaProductionLineRepository extends JpaAutoTxRepository<ProductionLine, Long, Long> implements ProductionLineRepository {


    public JpaProductionLineRepository(String puname){
        super(puname,Application.settings().getExtendedPersistenceProperties(),"plID");

    }

    public JpaProductionLineRepository(TransactionalContext autoTx){

            super(autoTx, "plID");
    }

    public Iterable<ProductionLine> getListProductionLine(){
        return findAll();
    }

    @Override
    public void update(ProductionLine selectedElement) {
        repo.update(selectedElement);
    }

    ;

    @Override
    public Optional<ProductionLine> ofIdentity(Long id) {
        return Optional.empty();
    }

    @Override
    public void deleteOfIdentity(Long entityId) {

    }


    @Override
    public Optional<ProductionLine> findByProductionLineID(Long id){ return findById(id); };
}
