package eapli.base.persistence.impl.jpa;

import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.domain.ProductionOrder;
import eapli.base.productionordermanagement.repositories.ProductionOrderRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.util.Optional;

public class JpaProductionOrderRepository extends JpaAutoTxRepository<ProductionOrder,Long,Long> implements ProductionOrderRepository {
    public JpaProductionOrderRepository(String persistenceUnitName) {
        super(persistenceUnitName, "productionOrderID");
    }

    public JpaProductionOrderRepository(TransactionalContext autoTx) {
        super(autoTx, "productionOrderID");
    }

    @Override
    public Optional<ProductionOrder> findByProductionOrderID(Long id) {
        return findById(id);
    }

    @Override
    public Iterable<ProductionOrder> findAllProductionOrders() {
        return findAll();
    }

    @Override
    public ProductionOrder updateProductionOrder(ProductionOrder prodOrd) {
        return this.repo.update(prodOrd);
    }
    @Override
    public Iterable<Long> getProductionOrdersByState(String state) {
        final TypedQuery<Long> query = super.createQuery("SELECT productionOrderID FROM ProductionOrder WHERE ProductionOrderState = '" + state +"'", Long.class);

        return query.getResultList();
    }

    @Override
    public Iterable<String> getAllOrderID() {
        final TypedQuery<String> queryOrderID = super.createQuery("select id from Parcel", String.class);


        return queryOrderID.getResultList();
    }

    @Override
    public Iterable<Long> getAllProductionOrdersOfAnOrder(Parcel parcel) {
        // falta saber se os nomes das coisas estao bem
        final TypedQuery<Long> queryProductionOrder = super.createQuery("select productionOrderID from Parcel where id =  '"+ parcel +"'",Long.class);

        return queryProductionOrder.getResultList();
    }


}
