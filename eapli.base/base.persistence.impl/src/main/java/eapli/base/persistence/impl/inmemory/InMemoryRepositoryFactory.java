package eapli.base.persistence.impl.inmemory;

import eapli.base.chargebackmanagement.repositories.ChargebackRepository;
import eapli.base.clientusermanagement.repositories.ClientUserRepository;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.consumemanagement.repositories.ConsumeRepository;
import eapli.base.depositmanagement.repositories.DepositRepository;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationArchived;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationArchivedRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;
import eapli.base.infrastructure.bootstrapers.BaseBootstrapper;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.repositories.*;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.productionordermanagement.repositories.OrderRepository;
import eapli.base.productionordermanagement.repositories.ProductionOrderRepository;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.rawmaterialmanagement.repositories.MaterialRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.InMemoryUserRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

	static {
		// only needed because of the in memory persistence
		new BaseBootstrapper().execute();
	}

	@Override
	public UserRepository users(final TransactionalContext tx) {
		return new InMemoryUserRepository();
	}

	@Override
	public UserRepository users() {
		return users(null);
	}


	@Override
	public ClientUserRepository clientUsers(final TransactionalContext tx) {

		return new InMemoryClientUserRepository();
	}

	@Override
	public ClientUserRepository clientUsers() {
		return clientUsers(null);
	}

	@Override
	public SignupRequestRepository signupRequests() {
		return signupRequests(null);
	}

	@Override
	public ProductRepository product() {
		return null;
	}

	@Override
	public ProductRepository product(TransactionalContext autoTx) {
		return null;
	}

	@Override
	public SignupRequestRepository signupRequests(final TransactionalContext tx) {
		return new InMemorySignupRequestRepository();
	}


	@Override
	public DepositRepository deposit() {
		return null;
	}

	@Override
	public RawMaterialCategoryRepository rawMaterialCategory(){return null;}

	@Override
	public ProductionLineRepository productionLine (){return null;}

	@Override
	public ProductionLineRepository productionLine(TransactionalContext txCtx) {
		return null;
	}

	;

	@Override
	public MachineRepository machine(){return null;}

	@Override
	public ProductionFormRepository productionFile() {
		return null;
	}

	@Override
	public ProductionFormRepository productionFile(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ProductionOrderRepository productionOrder() {
		return null;
	}

	@Override
	public ProductionOrderRepository productionOrder(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public RawMaterialRepository rawMaterial(){return null;}

	@Override
	public MaterialRepository material() {
		return null;
	}

	@Override
	public ErrorNotificationUntreatedRepository errorNotificationUntreated(){return null;}

	@Override
	public ErrorNotificationArchivedRepository errorNotificationArchived(){return null;}

	@Override
	public ChargebackMessageRepository chargebackMessage() {
		return null;
	}

	@Override
	public ConsumeMessageRepository ConsumeMessage() {
		return null;
	}

	@Override
	public EndActivityMessageRepository endActivity() {
		return null;
	}

	@Override
	public ErrorMessageRepository errorMessage() {
		return null;
	}

	@Override
	public ProductionDeliveryMessageRepository productionDeliveryMessage() {
		return null;
	}

	@Override
	public ProductionMessageRepository productionMessage() {
		return null;
	}

	@Override
	public RestartActivityMessageRepository restartActivityMessage() {
		return null;
	}

	@Override
	public StartMessageRepository startActivityMessage() {
		return null;
	}

	@Override
	public ChargebackMessageRepository chargebackMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ConsumeMessageRepository ConsumeMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public EndActivityMessageRepository endActivity(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ErrorMessageRepository errorMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ProductionDeliveryMessageRepository productionDeliveryMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ProductionMessageRepository productionMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public RestartActivityMessageRepository restartActivityMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public StartMessageRepository startActivityMessage(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public ChargebackRepository chargeback() {
		return null;
	}

	@Override
	public ConsumeRepository consume() {
		return null;
	}

	@Override
	public OrderRepository order(TransactionalContext txCtx) {
		return null;
	}

	@Override
	public OrderRepository order() {
		return null;
	}


	@Override
	public TransactionalContext newTransactionalContext() {
		// in memory does not support transactions...
		return null;
	}

}
