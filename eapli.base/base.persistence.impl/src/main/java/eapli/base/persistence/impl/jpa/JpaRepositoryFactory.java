package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.chargebackmanagement.repositories.ChargebackRepository;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.consumemanagement.repositories.ConsumeRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationArchivedRepository;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.messagemanagement.repositories.*;
import eapli.base.productionformmanagement.repositories.ProductionFormRepository;
import eapli.base.productionlinemanagement.repositories.ProductionLineRepository;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.productionordermanagement.repositories.OrderRepository;
import eapli.base.productionordermanagement.repositories.ProductionOrderRepository;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.depositmanagement.repositories.DepositRepository;
import eapli.base.rawmaterialmanagement.repositories.MaterialRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.JpaAutoTxUserRepository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import javafx.application.Preloader;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

	@Override
	public UserRepository users(final TransactionalContext autoTx) {
		return new JpaAutoTxUserRepository(autoTx);
	}

	@Override
	public UserRepository users() {
		return new JpaAutoTxUserRepository(Application.settings().getPersistenceUnitName(),
				Application.settings().getExtendedPersistenceProperties());
	}


	@Override
	public JpaClientUserRepository clientUsers(final TransactionalContext autoTx) {
		return new JpaClientUserRepository(autoTx);
	}

	@Override
	public JpaClientUserRepository clientUsers() {
		return new JpaClientUserRepository(Application.settings().getPersistenceUnitName());
	}

	@Override
	public SignupRequestRepository signupRequests(final TransactionalContext autoTx) {
		return new JpaSignupRequestRepository(autoTx);
	}

	@Override
	public SignupRequestRepository signupRequests() {
		return new JpaSignupRequestRepository(Application.settings().getPersistenceUnitName());
	}

	@Override
	public ProductRepository product() {
		return new JpaProductRepository(Application.settings().getPersistenceUnitName());
	}

	@Override
	public ProductRepository product(TransactionalContext autoTx) {
		return new JpaProductRepository(autoTx);
	}

	@Override
	public DepositRepository deposit() {
		return new JpaDepositRepository();
	}

	@Override
	public RawMaterialCategoryRepository rawMaterialCategory(){
		return new JpaRawMaterialCategoryRepository();
	}

	@Override
	public ProductionLineRepository productionLine(){
		return new JpaProductionLineRepository(Application.settings().getPersistenceUnitName());
	}

	@Override
	public ProductionLineRepository productionLine(TransactionalContext txCtx) {
		return new JpaProductionLineRepository(txCtx);
	}

	@Override
	public MachineRepository machine(){
		return new JpaMachineRepository();
	}

	@Override
	public ProductionFormRepository productionFile() {
		return new JpaProductionFormRepository(Application.settings().getPersistenceUnitName());
	}

	@Override
	public ProductionFormRepository productionFile(final TransactionalContext txCtx) {
		return new JpaProductionFormRepository(txCtx);
	}

	@Override
	public ProductionOrderRepository productionOrder() {
		return new JpaProductionOrderRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ProductionOrderRepository productionOrder(TransactionalContext txCtx) {
		return new JpaProductionOrderRepository(txCtx);
	}

	@Override
	public RawMaterialRepository rawMaterial(){
		return new JpaRawMaterialRepository();
	}

	@Override
	public MaterialRepository material() {
		return new JpaMaterialRepository();
	}

	@Override
	public ErrorNotificationUntreatedRepository errorNotificationUntreated(){return new JpaErrorNotificationUntreatedRepository();}

	@Override
	public ErrorNotificationArchivedRepository errorNotificationArchived(){return new JpaErrorNotificationArchivedRepository();}

	@Override
	public ChargebackMessageRepository chargebackMessage() {
		return new JpaChargebackMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ChargebackMessageRepository chargebackMessage(TransactionalContext txCtx) {
		return new JpaChargebackMessageRepository(txCtx);
	}

	@Override
	public ConsumeMessageRepository ConsumeMessage() {
		return new JpaConsumeMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ConsumeMessageRepository ConsumeMessage(TransactionalContext txCtx) {
		return new JpaConsumeMessageRepository(txCtx);
	}

	@Override
	public EndActivityMessageRepository endActivity() {
		return new JpaEndActivityMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public EndActivityMessageRepository endActivity(TransactionalContext txCtx) {
		return new JpaEndActivityMessageRepository(txCtx);
	}

	@Override
	public ErrorMessageRepository errorMessage() {
		return new JpaErrorMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ErrorMessageRepository errorMessage(TransactionalContext txCtx) {
		return new JpaErrorMessageRepository(txCtx);
	}

	@Override
	public ProductionDeliveryMessageRepository productionDeliveryMessage() {
		return new JpaProductionDeliveryMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ProductionDeliveryMessageRepository productionDeliveryMessage(TransactionalContext txCtx) {
		return new JpaProductionDeliveryMessageRepository(txCtx);
	}

	@Override
	public ProductionMessageRepository productionMessage() {
		return new JpaProductionMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public ProductionMessageRepository productionMessage(TransactionalContext txCtx) {
		return new JpaProductionMessageRepository(txCtx);
	}

	@Override
	public RestartActivityMessageRepository restartActivityMessage() {
		return new JpaRestartActivityMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public RestartActivityMessageRepository restartActivityMessage(TransactionalContext txCtx) {
		return new JpaRestartActivityMessageRepository(txCtx);
	}

	@Override
	public StartMessageRepository startActivityMessage() {
		return new JpaStartMessageRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public StartMessageRepository startActivityMessage(TransactionalContext txCtx) {
		return new JpaStartMessageRepository(txCtx);
	}

	@Override
	public ChargebackRepository chargeback() {
		return new JpaChargebackRepository();
	}

	@Override
	public ConsumeRepository consume() {
		return new JpaConsumeRepository();
	}

	@Override
	public OrderRepository order(TransactionalContext txCtx) {
		return new JpaOrderRepository(txCtx);
	}

	@Override
	public OrderRepository order() {
		return new JpaOrderRepository((Application.settings().getPersistenceUnitName()));
	}

	@Override
	public TransactionalContext newTransactionalContext() {
		return JpaAutoTxRepository.buildTransactionalContext(Application.settings().getPersistenceUnitName(),
				Application.settings().getExtendedPersistenceProperties());
	}

}
