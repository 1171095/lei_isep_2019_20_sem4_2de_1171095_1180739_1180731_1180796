package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.depositmanagement.domain.Deposit;
import eapli.base.depositmanagement.repositories.DepositRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;



public class JpaDepositRepository extends BasepaRepositoryBase<Deposit, Long, Long> implements DepositRepository {

    public JpaDepositRepository() {
        super("depositID");

    }

    @Override
    public Iterable<Deposit> getAllDeposits() {
        return findAll();
    }
}
