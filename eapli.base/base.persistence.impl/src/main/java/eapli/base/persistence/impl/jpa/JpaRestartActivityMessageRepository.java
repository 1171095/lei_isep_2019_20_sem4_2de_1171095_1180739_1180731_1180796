package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.RestartActivityMessage;
import eapli.base.messagemanagement.repositories.RestartActivityMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaRestartActivityMessageRepository extends JpaAutoTxRepository<RestartActivityMessage, Long, Long> implements RestartActivityMessageRepository {

    public JpaRestartActivityMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaRestartActivityMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }
    @Override
    public Iterable<RestartActivityMessage> getAllRestartActivityMessages() {
        return null;
    }

    @Override
    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<RestartActivityMessage> query =
                super.createQuery("select cm from RestartActivityMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", RestartActivityMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<RestartActivityMessage> getRestartActivityUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<RestartActivityMessage> query =
                super.createQuery("select cm from RestartActivityMessage cm where cm.machineID = :m and cm.processed = 0", RestartActivityMessage.class);

        query.setParameter("m", machine.identity());
        return query.getResultList();
    }

    @Override
    public RestartActivityMessage updateMessage(RestartActivityMessage ram) {
        return this.repo.update(ram);
    }
}
