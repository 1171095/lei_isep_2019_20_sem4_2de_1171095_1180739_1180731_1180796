package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.StartActivityMessage;
import eapli.base.messagemanagement.domain.StartActivityMessage;
import eapli.base.messagemanagement.repositories.StartMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaStartMessageRepository extends JpaAutoTxRepository<StartActivityMessage, Long, Long> implements StartMessageRepository {

    public JpaStartMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaStartMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }
    @Override
    public Iterable<StartActivityMessage> getAllStartActivityMessages() {
        return findAll();
    }

    @Override
    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<StartActivityMessage> query =
                super.createQuery("select cm from StartActivityMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", StartActivityMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<StartActivityMessage> getStartActivityUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<StartActivityMessage> query =
                super.createQuery("select cm from StartActivityMessage cm where cm.machineID = :m and cm.processed = 0", StartActivityMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public StartActivityMessage updateMessage(StartActivityMessage sam) {
        return this.repo.update(sam);
    }
}
