/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;


import eapli.base.rawmaterialmanagement.domain.Material;
import eapli.base.rawmaterialmanagement.repositories.MaterialRepository;


public class JpaMaterialRepository 
    extends BasepaRepositoryBase<Material, String, String>
        implements MaterialRepository {

    public JpaMaterialRepository() {
        super("materialID");
    }

}
