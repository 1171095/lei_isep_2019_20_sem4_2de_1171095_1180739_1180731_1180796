package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ProductionMessage;
import eapli.base.messagemanagement.repositories.ProductionMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaProductionMessageRepository extends JpaAutoTxRepository<ProductionMessage, Long, Long> implements ProductionMessageRepository {

    public JpaProductionMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaProductionMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }
    @Override
    public Iterable<ProductionMessage> getAllProductionMessages() {
        return findAll();
    }

    @Override
    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<ProductionMessage> query =
                super.createQuery("select cm from ProductionMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", ProductionMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<ProductionMessage> getProductionUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<ProductionMessage> query =
                super.createQuery("select cm from ProductionMessage cm where cm.machineID = :m and cm.processed = 0", ProductionMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public ProductionMessage updateMessage(ProductionMessage pm) {
        return this.repo.update(pm);
    }


}
