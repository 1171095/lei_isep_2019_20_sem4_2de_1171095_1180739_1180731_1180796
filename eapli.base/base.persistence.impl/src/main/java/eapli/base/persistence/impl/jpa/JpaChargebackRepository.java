package eapli.base.persistence.impl.jpa;

import eapli.base.chargebackmanagement.domain.Chargeback;
import eapli.base.chargebackmanagement.repositories.ChargebackRepository;

public class JpaChargebackRepository extends BasepaRepositoryBase<Chargeback, Long, Long> implements ChargebackRepository {

    public JpaChargebackRepository(){super("chargebackId");}

    @Override
    public Iterable<Chargeback> getAllChargebacks() {
        return null;
    }
}
