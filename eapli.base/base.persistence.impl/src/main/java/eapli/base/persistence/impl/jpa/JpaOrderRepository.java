package eapli.base.persistence.impl.jpa;

import eapli.base.productionordermanagement.domain.Parcel;
import eapli.base.productionordermanagement.repositories.OrderRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;


public class JpaOrderRepository extends JpaAutoTxRepository<Parcel,String,String> implements OrderRepository {
    {
    }

    public JpaOrderRepository(String persistenceUnitName) {
        super(persistenceUnitName, "id");
    }


    public JpaOrderRepository(TransactionalContext autoTx) {
        super(autoTx, "id");
    }

    @Override
    public Iterable<Parcel> listOrders() {
        return findAll();
    }

    @Override
    public Optional<Parcel> findOrderByID(String id) {
        return findById(id);
    }

    @Override
    public Parcel updateOrder(Parcel parcel) {
        return this.repo.update(parcel);
    }

//    @Override
//    public Order updateOrder(Order order) {
//        return this.repo.update(order);
//    }
}
