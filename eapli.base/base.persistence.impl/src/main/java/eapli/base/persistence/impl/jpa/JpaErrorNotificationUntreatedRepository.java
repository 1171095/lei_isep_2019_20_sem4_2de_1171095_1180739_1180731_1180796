package eapli.base.persistence.impl.jpa;

import eapli.base.errornotificationmanagement.domain.ErrorDate;
import eapli.base.errornotificationmanagement.domain.ErrorNotificationUntreated;
import eapli.base.errornotificationmanagement.domain.ErrorType;
import eapli.base.errornotificationmanagement.repositories.ErrorNotificationUntreatedRepository;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.domain.ProductionLine;

import javax.persistence.TypedQuery;
import java.util.Optional;

public class JpaErrorNotificationUntreatedRepository extends BasepaRepositoryBase<ErrorNotificationUntreated, Long, Long>
        implements ErrorNotificationUntreatedRepository {

    public JpaErrorNotificationUntreatedRepository(){ super("errorNotificationUntreatedID"); }

    @Override
    public Optional<ErrorNotificationUntreated> findByErrorNotificationUntreatedID(Long id){ return findById(id); }

    @Override
    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreated(){return findAll();}


    @Override
    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByErrorType(ErrorType errorType){
        final TypedQuery<ErrorNotificationUntreated> queryErrorNotificationUntreated =
                super.createQuery("select en from ErrorNotificationUntreated en where en.errorType = :et"
                        , ErrorNotificationUntreated.class);
        queryErrorNotificationUntreated.setParameter("et", errorType);

        return queryErrorNotificationUntreated.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByErrorDate(ErrorDate errorDate){
        final TypedQuery<ErrorNotificationUntreated> queryErrorNotificationUntreated =
                super.createQuery("select en from ErrorNotificationUntreated en where en.errorDate = :ed"
                        , ErrorNotificationUntreated.class);

        queryErrorNotificationUntreated.setParameter("ed", errorDate);

        return queryErrorNotificationUntreated.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByProdLine(ProductionLine pl){
        final TypedQuery<ErrorNotificationUntreated> queryErrorNotificationUntreated =
                super.createQuery("select en from ErrorNotificationUntreated en where en.prodLine = :pl", ErrorNotificationUntreated.class);

        queryErrorNotificationUntreated.setParameter("pl", pl);

        return queryErrorNotificationUntreated.getResultList();
    }

    @Override
    public Iterable<ErrorNotificationUntreated> getAllErrorNotificationUntreatedByMachine(Machine machine){
        final TypedQuery<ErrorNotificationUntreated> queryErrorNotificationUntreated =
                super.createQuery("select en from ErrorNotificationUntreated en where en.machine = :m", ErrorNotificationUntreated.class);

        queryErrorNotificationUntreated.setParameter("m", machine);

        return queryErrorNotificationUntreated.getResultList();
    }

    @Override
    public Iterable<ErrorDate> getAllErrorDates(){
        final TypedQuery<ErrorDate> queryErrorDate =
                super.createQuery("select en.errorDate from ErrorNotificationUntreated en", ErrorDate.class);

        return queryErrorDate.getResultList();
    }

    @Override
    public ErrorNotificationUntreated addErroNotificationUntreated(ErrorNotificationUntreated enu) {
        return save(enu);
    }

    @Override
    public void removeErrorNotificationUntreated(ErrorNotificationUntreated enu) {
        delete(enu);
    }

}
