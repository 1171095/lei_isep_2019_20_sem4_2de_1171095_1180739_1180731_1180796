package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.productmanagement.domain.Product;
import eapli.base.rawmaterialmanagement.domain.RawMaterialCategory;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.base.rawmaterialmanagement.repositories.RawMaterialCategoryRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;

public class JpaRawMaterialCategoryRepository extends BasepaRepositoryBase <RawMaterialCategory, Long, Long> implements RawMaterialCategoryRepository {


    public JpaRawMaterialCategoryRepository(){
        super("rmcID");

    }

    @Override
    public Optional<RawMaterialCategory> findByRawMaterialCategoryID(Long id) {
        return findById(id);
    }

    @Override
    public Iterable<RawMaterialCategory> getAllRawMaterialCategory (){
        return findAll();
    }
}
