package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.domain.ChargebackMessage;
import eapli.base.messagemanagement.domain.ProductionDeliveryMessage;
import eapli.base.messagemanagement.repositories.ProductionDeliveryMessageRepository;
import eapli.base.persistence.impl.jpa.BasepaRepositoryBase;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.util.Calendar;

public class JpaProductionDeliveryMessageRepository extends JpaAutoTxRepository<ProductionDeliveryMessage, Long, Long> implements ProductionDeliveryMessageRepository {

    public JpaProductionDeliveryMessageRepository(TransactionalContext autoTx) {
        super(autoTx, "messageID");
    }

    public JpaProductionDeliveryMessageRepository(String puname) {
        super(puname, Application.settings().getExtendedPersistenceProperties(), "messageID");
    }

    @Override
    public Iterable<ProductionDeliveryMessage> getAllProductionDeliveryMessages() {
        return findAll();
    }

    @Override
    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachineBetweenDates(Machine machine, Calendar initialDate, Calendar finalDate) {
        final TypedQuery<ProductionDeliveryMessage> query =
                super.createQuery("select cm from ProductionDeliveryMessage cm where cm.machineID = :m and cm.processed = 0 and cm.messageDate between :id and :fd", ProductionDeliveryMessage.class);

        query.setParameter("m", machine.identity());
        query.setParameter("id", initialDate);
        query.setParameter("fd", finalDate);

        return query.getResultList();
    }

    @Override
    public Iterable<ProductionDeliveryMessage> getProductionDeliveryUnprocessedMessagesFromMachine(Machine machine) {
        final TypedQuery<ProductionDeliveryMessage> query =
                super.createQuery("select cm from ProductionDeliveryMessage cm where cm.machineID = :m and cm.processed = 0", ProductionDeliveryMessage.class);

        query.setParameter("m", machine.identity());

        return query.getResultList();
    }

    @Override
    public ProductionDeliveryMessage updateMessage(ProductionDeliveryMessage pdm) {
        return this.repo.update(pdm);
    }
}
