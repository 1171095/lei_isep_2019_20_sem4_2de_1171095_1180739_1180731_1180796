package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.productmanagement.domain.Product;
import eapli.base.productmanagement.repositories.ProductRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;

import java.util.Optional;

public class JpaProductRepository extends JpaAutoTxRepository<Product, String, String> implements ProductRepository {

    public JpaProductRepository(TransactionalContext autoTx){
        super(autoTx, "productionID");
    }

    public JpaProductRepository(String puname){
        super(puname, Application.settings().getExtendedPersistenceProperties(),"productionID");

    }

    public Iterable<Product> findAllProducts(){
        return findAll();
    }
    @Override
    public Optional<Product> findByProductionID(String id) {
        return findById(id);
    }

    @Override
    public Product updateProduct(Product prod) {
        return this.repo.update(prod);
    }
}
