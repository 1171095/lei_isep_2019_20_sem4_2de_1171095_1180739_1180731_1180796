package eapli.base.smm;


import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.repositories.MachineRepository;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UdpSMM {
    static InetAddress targetIP;

    private static final int TIMEOUT_REQUEST = 5;
    private static final int TIMEOUT_RESPONSE = 60;
    private static final int HELLO_TIME = 30;
    private static final int IP_POSITION = 0;
    private static final int STATUS_POSITION = 1;
    private static final String AVAILABLE_STATUS = "Available";
    private static final String UNAVAILABLE_STATUS = "Unavailable";

    private static Map<Long, List<String>> ipsMachines = new HashMap<>();

    private static final int VERSION_OFFSET = 0;
    private static final int CODE_OFFSET = 1;
    private static final int ID_OFFSET = 2;
    private static final int DATA_LENGTH_OFFSET = 4;
    private static final int RAW_DATA_OFFSET = 6;

    private static final byte HELLO_REQUEST = (byte) 0;
    private static final byte MSG_REQUEST = (byte) 1;
    private static final byte ACK_RESPONSE = (byte) 150;
    private static final byte NACK_RESPONSE = (byte) 151;


    public void startUdp() {
        try {
            byte[] data = new byte[512];
            byte[] clean = new byte[512];
            targetIP = InetAddress.getByName("255.255.255.255");

            DatagramSocket sock = new DatagramSocket();
            sock.setBroadcast(true);
            DatagramPacket udpPacket = new DatagramPacket(data, data.length, targetIP, 9998);

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            data[VERSION_OFFSET] =(byte) 0;
            data[CODE_OFFSET] = HELLO_REQUEST;
            data[ID_OFFSET] = (byte) 0;
            data[ID_OFFSET +1] = (byte) 0;
            data[DATA_LENGTH_OFFSET] = (byte) 0;

            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            sock.send(udpPacket);
            udpPacket.setData(clean);
            udpPacket.setLength(clean.length);

            sock.setSoTimeout(TIMEOUT_REQUEST*1000);
            try {
                while(true){
                    sock.receive(udpPacket);
                    data = udpPacket.getData();
                    Long id = (long) ((data[ID_OFFSET] & 0xff) + ((data[ID_OFFSET+1]&0xff) << 8));
                    System.out.println(id);
                    ipsMachines.put(id,new ArrayList<>());
                    System.out.println("size: "+ipsMachines.size());
                    ipsMachines.get(id).add(udpPacket.getAddress().getHostAddress());
                    ipsMachines.get(id).add(AVAILABLE_STATUS);
                    //ipsMachines.get(id).set(IP_POSITION,udpPacket.getAddress().getHostAddress());
                    //ipsMachines.get(id).set(STATUS_POSITION,AVAILABLE_STATUS);
                    udpPacket.setData(clean);
                    udpPacket.setLength(clean.length);
                }
            }catch (SocketTimeoutException ex){
                //ex.printStackTrace();
                System.out.println("First HELLO REQUEST timeout");
            }

            sock.setSoTimeout(TIMEOUT_RESPONSE*1000);
            while (true) {
                Thread.sleep(HELLO_TIME*1000);
                for(Long id: ipsMachines.keySet()){
                    new Thread(new UdpSMMThread(id,ipsMachines)).start();
                }
            }
            //sock.close();
        }catch(IOException | InterruptedException E){
            E.printStackTrace();
        }
    }

}
