package eapli.base.smm;

import eapli.framework.util.Console;

import java.io.IOException;
import java.net.*;
import java.util.List;
import java.util.Map;

public class UdpSMMThread implements Runnable {

    private Long id;
    private Map<Long, List<String>> ipsMachines;

    static InetAddress targetIP;

    private static final int TIMEOUT_REQUEST = 5;
    private static final int TIMEOUT_RESPONSE = 60;
    private static final int HELLO_TIME = 30;
    private static final int IP_POSITION = 0;
    private static final int STATUS_POSITION = 1;
    private static final String AVAILABLE_STATUS = "Available";
    private static final String UNAVAILABLE_STATUS = "Unavailable";

    private static final int VERSION_OFFSET = 0;
    private static final int CODE_OFFSET = 1;
    private static final int ID_OFFSET = 2;
    private static final int DATA_LENGTH_OFFSET = 4;
    private static final int RAW_DATA_OFFSET = 6;

    private static final byte HELLO_REQUEST = (byte) 0;
    private static final byte MSG_REQUEST = (byte) 1;
    private static final byte CONFIG_REQUEST = (byte) 2;
    private static final byte RESET_REQUEST = (byte) 3;
    private static final byte ACK_RESPONSE = (byte) 150;
    private static final byte NACK_RESPONSE = (byte) 151;

    public UdpSMMThread (Long id, Map<Long, List<String>> ipsMachines){
        this.id = id;
        this.ipsMachines = ipsMachines;
    }

    @Override
    public void run() {
        try{
            byte[] data = new byte[512];
            byte[] clean = new byte[512];
            targetIP = InetAddress.getByName(ipsMachines.get(id).get(IP_POSITION));
            DatagramSocket sock = new DatagramSocket();
            DatagramPacket udpPacket = new DatagramPacket(data, data.length, targetIP, 9998);

            //TO DO RESET REQUEST FOR THE MACHINE IN THE FOR EACH
            if(Console.readBoolean("Do you want to reset this machine "+this.id+"? (Y/N)")){
                data[CODE_OFFSET] = RESET_REQUEST;
                System.out.println("Machine ");
            }else{
                data[CODE_OFFSET] = HELLO_REQUEST;
            }

            data[VERSION_OFFSET] =(byte) 0;
            byte firstByteOfID = (byte) (id & 0xff);
            byte secondByteOfID = (byte) (id >> 8);
            data[ID_OFFSET] = firstByteOfID;
            data[ID_OFFSET +1] = secondByteOfID;
            data[DATA_LENGTH_OFFSET] = (byte) 0;

            udpPacket.setData(data);
            udpPacket.setLength(data.length);
            targetIP = InetAddress.getByName(ipsMachines.get(id).get(IP_POSITION));
            udpPacket.setAddress(targetIP);
            sock.send(udpPacket);
            udpPacket.setData(clean);
            udpPacket.setLength(clean.length);
            sock.receive(udpPacket);

            data = udpPacket.getData();
            byte code = data[CODE_OFFSET];
            if(code == ACK_RESPONSE){
                System.out.println("There was no error in the machine: "+ id);
            }else if(code == NACK_RESPONSE){
                System.out.println("There was an error in the machine: "+ id);
            }

        }catch (SocketTimeoutException | SocketException | UnknownHostException ex){
            ipsMachines.get(id).set(STATUS_POSITION,UNAVAILABLE_STATUS);
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
