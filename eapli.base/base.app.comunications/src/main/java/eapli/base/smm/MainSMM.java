package eapli.base.smm;

public class MainSMM {

    public static void main(String[] args) {
        System.out.println("SMM APPLICATION STARTED");
        UdpSMM udp = new UdpSMM();
        udp.startUdp();
        System.out.println("SMM APPLICATION CLOSED, BYE BYE :(");
    }
}
