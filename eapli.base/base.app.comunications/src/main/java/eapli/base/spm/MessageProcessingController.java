package eapli.base.spm;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProcessingStatus;
import eapli.base.productionlinemanagement.domain.ProductionLine;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MessageProcessingController {

    private final ProductionLineManagementService plms = new ProductionLineManagementService();
    private static final long MINUTES_TO_MILLI = 60000;
    private static final long DEFAULT_MINUTES = 15;

    public void messageProcessingBetweenDate(Calendar initialDate, Calendar finalDate){

       Iterable<ProductionLine> listProductionLines =  plms.listProductionLines();

        for (ProductionLine pd : listProductionLines) {
            new Thread(new MessagesProcessingThread(initialDate, finalDate, pd)).start();
        }

    }

    public void messageProcessingTimer(){

        Iterable<ProductionLine> listProductionLines =  plms.listProductionLines();

        for (ProductionLine pd : listProductionLines) {
            if(pd.getProcessingStatus() == ProcessingStatus.ACTIVE) {
                new Thread(new MessagesProcessingThreadTimer(pd)).start();
            }
        }

    }

    //in minutes
    public void startTimer(long delay){
        TimerTask task = new MessageProcessingTimer(this);
        Timer timer = new Timer();
        timer.schedule(task, new Date(), delay*MINUTES_TO_MILLI);
    }
}
