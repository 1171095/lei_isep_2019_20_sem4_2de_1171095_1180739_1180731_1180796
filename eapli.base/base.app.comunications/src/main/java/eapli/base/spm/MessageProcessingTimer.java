package eapli.base.spm;

import java.util.TimerTask;

public class MessageProcessingTimer extends TimerTask {

    private MessageProcessingController messageProcessingController;

    public MessageProcessingTimer(MessageProcessingController messageProcessingController){
        this.messageProcessingController = messageProcessingController;
    }

    @Override
    public void run() {

    this.messageProcessingController.messageProcessingTimer();

    }

}
