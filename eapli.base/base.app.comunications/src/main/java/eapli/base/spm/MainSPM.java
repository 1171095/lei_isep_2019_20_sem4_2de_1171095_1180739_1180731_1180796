package eapli.base.spm;

import eapli.base.app.common.console.presentation.authz.LoginUI;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.simulations.ImportMessagesFromFilesUI;
import eapli.base.simulations.MessageProcessingTimerUI;
import eapli.base.simulations.MessageProcessingUI;
import eapli.base.usermanagement.domain.BasePasswordPolicy;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;

import java.util.Scanner;

public class MainSPM {


    private static final String IMPORT_MESSAGE_FROM_FILE_STRING = "Import messages from files.";
    private static final String MESSAGE_PROCESSING_STRING = "Process messages.";
    private static final String MESSAGE_PROCESSING_TIMER_STRING = "Execute processing timer.";

    private static final String EXIT_OPTION = "0";
    private static final String IMPORT_MESSAGE_FROM_FILE_OPTION = "1";
    private static final String MESSAGE_PROCESSING_OPTION = "2";
    private static final String MESSAGE_PROCESSING_TIMER_OPTION = "3";

    public static void main(String[] args) {

        AuthzRegistry.configure(PersistenceContext.repositories().users(),
                new BasePasswordPolicy(), new PlainTextEncoder());
        AuthenticationService authenticationService = AuthzRegistry.authenticationService();
        authenticationService.authenticate("spm", "spmPW1", BaseRoles.SPM).isPresent();

        System.out.println("\nSPM APPLICATION STARTED!");

        String option;
        do {
            System.out.println("\nPlease select an option:" +
                    "\n1 - " + IMPORT_MESSAGE_FROM_FILE_STRING
                    + "\n2 - " + MESSAGE_PROCESSING_STRING
                    + "\n3 - " + MESSAGE_PROCESSING_TIMER_STRING
                    + "\n0 - Exit.");
            Scanner scanner = new Scanner(System.in);
            option = scanner.nextLine();

            switch (option.trim()) {
                case IMPORT_MESSAGE_FROM_FILE_OPTION:
                    new ImportMessagesFromFilesUI().show();
                    break;
                case MESSAGE_PROCESSING_OPTION:
                    new MessageProcessingUI().show();
                    break;
                case MESSAGE_PROCESSING_TIMER_OPTION:
                    new MessageProcessingTimerUI().show();
                default:
                    break;
            }

        } while (!option.trim().equalsIgnoreCase(EXIT_OPTION));

        System.out.println("\nSPM APPLICATION SAYS BYE BYE!\n");
        System.exit(0);
    }
}
