package eapli.base.spm;

import eapli.base.messagemanagement.application.MessageManagementService;

import java.io.*;


public class ReadMessageFileThread  implements Runnable {

    private File file;

    private File unprocessedMessagesFolder;

    private final MessageManagementService messageManagementService = new MessageManagementService();

    public ReadMessageFileThread(File file, File unprocessedMessagesFolder) {
        this.file = file;
        this.unprocessedMessagesFolder = unprocessedMessagesFolder;
    }

    @Override
    public void run() {

        try (BufferedReader reader = new BufferedReader(new FileReader(this.file))) {
            String line;

            reader.readLine();

            while ((line = reader.readLine()) != null) {
                line.trim();
                messageManagementService.createMessage(line);

            }
            reader.close();
            System.out.println("file "+ file.getName() + " read");
            String newName = String.format("%s%s%s",unprocessedMessagesFolder.getPath(),"/", file.getName());
            file.renameTo(new File(newName));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
