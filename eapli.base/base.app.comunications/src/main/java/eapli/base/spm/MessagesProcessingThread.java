package eapli.base.spm;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.application.MessageManagementService;
import eapli.base.messagemanagement.domain.*;
import eapli.base.productionlinemanagement.domain.ProductionLine;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

public class MessagesProcessingThread implements Runnable{

    private final Calendar initialDate;
    private final Calendar finalDate;
    private final ProductionLine prodLine;
    private final MessageManagementService mms =new MessageManagementService();

    public MessagesProcessingThread(Calendar initialDate, Calendar finalDate, ProductionLine prodLine) {
        this.initialDate = initialDate;
        this.finalDate = finalDate;
        this.prodLine = prodLine;
    }

    @Override
    public void run() {

        Iterable<Machine> machineIterable = prodLine.getLstMachines().values();

        for (Machine m :machineIterable) {
            Iterable<ChargebackMessage> chargebackMessageIterable = mms.getChargebackUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);
            Iterable<ConsumeMessage> consumeMessageIterable = mms.getConsumeUnprocessedMessagesFromMachineBetweenDates(m, initialDate, finalDate);
            Iterable<EndActivityMessage> endActivityMessageIterable = mms.getEndActivityUnprocessedMessagesFromMachineBetweenDates(m, initialDate, finalDate);
            Iterable<ErrorMessage> errorMessageIterable = mms.getErrorUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);
            Iterable<ProductionDeliveryMessage> deliveryMessageIterable = mms.getProductionDeliveryUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);
            Iterable<ProductionMessage> productionMessageIterable = mms.getProductionUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);
            Iterable<RestartActivityMessage> restartActivityMessageIterable = mms.getRestartActivityUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);
            Iterable<StartActivityMessage> startActivityMessageIterable = mms.getStartActivityUnprocessedMessagesFromMachineBetweenDates(m,initialDate,finalDate);

            mms.processChargebackMessageIterable(chargebackMessageIterable);
            mms.processConsumeMessageIterable(consumeMessageIterable);
            mms.processEndActivityMessageIterable(endActivityMessageIterable);
            mms.processErrorMessageIterable(errorMessageIterable,this.prodLine);
            mms.processProductionDeliveryMessageIterable(deliveryMessageIterable);
            mms.processProductionMessageIterable(productionMessageIterable);
            mms.processRestartActivityMessageIterable(restartActivityMessageIterable);
            mms.processStartActivityMessageIterable(startActivityMessageIterable);

        }

    }
}
