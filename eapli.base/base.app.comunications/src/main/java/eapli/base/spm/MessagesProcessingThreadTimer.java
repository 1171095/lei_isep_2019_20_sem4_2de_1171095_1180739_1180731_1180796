package eapli.base.spm;

import eapli.base.machinemanagement.domain.Machine;
import eapli.base.messagemanagement.application.MessageManagementService;
import eapli.base.messagemanagement.domain.*;
import eapli.base.productionlinemanagement.domain.ProductionLine;

import java.util.Calendar;

public class MessagesProcessingThreadTimer implements Runnable{

    private final ProductionLine prodLine;
    private final MessageManagementService mms =new MessageManagementService();

    public MessagesProcessingThreadTimer(ProductionLine prodLine) {
        this.prodLine = prodLine;
    }

    @Override
    public void run() {

        Iterable<Machine> machineIterable = prodLine.getLstMachines().values();

        for (Machine m :machineIterable) {
            Iterable<ChargebackMessage> chargebackMessageIterable = mms.getChargebackUnprocessedMessagesFromMachine(m);
            Iterable<ConsumeMessage> consumeMessageIterable = mms.getConsumeUnprocessedMessagesFromMachine(m);
            Iterable<EndActivityMessage> endActivityMessageIterable = mms.getEndActivityUnprocessedMessagesFromMachine(m);
            Iterable<ErrorMessage> errorMessageIterable = mms.getErrorUnprocessedMessagesFromMachine(m);
            Iterable<ProductionDeliveryMessage> deliveryMessageIterable = mms.getProductionDeliveryUnprocessedMessagesFromMachine(m);
            Iterable<ProductionMessage> productionMessageIterable = mms.getProductionUnprocessedMessagesFromMachine(m);
            Iterable<RestartActivityMessage> restartActivityMessageIterable = mms.getRestartActivityUnprocessedMessagesFromMachine(m);
            Iterable<StartActivityMessage> startActivityMessageIterable = mms.getStartActivityUnprocessedMessagesFromMachine(m);

            mms.processChargebackMessageIterable(chargebackMessageIterable);
            mms.processConsumeMessageIterable(consumeMessageIterable);
            mms.processEndActivityMessageIterable(endActivityMessageIterable);
            mms.processErrorMessageIterable(errorMessageIterable,this.prodLine);
            mms.processProductionDeliveryMessageIterable(deliveryMessageIterable);
            mms.processProductionMessageIterable(productionMessageIterable);
            mms.processRestartActivityMessageIterable(restartActivityMessageIterable);
            mms.processStartActivityMessageIterable(startActivityMessageIterable);

        }

    }
}
