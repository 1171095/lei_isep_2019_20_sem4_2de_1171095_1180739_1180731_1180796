package eapli.base.spm;

import java.io.File;
import java.util.Objects;

public class ImportMessagesFromFilesController {

    public void importMessagesFromFiles(){
        final File processedMessagesFolder = new File("messages_processed");
        final File unprocessedMessagesFolder = new File("messages_unprocessed");

        for(File file: Objects.requireNonNull(unprocessedMessagesFolder.listFiles())){
            new Thread(new ReadMessageFileThread(file, processedMessagesFolder)).start();
        }

    }
}
