package eapli.base.simulations;

import eapli.base.spm.MessageProcessingController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;


import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class MessageProcessingUI extends AbstractUI {

    private MessageProcessingController messageProcessingController = new MessageProcessingController();

    @Override
    protected boolean doShow() {
        final int initialYear = Console.readInteger("Initial Year:");
        final int initialMonth = Console.readInteger("Initial Month:");
        final int initialDay = Console.readInteger("Initial Day:");
        final int initialHour = Console.readInteger("Initial Hour:");
        final int initialMinute = Console.readInteger("Initial Minute:");
        final Calendar initialDate = new GregorianCalendar(initialYear,initialMonth,initialDay,initialHour,initialMinute);
        final int finalYear = Console.readInteger("Final Year:");
        final int finalMonth = Console.readInteger("Final Month:");
        final int finalDay = Console.readInteger("Final Day:");
        final int finalHour = Console.readInteger("Final Hour:");
        final int finalMinute = Console.readInteger("Final Minute:");
        final Calendar finalDate = new GregorianCalendar(finalYear, finalMonth, finalDay,finalHour,finalMinute);
        messageProcessingController.messageProcessingBetweenDate(initialDate, finalDate);


        return false;
    }

    @Override
    public String headline() {
        return "Message Processing";
    }
}
