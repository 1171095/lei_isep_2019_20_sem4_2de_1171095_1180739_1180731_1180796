package eapli.base.simulations;

import eapli.base.spm.MessageProcessingController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;


public class MessageProcessingTimerUI extends AbstractUI {

    private MessageProcessingController messageProcessingController = new MessageProcessingController();

    @Override
    protected boolean doShow() {
        final int delayMinutes = Console.readInteger("Insert delay in minutes to the timer:");

        messageProcessingController.startTimer(delayMinutes);

        return false;
    }

    @Override
    public String headline() {
        return "Message Processing Timer";
    }
}
