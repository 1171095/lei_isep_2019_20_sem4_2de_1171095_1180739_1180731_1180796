package eapli.base.simulations;

import eapli.base.productionlinemanagement.application.ListProductionLineDTOService;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

public class ProductionLineCommunicationSimulationUI extends AbstractUI {


    private ProductionLineManagementService productionLineManagementService = new ProductionLineManagementService();
    private ListProductionLineDTOService listDTO = new ListProductionLineDTOService();



    @Override
    protected boolean doShow() {

        //ProductionLine
        final SelectWidget<ProductionLineDTO> productionLineDTOSelectWidget = new SelectWidget<>("ProductionLine:", listDTO.getProductionLineDTOS(productionLineManagementService.listProductionLines()));
        productionLineDTOSelectWidget.show();
        if (productionLineDTOSelectWidget.selectedOption() != 0) {
            final ProductionLineDTO productionLineDTO = productionLineDTOSelectWidget.selectedElement();
            final ProductionLine productionLine = productionLineManagementService.findByProductionLineID(productionLineDTO.getProdLineID()).get();

            //Simulation
            //antiganmente criava thread para o tcp e o udp
        }
        return false;
    }

    @Override
    public String headline(){
        return "ProductionLine Communication Simulation";
    }
}
