package eapli.base.simulations;


import eapli.base.spm.ImportMessagesFromFilesController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

public class ImportMessagesFromFilesUI extends AbstractUI {

    private ImportMessagesFromFilesController importMessagesFromFilesController = new ImportMessagesFromFilesController();

    @Override
    protected boolean doShow() {
        System.out.println("Do you want to process messages from the unprocessed messages folder?");
        if( Console.readBoolean("Y/N")){
            importMessagesFromFilesController.importMessagesFromFiles();
        }

        return false;
    }

    @Override
    public String headline() {
        return "Import Messages From Files";
    }
}
