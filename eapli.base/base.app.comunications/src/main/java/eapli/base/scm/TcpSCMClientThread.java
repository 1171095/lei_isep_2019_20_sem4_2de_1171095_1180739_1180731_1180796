package eapli.base.scm;

import eapli.base.machinemanagement.application.MachineManagementService;
import eapli.base.machinemanagement.domain.Machine;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.security.KeyStore;
import java.util.concurrent.ExecutionException;

public class TcpSCMClientThread implements Runnable{

    static final int SERVER_PORT=9999;
    static final String KEYSTORE_PASS="forgotten";
    static final String CLIENT_NAME = "ssl/client1J";

    private InetAddress serverIP;
    private SSLSocket sock;
    private Machine machine;
    private byte[] bytesReceived;

    private MachineManagementService machineManagementService = new MachineManagementService();

    private static final int VERSION_OFFSET = 0;
    private static final int CODE_OFFSET = 1;
    private static final int ID_OFFSET = 2;
    private static final int DATA_LENGTH_OFFSET = 4;
    private static final int RAW_DATA_OFFSET = 6;

    private static final byte HELLO_REQUEST = (byte) 0;
    private static final byte MSG_REQUEST = (byte) 1;
    private static final byte CONFIG_REQUEST = (byte) 2;
    private static final byte ACK_RESPONSE = (byte) 150;
    private static final byte NACK_RESPONSE = (byte) 151;

    public TcpSCMClientThread(Machine machine, InetAddress clientIP, byte[] bytesIn) {
        this.serverIP=clientIP;
        this.machine=machine;
        this.bytesReceived=bytesIn;
    }

    @Override
    public void run() {

        // Trust these certificates provided by servers
        System.setProperty("javax.net.ssl.trustStore", CLIENT_NAME+".pem");
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key for client certificate when requested by the server
        System.setProperty("javax.net.ssl.keyStore",CLIENT_NAME+".jks");
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();

        try {
            sock = (SSLSocket) sf.createSocket(serverIP,SERVER_PORT);
        } catch (IOException ex) {
            System.out.println("Failed to establish TCP connection");
            System.exit(1);
        }

        try {
            System.out.println("Connected to: " + serverIP.getHostAddress() + ":" + SERVER_PORT);


            sock.startHandshake();

            DataOutputStream sOut = new DataOutputStream(sock.getOutputStream());
            DataInputStream sIn = new DataInputStream(sock.getInputStream());

            byte[] bytes = new byte[512];

            File file = machine.getRequestedConfiguration();
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);

            bytes[VERSION_OFFSET] = 0;
            bytes[CODE_OFFSET] = CONFIG_REQUEST;
            bytes[ID_OFFSET] = bytesReceived[ID_OFFSET];
            bytes[ID_OFFSET+1] = bytesReceived[ID_OFFSET+1];


            long fileLength = file.length();

            bis.read(bytes,RAW_DATA_OFFSET, (int)fileLength);

            byte[] bytesLength = ByteBuffer.allocate(4).putLong(fileLength).array();

            bytes[DATA_LENGTH_OFFSET] = bytesLength[2];
            bytes[DATA_LENGTH_OFFSET+1] = bytesLength[3];

            sOut.write(bytes);
            System.out.println("CONFIG REQUEST sent to machine "+machine.identity());

            sIn.read(bytes);

            if(bytes[CODE_OFFSET]==ACK_RESPONSE){
                machineManagementService.removeConfigRequestFromMachine(machine);
            }else if(bytes[CODE_OFFSET]==NACK_RESPONSE){
                System.out.println("Machine "+machine.identity()+" did not accepted configuration file...");
            }else{
                System.out.println("Invalid response received...");
            }

            sock.close();
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
