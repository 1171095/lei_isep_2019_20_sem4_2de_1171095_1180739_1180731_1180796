package eapli.base.scm;

import eapli.base.app.common.console.presentation.authz.LoginUI;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.productionlinemanagement.application.ListProductionLineDTOService;
import eapli.base.productionlinemanagement.application.ProductionLineManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.productionlinemanagement.domain.ProductionLineDTO;
import eapli.base.usermanagement.domain.BasePasswordPolicy;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthenticationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.infrastructure.authz.domain.model.PlainTextEncoder;
import eapli.framework.presentation.console.SelectWidget;

public class MainSCM {

    public static void main(String[] args) {
        AuthzRegistry.configure(PersistenceContext.repositories().users(),
                new BasePasswordPolicy(), new PlainTextEncoder());
        AuthenticationService authenticationService = AuthzRegistry.authenticationService();
        authenticationService.authenticate("scm", "scmPW1", BaseRoles.SCM).isPresent();

            System.out.println("\nSCM APPLICATION STARTED");

            ProductionLineManagementService productionLineManagementService = new ProductionLineManagementService();
            ListProductionLineDTOService listDTO = new ListProductionLineDTOService();

            //ProductionLine
            SelectWidget<ProductionLineDTO> productionLineDTOSelectWidget = new SelectWidget<>("ProductionLine:", listDTO.getProductionLineDTOS(productionLineManagementService.listProductionLines()));
            productionLineDTOSelectWidget.show();
            if (productionLineDTOSelectWidget.selectedOption() != 0) {
                ProductionLineDTO productionLineDTO = productionLineDTOSelectWidget.selectedElement();
                ProductionLine productionLine = productionLineManagementService.findByProductionLineID(productionLineDTO.getProdLineID()).get();

                TcpSCM tcpSCM = new TcpSCM(productionLine);
                tcpSCM.startTcpSCM();

            }

            System.out.println("\nSCM APPLICATION CLOSED\n");
        }

}
