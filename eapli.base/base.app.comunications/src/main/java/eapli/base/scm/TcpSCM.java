package eapli.base.scm;

import eapli.base.productionlinemanagement.domain.ProductionLine;

//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLServerSocket;
//import javax.net.ssl.SSLServerSocketFactory;
//import javax.net.ssl.SSLSocket;
//import java.io.IOException;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.security.NoSuchAlgorithmException;

import java.io.*;
import java.net.*;

import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class TcpSCM {

    //static ServerSocket sock;
    private ProductionLine productionLine;

    static final int SERVER_PORT=9999;
    static final String TRUSTED_STORE="ssl/serverJ.jks";
    static final String KEYSTORE_PASS="forgotten";

    public TcpSCM(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public void startTcpSCM() {



        SSLServerSocket sock=null;
        Socket cliSock;


        // Trust these certificates provided by authorized clients
        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore",TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        try {
            sock = (SSLServerSocket) sslF.createServerSocket(SERVER_PORT);
            sock.setNeedClientAuth(true);
        }
        catch(IOException ex) {
            System.out.println("Server failed to open local port " + SERVER_PORT);
            System.exit(1);
        }

        System.out.println("\nTCP Server started! Ready to received requests from the machines...\n");


        while (true) {
            try {

                cliSock = sock.accept();
                new Thread(new TcpSCMThread(cliSock, productionLine)).start();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
