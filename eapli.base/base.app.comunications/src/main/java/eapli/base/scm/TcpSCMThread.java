package eapli.base.scm;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.machinemanagement.domain.Machine;
import eapli.base.machinemanagement.repositories.MachineRepository;
import eapli.base.messagemanagement.application.MessageManagementService;
import eapli.base.productionlinemanagement.domain.ProductionLine;
import eapli.base.spm.MessageProcessingController;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TcpSCMThread implements Runnable {

    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private ProductionLine productionLine;

    private final MachineRepository machineRepo = PersistenceContext.repositories().machine();

    private static Map<Long, InetAddress> ipsMachines = new HashMap<>();

    private MessageManagementService messageManagementService = new MessageManagementService();

    private static final int VERSION_OFFSET = 0;
    private static final int CODE_OFFSET = 1;
    private static final int ID_OFFSET = 2;
    private static final int DATA_LENGTH_OFFSET = 4;
    private static final int RAW_DATA_OFFSET = 6;

    private static final byte HELLO_REQUEST = (byte) 0;
    private static final byte MSG_REQUEST = (byte) 1;
    private static final byte ACK_RESPONSE = (byte) 150;
    private static final byte NACK_RESPONSE = (byte) 151;


    public TcpSCMThread(Socket s, ProductionLine productionLine) {
        this.s = s;
        this.productionLine = productionLine;
    }

    public static byte [] getBytesOut(byte [] bytesIn, byte code){
        byte [] bytesOut = new byte[512];
        bytesOut[VERSION_OFFSET] = 0;
        bytesOut[CODE_OFFSET] = code;
        bytesOut[ID_OFFSET] = bytesIn[ID_OFFSET];
        bytesOut[ID_OFFSET+1] = bytesIn[ID_OFFSET+1];
        bytesOut[DATA_LENGTH_OFFSET] = 0;
        return bytesOut;
    }

    public void run() {

        InetAddress clientIP;

        clientIP=s.getInetAddress();

        System.out.println("SCM: New client connection from " + clientIP.getHostAddress() +
                ", port number " + s.getPort());
        try {
            sOut = new DataOutputStream(s.getOutputStream());
            sIn = new DataInputStream(s.getInputStream());

            byte[] bytesIn = new byte[512];
                sIn.read(bytesIn);

                byte code = bytesIn[CODE_OFFSET];
                Long id = (long) ((bytesIn[ID_OFFSET] & 0xff) + ((bytesIn[ID_OFFSET + 1] & 0xff) << 8));

                if (code == HELLO_REQUEST) { //HELLO request

                    Machine machine = productionLine.getMachineByID(id);
                    if (machine!=null) {
                        ipsMachines.put(id, clientIP);
                        //ACK
                        byte[] bytesOut = getBytesOut(bytesIn, ACK_RESPONSE);
                        System.out.println("SCM: Hello received from machine " + id);
                        sOut.write(bytesOut);

                        //s.close();
                        //if machine has config
                        if(machine.hasRequestedConfiguration()){
                            new Thread(new TcpSCMClientThread(machine, clientIP, bytesIn)).start();
                        }
                    } else {
                        //NACK
                        byte[] bytesOut = getBytesOut(bytesIn, NACK_RESPONSE);
                        System.out.println("SCM: machine not found");
                        sOut.write(bytesOut);
                        //break;
                    }

                } else if (code == MSG_REQUEST) { // MSG request
                    int messageLength = (bytesIn[DATA_LENGTH_OFFSET] & 0xff) + ((bytesIn[DATA_LENGTH_OFFSET + 1] & 0xff) << 8);

                    byte[] bytesMessage = Arrays.copyOfRange(bytesIn, RAW_DATA_OFFSET, messageLength + RAW_DATA_OFFSET);

                    String message = new String(bytesMessage, StandardCharsets.UTF_8);

                    if (messageLength > 0) {
                        System.out.println("SCM: Message received from machine " + id + ": " + message);
                    } else {
                        System.out.println("SCM: empty message from machine " + id);
                    }
                    if (ipsMachines.containsKey(id)) {
                        if (ipsMachines.get(id).getHostAddress().equals(clientIP.getHostAddress())) {
                            if (messageManagementService.createMessage(message)) {
                                //ACK
                                System.out.println("SCM: message created successfully");
                                byte[] bytesOut = getBytesOut(bytesIn, ACK_RESPONSE);
                                sOut.write(bytesOut);

                            } else {
                                //NACK
                                System.out.println("SCM: Error creating message");
                                byte[] bytesOut = getBytesOut(bytesIn, NACK_RESPONSE);
                                sOut.write(bytesOut);
                            }
                        } else {
                            //NACK
                            System.out.println("SCM: machine's address not matching");
                            byte[] bytesOut = getBytesOut(bytesIn, NACK_RESPONSE);
                            sOut.write(bytesOut);
                            //break;
                        }
                    } else {
                        //NACK
                        System.out.println("SCM: machine not found");
                        byte[] bytesOut = getBytesOut(bytesIn, NACK_RESPONSE);
                        sOut.write(bytesOut);
                        //break;
                    }
                } else{
                    //NACK
                    System.out.println("SCM: Invalid code received in message");
                    byte[] bytesOut = getBytesOut(bytesIn, NACK_RESPONSE);
                    sOut.write(bytesOut);
                }

            System.out.println("SCM: Client " + clientIP.getHostAddress() + ", port number: " + s.getPort() +
                    " disconnected");
            s.close();
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
