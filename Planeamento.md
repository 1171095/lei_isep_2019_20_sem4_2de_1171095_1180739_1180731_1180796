# Projeto Integrador da LEI-ISEP Sem4 2019-20

# 1. Constituição do Grupo de Trabalho

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.                     | Nome do Aluno      |
| ----------------------------- | ------------------ |
| **[1180796](/docs/1180796/)** | João Gaiteiro      |
| **[1180739](/docs/1180739/)** | Jorge Pacheco      |
| **[1180731](/docs/1180731/)** | Alexandre Lourenço |
| **[1171095](/docs/1171095/)** | Tiago Pinto        |



# 2. Distribuição de Funcionalidades ###

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

| Aluno Nr.                                                  | Sprint B                                                   | Sprint C                                                                                                                                         | Sprint D |
| ---------------------------------------------------------- | ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------- |
| [**1171095**](/docs/1171095/)                              | [US1003]                                                   | [US2009-IMPORTAR ORDENS DE PRODUCAO FICH CSV](/docs/1171095/ImportProductionOrdersFromCSV)                                                   |[US2013-APLICAR TRANSFORMAÇÕES AO FICHEIRO XML](/docs/1171095/TransformXML)        |
|                                                            | [US1006]                                                   | [US2011-CONSULTAR ORDENS DE PRODUCAO NUM DETERMINADO ESTADO](/docs/1171095/ConsultProductionOrders)                                                   |          |
|                                                            | [US2003](/docs/1171095/ListProductsWithoutProductionForm/sd.jpg) | [US1011-SIMULACAO FUNCIONAMENTO DE MAQUINA](/docs/Group_Made_Documentation/US1011)                                                                    |          |
|                                                            | [US2004]                                                   | [US6001-SMM MONITORIZAR O ESTADO DAS MAQUINAS POR LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US6001)                                           |          |
| [**1180796**](/docs/1180796/)                              | [US1005](/docs/1180796/Bootstrap_RawMaterialCategory)      | [U2012-CONSULTAR ORDENS DE PRODUCAO DE UMA DADA ENCOMENDA](/docs/1180796/ConsultTheProductionOrderOfASelectedOrder)                                   |  [US1016 - MAQUINA SUPORTAR PEDIDOS DE RESET](/docs/1180796/RESET_REQUEST)        |
|                                                            | [US1008](/docs/1180796/Bootstrap_ProductionLine)           | [U3006-ARQUIVAR NOTIFICACOES DE ERROS DE PROCESSAMENTO](/docs/1180796/FileErrorNotifications)                                                         | [US3009 - SABER E ALTERAR O ESTADO DE PROCESSAMENTO](/docs/1180796/SwitchMessageProcessmentState)         |
|                                                            | [US2002](/docs/1180796/AddNewRawMaterialCategory)          | [US1011-SIMULACAO FUNCIONAMENTO DE MAQUINA](/docs/Group_Made_Documentation/US1011)                                                                    |          |
|                                                            | [US3002](/docs/1180796/SpecifyNewProductionLine)           | [US6001-SMM MONITORIZAR O ESTADO DAS MAQUINAS POR LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US6001)                                            |          |
| [**1180739**](/docs/1180739/)                              | [US1004](/docs/1180739/BootstrapAddNewRawMaterial)         | [US3005-CONSULTAR NOTIFICACOES DE ERRO DE PROCESSAMENTO DE TRATAR](/docs/1180739/ConsultErrorNotificationsUntreated)                             |[US1014 - MAQUINA SUPORTAR](/docs/1180739/US1014)          |
|                                                            | [US1007](/docs/1180739/BootstrapAddNewMachine)             | [US3007-CONSULTAR NOTIFICACOES DE ERRO DE PROCESSAMENTO ARQUIVADAS](/docs/1180739/ConsultErrorNotificationsArchived)                             | [US5002 - EFETUAR PROCESSAMENTO DE MENSAGENS DISPONIVEIS NO SISTEMA](/docs/1180739/US5002)         |
|                                                            | [US2001](/docs/1180739/AddNewRawMaterial)                  | [US1012-SIMULADOR DE MAQUINA SUPORTAR PEDIDOS DE MONITORIZACAO DO SEU ESTADO](/docs/Group_Made_Documentation/US1012)                                  |          |
|                                                            | [US3001](/docs/1180739/AddNewMachine)                      | [US4001-SCM IMPORTAR E DISPONIBILIZAR AS MENSAGENS EXISTENTES NOS FICHEIROS PRESENTES NO DIRETORIO DE ENTRADA](/docs/Group_Made_Documentation/US4001) |          |
|                                                            | -                                                          | [US4002-SCM RECOLHA DAS MENSAGENS GERADAS NAS/PELAS MAQUINAS DE UMA DETERMINADA LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US4002)             |          |
| [**1180731**](/docs/1180731/)                              | [US1009](/docs/1180731/Bootstrap_Deposit)                  | [US2010-INTRODUZIR MANUALMENTE UMA NOVA ORDEM DE PRODUCAO](/docs/1180731/IntroduceNewProductionOrder)                                                 | [US1013 - PROTEGER COMUNICAÇÃO ENTRE SCM E MAQUINAS](/docs/1180731/US1013)         |
|                                                            | [US2005](/docs/1180731/ImportCatalogFromCSV)               | [US3004-ASSOCIAR UM FICHEIRO DE CONFIGURACAO A UMA MAQUINA](/docs/1180731/AddConfigurationToMachine)                                                  |[US1015 - PROTEGER COMUNICAÇÃO ENTRE MAQUINAS E SCM](/docs/1180731/US1015)          |
|                                                            | [US2006](/docs/1180731/AddProductToCatalog)                | [US1012-SIMULADOR DE MAQUINA SUPORTAR PEDIDOS DE MONITORIZACAO DO SEU ESTADO](/docs/Group_Made_Documentation/US1012)                                  | [US3010 - ENVIAR CONFIGURACAO PARA MAQUINA](/docs/1180731/US3010)         |
|                                                            | [US3003](/docs/1180731/SpecifyNewDeposit)                  | [US4001-SCM IMPORTAR E DISPONIBILIZAR AS MENSAGENS EXISTENTES NOS FICHEIROS PRESENTES NO DIRETORIO DE ENTRADA](/docs/Group_Made_Documentation/US4001) |          |
|                                                            | -                                                          | [US4002-SCM RECOLHA DAS MENSAGENS GERADAS NAS/PELAS MAQUINAS DE UMA DETERMINADA LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US4002)             |          |
| [**GROUP_DOCUMENTATION**](/docs/Group_Made_Documentation/) | -                                                          | [US1011-SIMULACAO FUNCIONAMENTO DE MAQUINA](/docs/Group_Made_Documentation/US1011)                                                                    |          |
|                                                            | -                                                          | [US1012-SIMULADOR DE MAQUINA SUPORTAR PEDIDOS DE MONITORIZACAO DO SEU ESTADO](/docs/Group_Made_Documentation/US1012)                                  |          |
|                                                            | -                                                          | [US2007-EXPORTAR INFORMAÇÃO SUBJACENTE AO CHAO DE FABRICO PARA XML](/docsVGroup_Made_Documentation/US2007)                                            |          |
|                                                            | -                                                          | [US4001-SCM IMPORTAR E DISPONIBILIZAR AS MENSAGENS EXISTENTES NOS FICHEIROS PRESENTES NO DIRETORIO DE ENTRADA](/docs/Group_Made_Documentation/US4001) |          |
|                                                            | -                                                          | [US4002-SCM RECOLHA DAS MENSAGENS GERADAS NAS/PELAS MAQUINAS DE UMA DETERMINADA LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US4002)             |          |
|                                                            | -                                                          | [US5001-SPM PROCESSAMENTO DE MENSAGENS DISPONIVEIS](/docs/Group_Made_Documentation/US5001)             |          |
|                                                            | -                                                          | [US6001-SMM MONITORIZAR O ESTADO DAS MAQUINAS POR LINHA DE PRODUCAO](/docs/Group_Made_Documentation/US6001)                                           |          |
