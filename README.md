# README #

FACTORY MANAGEMENT APPLICATION



1171095 - Tiago Durão Pinto
1180796 - João Gaiteiro
1180739 - Jorge Pacheco
1180731 - Alexandre Lourenço

Computer engineering students Polythecnic of Porto, School of Engineering

## Planning file ##

* [PLANEAMENTO](Planeamento.md)

### Purpose and language ###

* Semester final project of lpr4 course unit 
* Project languange JAVA maven and C
 

## Build

Make sure Maven is installed and on the PATH.

The java source is Java 1.8+ so any JDK 1.8 or later will work. However, in order to generate the javadoc and UML diagrams the JDK version must be *strictly 1.8*.

If using an Oracle database, you will need to change your maven settings for 
downloading the Oracle drivers. see <https://blogs.oracle.com/dev2dev/entry/how_to_get_oracle_jdbc#settings> for more information.

run script 

    rebuild-all.bat

## Running

make sure a JRE is installed and on the PATH

run script 

    run-backoffice 

or 

    run-user.bat

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact